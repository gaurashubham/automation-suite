package com.sfv.qa.testcase;

import java.awt.AWTException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.Createplaylistpage;
import com.sfv.qa.pages.Createtagprofilepage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.pages.Playlistpage;
import com.sfv.qa.pages.Tagpage;
import com.sfv.qa.util.TestUtil;

public class Createtagprofiletest extends testbase
{
	Loginpage loginpage;
    HomePage homePage;
    TestUtil testUtil;
    Blazeclanpage blazeclanapppage;
    ActiveaccountPage activeaccpage;
    Tagpage tagpage;
    Createtagprofilepage creatagpage;
    testbase testbase;
    
    String sheetName = "tagprofilecreation";
    
    public Createtagprofiletest()
    {
       super();
    }
    
    @BeforeMethod
    public void setUp() 
    {
    	initialization();
        testUtil = new TestUtil();
        loginpage = new  Loginpage();
        activeaccpage = new ActiveaccountPage();
        homePage = new HomePage();
        blazeclanapppage = new Blazeclanpage();
        tagpage = new Tagpage();
        creatagpage = new Createtagprofilepage();
        testbase = new testbase();
        
       homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
       activeaccpage = homePage.clickOnActiveAccountButton();
       activeaccpage.clickOnButton();
       blazeclanapppage.clickOntagtab();
       tagpage.tagpageprofile();
   }
    
    @DataProvider
    public Object[][] getSFVTestData()
    {
        Object data[][] = TestUtil.getTestData(sheetName);
        return data;
    }
    
    @Test(priority=1, dataProvider="getSFVTestData")

	public void creationtag(String title,String namespac,String predi,String values) throws InterruptedException, AWTException
    {
    	creatagpage.createNewtagprofile(title, namespac, predi, values);

	}
    
    @AfterMethod
    public void closebrowser() throws InterruptedException
	{
       Thread.sleep(5000); 
       testbase.logout();
       Thread.sleep(5000);
       driver.quit();
    }
    
   

}
