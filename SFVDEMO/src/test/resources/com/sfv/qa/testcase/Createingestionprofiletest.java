package com.sfv.qa.testcase;

import java.awt.AWTException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.Createingestionprofilepage;
import com.sfv.qa.pages.Createuserpage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Ingestionprofilepage;
import com.sfv.qa.pages.Ingestprofilepage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.pages.Managementpage;
import com.sfv.qa.pages.Playlistpage;
import com.sfv.qa.pages.Usersettingpage;
import com.sfv.qa.util.TestUtil;

public class Createingestionprofiletest extends testbase
{
	Loginpage loginpage;
    HomePage homePage;
    TestUtil testUtil;
    Blazeclanpage blazeclanapppage;
    ActiveaccountPage activeaccpage;
    Playlistpage playlistpage;
    Managementpage managepage;
    Ingestprofilepage ingestpropage;
    Createingestionprofilepage createinpropage;
    testbase testbase;
    
    String sheetName = "ingestapi1";
    String sheetName1 = "ingestionpull";
    String sheetName2 = "ingestionpush";
    
    public Createingestionprofiletest()
    {
       super();
    }
    
    @BeforeMethod
    public void setUp() 
    {
    	initialization();
        testUtil = new TestUtil();
        loginpage = new  Loginpage();
        activeaccpage = new ActiveaccountPage();
        homePage = new HomePage();
        blazeclanapppage = new Blazeclanpage();
        managepage= new Managementpage();
        ingestpropage = new Ingestprofilepage();
        createinpropage = new  Createingestionprofilepage();
        testbase = new testbase();
        
        
        homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
        activeaccpage = homePage.clickOnActiveAccountButton();
        activeaccpage.clickOnButton();
        blazeclanapppage.clickOnmanagementab();
        managepage.clickoningestionprofilepage();
        ingestpropage.clickoncreateingestprofilebtn();
        
        
    }
    
     @DataProvider
    public Object[][] getSFVTestData()
    {
        Object data[][] = TestUtil.getTestData(sheetName);
        return data;
    }
     
     @DataProvider
     public Object[][] getSFVTestData1()
     {
         Object data[][] = TestUtil.getTestData(sheetName1);
         return data;
     }
     
     @DataProvider
     public Object[][] getSFVTestData2()
     {
         Object data[][] = TestUtil.getTestData(sheetName2);
         return data;
     }
    
    @Test(priority=1, dataProvider="getSFVTestData")

	public void createingestionprof(String title, String username,String password,String description) throws InterruptedException, AWTException
    {
    	createinpropage.createingest(title,username,password,description);
    }
    
    @Test(priority=2, dataProvider="getSFVTestData1")

   	public void createingestftppull(String title,String Url,String mediafol,String thumbnailfol,String metadatafol,String username,String password,String description) throws InterruptedException, AWTException
    {
    	
       	createinpropage.createFTPpull(title, Url, mediafol, thumbnailfol, metadatafol, username, password, description);
    }
       
    @Test(priority=3, dataProvider="getSFVTestData2")

   	public void createingestftppush(String title,String username,String password,String description) throws InterruptedException, AWTException
    {
       	createinpropage.createftppush(title, username, password, description);
    }
       
    @AfterMethod
    public void closebrowser() throws InterruptedException
	{
       Thread.sleep(5000); 
       testbase.logout();
       Thread.sleep(5000);
       driver.quit();
    }
   
    

}
