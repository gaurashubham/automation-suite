package com.sfv.qa.testcase;

import java.awt.AWTException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.pages.Mediapage;
import com.sfv.qa.pages.Uploadpage;
import com.sfv.qa.util.TestUtil;

public class Mediatest extends testbase
{
	Loginpage loginpage;
    HomePage homePage;
    TestUtil testUtil;
    Blazeclanpage blazeclanapppage;
    ActiveaccountPage activeaccpage;
    Uploadpage uploadpage;
    Mediapage mediapage;
    testbase testbase;
    
    String sheetName = "Sheet2";
    String sheetName1 = "incomplete";
    String sheetName2 = "multilanguage";
    String sheetName3 = "mediapreview";
    
    public Mediatest()
    {
       super();
    }
    
    @BeforeMethod
    public void setUp() 
    {
    	initialization();
        testUtil = new TestUtil();
        loginpage = new  Loginpage();
        activeaccpage = new ActiveaccountPage();
        homePage = new HomePage();
        blazeclanapppage = new Blazeclanpage();
        uploadpage = new Uploadpage();
        mediapage = new Mediapage();
        testbase = new testbase();
        
        homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
        activeaccpage = homePage.clickOnActiveAccountButton();
        activeaccpage.clickOnButton();
        blazeclanapppage.clickOnmediatab();
       
     }
    
    @DataProvider
    public Object[][] getSFVTestData()
    {
       Object data[][] = TestUtil.getTestData(sheetName);
       return data;
    }
    
    @DataProvider
    public Object[][] getSFVTestData1()
    {
       Object data[][] = TestUtil.getTestData(sheetName1);
       return data;
    }
    
    @DataProvider
    public Object[][] getSFVTestData2()
    {
       Object data[][] = TestUtil.getTestData(sheetName2);
       return data;
    }
    
    @DataProvider
    public Object[][] getSFVTestData3()
    {
       Object data[][] = TestUtil.getTestData(sheetName3);
       return data;
    }
    
    @Test(priority=1)
    public void syndication() throws InterruptedException
    {
    	mediapage.syndicationofmedia();
    //driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    }
    
  @Test(priority=2, dataProvider="getSFVTestData")
  public void verificationofreplace(String test) throws InterruptedException, AWTException
   {
	  mediapage.replacement(test);
   }
  
  @Test(priority=3, dataProvider="getSFVTestData1")
  public void verificationofincomplete(String path) throws InterruptedException, AWTException
   {
	  mediapage.incompletedmedia(path);
   }
  
  @Test(priority=4)
  public void deletemultimedia() throws InterruptedException
   {
	  mediapage.deletemulti();
   }
  
  @Test(priority=5)
  public void statusmultimedia() throws InterruptedException
   {
	  mediapage.statuschangemul();
   }
  
  @Test(priority=6)
  public void syndicatemultimedia() throws InterruptedException
   {
	  mediapage.syndicatemul();
   }
  
  @Test(priority=7)
  public void syndicatedmedia() throws InterruptedException, AWTException
   {
	  mediapage.syndicatedmediatab();
   }
  
  @Test(priority=8)
  public void deletedmedia() throws InterruptedException, AWTException
   {
	  mediapage.deletedmediatab();
   }
  
  @Test(priority=11, dataProvider="getSFVTestData2")
  public void veriofmulti(String title,String description) throws InterruptedException, AWTException
   {
	  mediapage.multiplelanguage(title, description);
   }
  
  @Test(priority=10, dataProvider="getSFVTestData3")
  public void veriofmediapre(String path) throws InterruptedException, AWTException
   {
	  mediapage.mediapreview(path);
   }
  
    @Test(priority=9)
    public void veriofaddmediatoplay() throws InterruptedException, AWTException
   {
	  mediapage.addmediatoplay();
   }
    
    @AfterMethod
     public void closebrowser() throws InterruptedException
	{
     Thread.sleep(5000); 
     testbase.logout();
     Thread.sleep(5000);
     driver.quit();
  }
}    
    

