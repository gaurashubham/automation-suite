package com.sfv.qa.testcase;

import java.awt.AWTException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.Createplaylistpage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.pages.Playlistpage;
import com.sfv.qa.util.TestUtil;

public class Createplaylisttest extends testbase
{
	Loginpage loginpage;
    HomePage homePage;
    TestUtil testUtil;
    Blazeclanpage blazeclanapppage;
    ActiveaccountPage activeaccpage;
    Playlistpage playlistpage;
    Createplaylistpage createplaylistpage;
    testbase testbase;
    
    String sheetName = "createplaylist";
    
    String sheetName1 = "endtoendplay";
    
    public Createplaylisttest()
    {
       super();
    }
    
    @BeforeMethod
    public void setUp() 
    {
    	initialization();
        testUtil = new TestUtil();
        loginpage = new  Loginpage();
        activeaccpage = new ActiveaccountPage();
        homePage = new HomePage();
        blazeclanapppage = new Blazeclanpage();
        playlistpage = new Playlistpage();
        createplaylistpage = new Createplaylistpage();
        testbase = new testbase();
        
        homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
        activeaccpage = homePage.clickOnActiveAccountButton();
        activeaccpage.clickOnButton();
        blazeclanapppage.clickOnplaylisttab();
        playlistpage.Listbtn();
        
    }
    
    @DataProvider
    public Object[][] getSFVTestData()
    {
        Object data[][] = TestUtil.getTestData(sheetName);
        return data;
    }
    
    @DataProvider
    public Object[][] getSFVTestData1()
    {
        Object data[][] = TestUtil.getTestData(sheetName1);
        return data;
    }
    
    @Test(priority=1, dataProvider="getSFVTestData")

	public void validateCreateplaylist(String title, String description) throws InterruptedException, AWTException
    {
    	createplaylistpage.createNewPlaylist(title , description);

	}
    
    @Test(priority=2)
    public void AddMediaToThePlayist() throws InterruptedException, AWTException
    {
    	createplaylistpage.addmediatoplaylist();
    }
    
    @Test(priority=3, dataProvider="getSFVTestData1")
    public void CompletePlaylistFlow(String title, String description) throws InterruptedException
    {
    	createplaylistpage.endtoendplaylist(title, description);
    }
    
    @Test(priority=4)
    public void DeletionOfMultiplaylist() throws InterruptedException
    {
    	createplaylistpage.deletemulti();
    }
    
   @Test(priority=5)
    public void ChangingStatusOfMultiplayLIST() throws InterruptedException
    {
    	createplaylistpage.changestatusmulti();
    }
    
//    @Test(priority=6)
//  public void RemovalOfMediaFromPlaylist() throws InterruptedException
//  {
//  	createplaylistpage.removalmedia();
//  }
  
   
    @AfterMethod
    public void closebrowser() throws InterruptedException
	{
       Thread.sleep(5000); 
       testbase.logout();
       Thread.sleep(5000);
       driver.quit();
    }
    
   
}
