package com.sfv.qa.testcase;

import java.awt.AWTException;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.pages.Playlistpage;
import com.sfv.qa.pages.Uploadpage;
import com.sfv.qa.util.TestUtil;

public class Uploadtest extends testbase
{
	Loginpage loginpage;
    HomePage homePage;
    TestUtil testUtil;
    Blazeclanpage blazeclanapppage;
    ActiveaccountPage activeaccpage;
    Uploadpage uploadpage;
    testbase testbase;
    
    String sheetName = "uploadwebm";
    String sheetName1 = "uplooadmp4";
    String sheetName2 = "uploadmov";
    String sheetName3 = "uploadincorrect";
    String sheetName4 = "syndicationvideo";
    String sheetName5 = "transcoding";
    String sheetName6 = "uploadurl";
    
    public Uploadtest()
    {
       super();
    }
    
    @BeforeMethod
    public void setUp() throws InterruptedException 
    {
    	initialization();
        testUtil = new TestUtil();
        loginpage = new  Loginpage();
        activeaccpage = new ActiveaccountPage();
        homePage = new HomePage();
        blazeclanapppage = new Blazeclanpage();
        uploadpage = new Uploadpage();
        testbase = new testbase();
        
        homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
        activeaccpage = homePage.clickOnActiveAccountButton();
        activeaccpage.clickOnButton();
        blazeclanapppage.clickOnuploadtab();
        
     }
    
    @DataProvider
    public Object[][] getSFVTestData()
    {
       Object data[][] = TestUtil.getTestData(sheetName);
       return data;
    }
    
//   @Test(priority=1)
//  public void  verificationofupload() throws InterruptedException
//  {
//	      uploadpage.verificationofuploading(sheetName);
//  }
    
    @Test(priority=1, dataProvider="getSFVTestData")
    public void uploadingwebm(String path, String title,String description,String copyright) throws InterruptedException, AWTException
    {
  	      uploadpage.uploadmedia(path, title, description, copyright);
    }
    
   @DataProvider
    public Object[][] getSFVTestData1()
    {
        Object data[][] = TestUtil.getTestData(sheetName1);
        return data;
    }
    
    @Test(priority=2, dataProvider="getSFVTestData1")
    public void uploadingmp4(String path, String title,String description,String copyright) throws InterruptedException, AWTException
    {
  	      uploadpage.uploadmedia(path, title, description, copyright);
    }
   
    @DataProvider
    public Object[][] getSFVTestData2()
    {
        Object data[][] = TestUtil.getTestData(sheetName2);
        return data;
    }
    
    @Test(priority=3, dataProvider="getSFVTestData2")
    public void uploadingmov(String path, String title,String description,String copyright) throws InterruptedException, AWTException
    {
  	      uploadpage.uploadmedia(path, title, description, copyright);
    }
    
   @DataProvider
    public Object[][] getSFVTestData3()
    {
        Object data[][] = TestUtil.getTestData(sheetName3);
        return data;
    }
    
//    @Test(priority=4, dataProvider="getSFVTestData3")
//    public void uploadingincorrect(String path, String title,String description,String copyright) throws InterruptedException, AWTException
//    {
//  	      uploadpage.uploadmedia(path, title, description, copyright);
//    }
//    
    @DataProvider
    public Object[][] getSFVTestData4()
    {
        Object data[][] = TestUtil.getTestData(sheetName4);
        return data;
    }
    
   @Test(priority=4, dataProvider="getSFVTestData4")
    public void syndicateendtoend(String path, String title,String description,String copyright) throws InterruptedException
    {
    	uploadpage.syndicateendtoend(path, title, description, copyright);
    }
    
    @DataProvider
    public Object[][] getSFVTestData5()
    {
       Object data[][] = TestUtil.getTestData(sheetName5);
       return data;
    }
    
    
    @Test(priority=5, dataProvider="getSFVTestData5")
    public void transcoding(String path, String title,String description,String copyright) throws InterruptedException, AWTException
    {
  	      uploadpage.transcoding(path, title, description, copyright);
    }
   
    @DataProvider
  public Object[][] getSFVTestData6()
  {
     Object data[][] = TestUtil.getTestData(sheetName6);
     return data;
  }
    
    @Test(priority=6, dataProvider="getSFVTestData6")
  public void uploadusingurl(String title,String description,String copyright) throws InterruptedException, AWTException
  {
	      uploadpage.uploadurl(title, description, copyright);
  }
 
    
   @AfterMethod
   public void closebrowser() throws InterruptedException
 	{
      Thread.sleep(5000); 
      testbase.logout();
      Thread.sleep(5000);
      driver.quit();
   }

    
   
    
}
