package com.sfv.qa.testcase;

import org.testng.Assert;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Loginpage;

public class Loginpagetest extends testbase
{
	Loginpage loginPage;
    HomePage homePage;
    testbase testbase;
    

	public Loginpagetest()
	{
       super();
    }

	@BeforeMethod
    public void setUp()
	{
        initialization();
        loginPage = new Loginpage();
        testbase = new testbase();
    }
    @Test(priority=1)
    public void loginPageTitleTest()
    {
       String title = loginPage.validateLoginPageTitle();
       Assert.assertEquals(title, "Short Form Video - Astro");
    }

	@Test(priority=2)
    public void LoginPageimagetest()
	{
        boolean flag = loginPage.validateAstroImage();
        Assert.assertTrue(flag);
    }
    @Test(priority=3)
    public void loginwithValidUserCredentials() throws InterruptedException
    {
       loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
       Thread.sleep(5000);
    }
    
    @Test(priority=4)
    public void loginWithInvalidUserCredentials() throws InterruptedException
    {
       String username = "test@gmail.com";
       String password = "dump";
       loginPage.loginfail("username", "password");
       Thread.sleep(5000);
    }
    
     @AfterMethod
    public void closebrowser() throws InterruptedException
	{
       Thread.sleep(5000); 
       testbase.logout();
       Thread.sleep(5000);
       driver.quit();
    }

}
