package com.sfv.qa.testcase;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.Blazeclanpage;

import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.pages.Playlistpage;
import com.sfv.qa.util.TestUtil;

public class Playlisttest extends testbase
{
	Loginpage loginpage;
    HomePage homePage;
    TestUtil testUtil;
    Blazeclanpage blazeclanapppage;
    ActiveaccountPage activeaccpage;
    Playlistpage playlistpage;
    testbase testbase;
    
    public Playlisttest()
    {
       super();
    }
    
    @BeforeMethod
    public void setUp() 
    {
    	initialization();
        testUtil = new TestUtil();
        loginpage = new  Loginpage();
        activeaccpage = new ActiveaccountPage();
        homePage = new HomePage();
        blazeclanapppage = new Blazeclanpage();
        playlistpage = new Playlistpage();
        testbase = new testbase();
        
        homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
        activeaccpage = homePage.clickOnActiveAccountButton();
        activeaccpage.clickOnButton();
        blazeclanapppage.clickOnplaylisttab();
        
     }

    @Test(priority=1)
    public void playlist()
    {
    	
   	  //driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    	 playlistpage.Listbtn();
    }
    
//    @Test(priority=2)
//    public void addmediaplay() throws InterruptedException
//    {
//    	playlistpage.addmediatoplaylist();
//    }
    
   @AfterMethod
    public void closebrowser() throws InterruptedException
	{
       Thread.sleep(5000); 
       testbase.logout();
       Thread.sleep(5000);
       driver.quit();
    }
}
