package com.sfv.qa.testcase;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.pages.Managementpage;
import com.sfv.qa.pages.Playlistpage;
import com.sfv.qa.util.TestUtil;

public class Managementtest extends testbase
{
	Loginpage loginpage;
    HomePage homePage;
    TestUtil testUtil;
    Blazeclanpage blazeclanapppage;
    ActiveaccountPage activeaccpage;
    Managementpage managepage;
    testbase testbase;
    
    public Managementtest()
    {
       super();
    }
    
    @BeforeMethod
    public void setUp() 
    {
    	initialization();
        testUtil = new TestUtil();
        loginpage = new  Loginpage();
        activeaccpage = new ActiveaccountPage();
        homePage = new HomePage();
        blazeclanapppage = new Blazeclanpage();
        managepage = new Managementpage();
        testbase = new testbase();
        
        homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
        activeaccpage = homePage.clickOnActiveAccountButton();
        activeaccpage.clickOnButton();
        blazeclanapppage.clickOnmanagementab();
    }
    
    @Test(priority=1)
    public void Usersetting()
    {
    	//driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    	managepage.clickonusersetting();
    }
    
    @Test(priority=2)
    public void encoding()
    {
    	
   	  //driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    	managepage.clickonencodingprofile();
    }
    
    @Test(priority=3)
    public void thumbnail()
    {
    	
   	  //driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    	managepage.clickonthumbnailprofile();
    }
    
    @Test(priority=4)
    public void syndicationsetting()
    {
    	
   	  //driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    	managepage.clickonsyndicatesetting();
    }
    
    @Test(priority=5)
    public void ingestionpro()
    {
    	
   	  //driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    	managepage.clickoningestionprofilepage();
    }
    
    @AfterMethod
    public void closebrowser() throws InterruptedException
	{
       Thread.sleep(5000); 
       testbase.logout();
       Thread.sleep(5000);
       driver.quit();
    }
    

}
