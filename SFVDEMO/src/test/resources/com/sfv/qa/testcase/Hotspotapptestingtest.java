package com.sfv.qa.testcase;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.Applicationpage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Hotspotapptestingpage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.util.TestUtil;

public class Hotspotapptestingtest extends testbase
{
	Loginpage loginpage;
    HomePage homePage;
    TestUtil testUtil;
    Blazeclanpage blazeclanapppage;
    ActiveaccountPage activeaccpage;
    Applicationpage applicationpage;
    Hotspotapptestingpage hotspotapppage;
    testbase testbase;

    public Hotspotapptestingtest()
    {
       super();
    }
    
    @BeforeMethod
    public void setUp() throws InterruptedException 
    {
    	initialization();
        testUtil = new TestUtil();
        loginpage = new  Loginpage();
        activeaccpage = new ActiveaccountPage();
        homePage = new HomePage();
        hotspotapppage=new Hotspotapptestingpage();
        testbase = new testbase();
  
        homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
        activeaccpage = homePage.clickOnActiveAccountButton();
        for (int second = 0;; second++) 
        {
            if(second >=3){
               break;
               } 
          JavascriptExecutor jse = (JavascriptExecutor)driver;
   	    jse.executeScript("window.scrollBy(0,50)");
            Thread.sleep(3000);
           }       
        activeaccpage.clickonhotspotbtn();
        
        
     }
    
    
     @Test(priority=1)
    public void verifytagtablink()
    {
    	
   	  //driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    	 hotspotapppage.clickOnplaylisttab();
    }
     
     @AfterMethod
     public void closebrowser() throws InterruptedException
 	{
        Thread.sleep(5000); 
        testbase.logout();
        Thread.sleep(5000);
        driver.quit();
     }
}
