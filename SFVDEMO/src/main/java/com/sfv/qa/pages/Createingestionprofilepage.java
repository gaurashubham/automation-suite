package com.sfv.qa.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.sfv.qa.base.testbase;
import com.sfv.qa.util.TestUtil;

public class Createingestionprofilepage extends testbase
{
	@FindBy(xpath="//select[@id='ingestionType']")
	 WebElement ingestiontypelist;
	
	@FindBy(xpath="//input[@id='ingestionUserTitle']")
	 WebElement ingestusertitle;
	
	@FindBy(xpath="//label[@for='status']")
	 WebElement activebtn;
	
	@FindBy(xpath="//input[@id='ingestionUserName']")
	 WebElement ingestionusername;
	
	@FindBy(xpath="//input[@id='ingestionUserPassword']")
	 WebElement ingestionuserpassword;
	
	@FindBy(xpath="//button[text()='Generate Password']")
	 WebElement generatepass;
	
	@FindBy(xpath="//label[@for='approvalRequired']")
	 WebElement approvalbtn;
	
	@FindBy(xpath="//textarea[@placeholder='Insert Ingestion Profile Description']")
	WebElement ingestionuserdes;
	
	@FindBy(xpath="//input[@placeholder='Insert Ingestion Profile notification email']")
	WebElement notiemail;
	
	@FindBy(xpath="//button[text()='Save']")
	 WebElement savebtn;
	
	@FindBy(xpath="//button[text()='Cancel']")
	 WebElement cancelbtn;
	
	@FindBy(xpath="//input[@id='ingestionURL']")
	 WebElement ingesturl;
	
	@FindBy(xpath="//input[@id='ingestionMediaFolder']")
	 WebElement ingestmediafol;
	
	@FindBy(xpath="//input[@id='ingestionThumbnailFolder']")
	 WebElement ingestthumbfol;
	
	@FindBy(xpath="//input[@id='ingestionMetaDataFolder']")
	 WebElement ingestmetadatafol;
	
	@FindBy(xpath="//input[@placeholder='Search']")
	 WebElement searchtextfield;
	
	@FindBy(xpath="(//a[@class='ng-binding'])[1]")
	 WebElement verificationftppul;
	
	@FindBy(xpath="//div[@class='ui-pnotify-text']")
	 WebElement ingestionpronotifica;
	
	@FindBy(xpath="(//a[@class='ng-binding'])[1]")
	 WebElement verificationingestion;
	
	
	public Createingestionprofilepage() 
	   {
	       PageFactory.initElements(driver, this);
	   }
	
	public void createingest(String title, String username,String password,String description) throws InterruptedException, AWTException
	 {
		Select select = new Select(ingestiontypelist);
		select.selectByIndex(0);
		String title1 = title + TestUtil.GetCurrentDateAndTimeAppFormat();
		Thread.sleep(6000);
		ingestusertitle.sendKeys(title1);
		activebtn.click();
		String username1 = username + TestUtil.GetCurrentDateAndTimeAppFormat();
		Thread.sleep(6000);
		ingestionusername.sendKeys(username1);
		ingestionuserpassword.sendKeys(password);
//		approvalbtn.click();
		JavascriptExecutor jse1 = (JavascriptExecutor)driver;
	    jse1.executeScript("window.scrollBy(0,700)");
	    ingestionuserdes.sendKeys(description);
//		notiemail.sendKeys(notificationemail);
		Thread.sleep(3000);
	    savebtn.click();
//	   	Thread.sleep(6000);
//	   	savebtn.click();
	   	Thread.sleep(3000);
		String text1 = "Ingest profile saved successfully";
	   	String text2 = ingestionpronotifica.getText();
	    Assert.assertEquals(text1, text2);
	   	System.out.println("Profile created successfully");
   		driver.navigate().refresh();
	    searchtextfield.sendKeys(title1);
	    searchtextfield.sendKeys(Keys.ENTER);
	  
	    Thread.sleep(2000);
	    String text3 = title1;
	    String text4 = verificationingestion.getText();
	    System.out.println(text3);
	    System.out.println(text4);
	    Assert.assertEquals(text3, text4);
	    System.out.println("Passed");
	}
	
	public void createFTPpull(String title,String Url,String mediafol,String thumbnailfol,String metadatafol,String username,String password,String description) throws InterruptedException, AWTException
	 {
		Select select = new Select(ingestiontypelist);
		select.selectByIndex(1);
		String title1 = title + TestUtil.GetCurrentDateAndTimeAppFormat();
		Thread.sleep(6000);
		ingestusertitle.sendKeys(title1);
		activebtn.click();
		ingesturl.sendKeys(Url);
		ingestmediafol.sendKeys(mediafol);
		JavascriptExecutor jse1 = (JavascriptExecutor)driver;
	    jse1.executeScript("window.scrollBy(0,700)");
		ingestthumbfol.sendKeys(thumbnailfol);
		ingestmetadatafol.sendKeys(metadatafol);
		String username1 = username + TestUtil.GetCurrentDateAndTimeAppFormat();
		Thread.sleep(6000);
		ingestionusername.sendKeys(username1);
		ingestionuserpassword.sendKeys(password);
	    ingestionuserdes.sendKeys(description);
		Thread.sleep(2000);
	    savebtn.click();
	    Thread.sleep(3000);
	    String text3 = "Ingest profile saved successfully";
	   	String text4 = ingestionpronotifica.getText();
	    Assert.assertEquals(text3, text4);
	    System.out.println("Profile created successfully");
	    driver.navigate().refresh();
	    searchtextfield.sendKeys(title1);
	    searchtextfield.sendKeys(Keys.ENTER);
	   
	    Thread.sleep(2000);
	    String text1 = title1;
	    String text2 = verificationftppul.getText();
	    System.out.println(text1);
	    System.out.println(text2);
	    Assert.assertEquals(text1, text2);
	    System.out.println("Passed");
	   	

	 }
   	public void createftppush(String title,String username,String password,String description) throws InterruptedException, AWTException
   	{
   		Select select = new Select(ingestiontypelist);
		select.selectByIndex(2);
		String title1 = title + TestUtil.GetCurrentDateAndTimeAppFormat();
		Thread.sleep(6000);
		ingestusertitle.sendKeys(title1);
		activebtn.click();
		String username1 = username + TestUtil.GetCurrentDateAndTimeAppFormat();
		Thread.sleep(6000);
		ingestionusername.sendKeys(username1);
		ingestionuserpassword.sendKeys(password);
	    ingestionuserdes.sendKeys(description);
	    Thread.sleep(3000);
	    savebtn.click();
	    Thread.sleep(3000);
	    //String text1 = "Ingest profile saved successfully";
	    String text1 = "User saved sucessfully";
	   	String text2 = ingestionpronotifica.getText();
	   	Assert.assertEquals(text1, text2);
	   	System.out.println("Profile created successfully");
	    driver.navigate().refresh();
   		searchtextfield.sendKeys(title1);
   		searchtextfield.sendKeys(Keys.ENTER);
	    Thread.sleep(2000);
	    String text3 = title1;
	    String text4 = verificationftppul.getText();
	    System.out.println(text3);
	    System.out.println(text4);
	    Assert.assertEquals(text3, text4);
	    System.out.println("Passed");
	 }
}
