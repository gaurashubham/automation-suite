package com.sfv.qa.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.sfv.qa.base.testbase;
 
public class Managementpage extends testbase
{
	@FindBy(xpath="//a[text()='User Setting']")
	 WebElement usersetting;
	
	@FindBy(xpath="//a[text()='Encoding Profile']")
	 WebElement encodingpro;
	
	@FindBy(xpath="//a[text()='Thumbnail Profile']")
	 WebElement thumbnailprofile;
	
	@FindBy(xpath="//a[text()='Syndication Settings']")
	 WebElement Syndicatesetting;
	
	@FindBy(xpath="//a[text()='Ingestion Profile']")
	 WebElement ingestionprofile;
	
	@FindBy(xpath="//th[@class='ng-binding']")
	 WebElement accountid;
	
	@FindBy(xpath="(//td[@class='ng-binding'])[1]")
	 WebElement accountname;
	
	@FindBy(xpath="(//td[@class='ng-binding'])[2]")
	 WebElement accountalias;
	
	@FindBy(xpath="(//td[@class='ng-binding'])[3]")
	 WebElement apikey;
	
	@FindBy(xpath="//a[text()='Thumbnail Profile']")
	 WebElement creationthumbnailprofiletab;
	
	@FindBy(xpath="//a[@sfv-acl='create-thumbnail-profile']")
	 WebElement createthumbnailprofilebutton;
	
	@FindBy(xpath="(//a[contains(@class, 'astro-group-btn') and contains(@class ,'col-xs-4')])[1]")
	 WebElement aspectratio;
	
	@FindBy(xpath="//input[@id='width']")
	 WebElement width;
	
	@FindBy(xpath="//input[@id='height']")
	 WebElement height;
	
	@FindBy(xpath="(//a[contains(@class, 'astro-group-btn') and contains(@class ,'col-xs-6')])[1]")
	 WebElement format;
		
	@FindBy(xpath="//button[text()='Save']")
	 WebElement savebtn;
	
	@FindBy(xpath="//div[text()='Thumbnail profile saved successfully.']")
	 WebElement thumbnailnoti;
			
	@FindBy(xpath="(//td[@class='ng-binding'])[4]")
	 WebElement thumbnaillist;	
	
	@FindBy(xpath="(//a[text()='Delete'])[1]")
	 WebElement thumbnailprofiledelete;
	
	@FindBy(xpath="//button[@data-bb-handler='confirm']")
	 WebElement OKbtn;
	
	@FindBy(xpath="//div[text()='Thumbnail profile deleted successfully.']")
	 WebElement notificationmsg;
	
	@FindBy(xpath="//div[text()='Thumbnail profile saved successfully.']")
	 WebElement notificationcreation;
	
					
	
	// Initializing the Page Objects:
		public Managementpage() 
		   {
		       PageFactory.initElements(driver, this);
		   }
		
		public Usersettingpage clickonusersetting()
		{
			usersetting.click();
			return new Usersettingpage();
		} 	
		
		public Encodingprofile clickonencodingprofile()
		{
			encodingpro.click();
			return new Encodingprofile();
		} 
		
		public Createplaylistpage clickonthumbnailprofile()
		{
			thumbnailprofile.click();
			return new Createplaylistpage();
		} 
		
		public Syndicationset clickonsyndicatesetting()
		{
			Syndicatesetting.click();
			return new Syndicationset();
		} 
		
		public Ingestionprofilepage clickoningestionprofilepage()
		{
			ingestionprofile.click();
			return new Ingestionprofilepage();
		} 
		
		public void creationthumbnailprofile(String widthvalue,String heightvalue,String thumbnailoption)
		{
			creationthumbnailprofiletab.click();
			createthumbnailprofilebutton.click();
			aspectratio.click();
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			
			width.clear();
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);	
			width.sendKeys(widthvalue);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			height.clear();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			height.sendKeys(heightvalue);
			format.click();
			savebtn.click();
			String text1 = thumbnailnoti.getText();
			String text2 = "Thumbnail profile saved successfully.";
			Assert.assertEquals(text1, text2);
			String text3 = thumbnaillist.getText();
			String text4 = thumbnailoption;
			System.out.println(text3);
			System.out.println(text4);
			Assert.assertEquals(text3, text4);
			System.out.println("Thumbnail profile created");
		}
		
		public void deletethumbnailprofile() throws InterruptedException
		{
			creationthumbnailprofiletab.click();
			thumbnailprofiledelete.click();
			Thread.sleep(5000);
			OKbtn.click();
		    String text1 = notificationmsg.getText();
			String text = "Thumbnail profile deleted successfully.";
			Assert.assertEquals(text1, text);
			System.out.println("Thumbnail profile deleted successfully");
		}
		
		
	
	

}
