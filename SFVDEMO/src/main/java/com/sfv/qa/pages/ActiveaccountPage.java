package com.sfv.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.sfv.qa.base.testbase;

public class ActiveaccountPage extends testbase
{
	
	 @FindBy(xpath="//div[@title='A1AB']")
	 WebElement A1ABaccount;
    
    @FindBy(xpath="//div[@title='Arena (Digital 5)']")
	 WebElement ArenaD5account;
    
    @FindBy(xpath="//div[@title='Astro Awani']")
	 WebElement AstroAwaniaccount ;
    
    @FindBy(xpath="//div[@title='Astro Content Sales and Distribution']")
	 WebElement Astrocontentaccount;
    
    @FindBy(xpath="//div[@title='Astro Radio']")
	 WebElement Astroradioaccount;
    
    @FindBy(xpath="//div[@title='Astro Test Account']")
	 WebElement Astrotestaccount;
    
    @FindBy(xpath="//div[@title='AstroCeria (Digital 5)']")
	 WebElement Astroceriaaccount;
    
    @FindBy(xpath="//div[@title='AstroEnt']")
	 WebElement AstroEntaccount;
    
    @FindBy(xpath="//div[@title='AstroFTA (Digital 5)']")
	 WebElement AstroFTAaccount;
    
    @FindBy(xpath="//div[@title='BlazeclanTest']")
	 WebElement Blazeclanaccount;
    
    @FindBy(xpath="//div[@title='CEP (Digital 5)']")
	 WebElement CEPaccount;
    
    @FindBy(xpath="//div[@title='Digital 5']")
	 WebElement Digital5account;
    
    @FindBy(xpath="//div[@title='Facebook Syndication Test']")
	 WebElement Fbsyndicationaccount;
    
    @FindBy(xpath="//div[@title='Hibur (Digital 5)']")
	 WebElement Hiburaccount;
    
    @FindBy(xpath="//div[@title='HomeShopping']")
	 WebElement Homeshopaccountbtn;
    
    @FindBy(xpath="//div[@title='Hotspot app testing']")
	 WebElement hotspotaccountbtn;
    
    @FindBy(xpath="//div[@title='IEP (Digital 5)']")
	 WebElement IEPaccountbtn;
    
    @FindBy(xpath="//div[@title='ImamMuda3 (Digital 5)']")
	 WebElement ImamMuda3accountbtn;
    
    @FindBy(xpath="//div[@title='KilauanEmas (Digital 5)']")
	 WebElement Kilauanaccountbtn;
    
    @FindBy(xpath="//div[@title='MLPXuan']")
	 WebElement MLPaccountbtn;
    
    @FindBy(xpath="//div[@title='Murai']")
	 WebElement Muraiaccountbtn;
    
    @FindBy(xpath="//div[@title='News Link']")
	 WebElement Newsaccountbtn;
    
    @FindBy(xpath="//div[@title='Packtpub - Demo Account']")
	 WebElement Packtpubaccountbtn;
    
    @FindBy(xpath="//div[@title='Parentsday (Digital 5)']")
	 WebElement Parentsdayaccountbtn;
    
    @FindBy(xpath="//div[@title='Raku']")
	 WebElement Rakuaccountbtn;
    
    @FindBy(xpath="//div[@title='RecipeApp (Digital 5)']")
	 WebElement Recipeaccountbtn;
    
    @FindBy(xpath="//div[@title='Salam Muslim (Islamic Portal)']")
	 WebElement Salamaccountbtn;
    
    @FindBy(xpath="//div[@title='Satyam']")
	 WebElement satyamaccountbtn;
    
    @FindBy(xpath="//div[@title='Stadium Astro (Digital 5)']")
	 WebElement stadiumaccountbtn;
    
    @FindBy(xpath="//div[@title='TutorTV (Digital 5)']")
	 WebElement tutoraccountbtn;
    
    @FindBy(xpath="//div[@title='Zon Lawak']")
	 WebElement zonaccountbtn;
    
    @FindBy(xpath="//div[@title='goshop']")
	 WebElement goshopaccountbtn;
    //Test
    @FindBy(xpath="//div[@title='iSooka']")
	 WebElement iSookaaccountbtn;
    
    @FindBy(xpath="//div[@title='old AstroAwani']")
	 WebElement oldaccountbtn;
    
    @FindBy(xpath="//div[@title='//div[@title='test account roma']")
     WebElement testromaaccountbtn;
    
    @FindBy(xpath="//div[@title='Test Account']")
    WebElement testaccount;
    
    @FindBy(xpath="//div[@title='TestingAccount']")
    WebElement testingaccount;
    
   // Initializing the Page Objects:
    public ActiveaccountPage() 
    {
        PageFactory.initElements(driver, this);
    }
    
    public Blazeclanpage clickOnButton() 
	{
    	testaccount.click();
		return new Blazeclanpage();
	}
    
    public Hotspotapptestingpage clickonhotspotbtn()
    {
    	hotspotaccountbtn.click();
		return new Hotspotapptestingpage();
    	
    }
 }
