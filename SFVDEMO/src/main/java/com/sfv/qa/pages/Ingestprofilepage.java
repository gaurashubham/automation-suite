package com.sfv.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.sfv.qa.base.testbase;

public class Ingestprofilepage extends testbase
{
	@FindBy(xpath="//a[@ng-click='showCreateUpdateIngestionProfile()']")
	 WebElement createingestionprobtn;
	
	@FindBy(xpath="//input[@placeholder='Search']")
	 WebElement searchtab;
	
	// Initializing the Page Objects:
				public Ingestprofilepage() 
				   {
				       PageFactory.initElements(driver, this);
				   }
				
				public Createingestionprofilepage clickoncreateingestprofilebtn()
				{
					createingestionprobtn.click();
					return new  Createingestionprofilepage();
				} 	
		
				public void clickonsearchbtn()
				{
					searchtab.click();
					
				} 	
}
