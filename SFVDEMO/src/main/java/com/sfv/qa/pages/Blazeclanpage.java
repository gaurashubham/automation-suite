package com.sfv.qa.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.sfv.qa.base.testbase;

public class Blazeclanpage extends testbase
{
	@FindBy(xpath="//a[@class='pointer']")
	 WebElement dashboardtab;
	
	@FindBy(xpath="//a[text()='Media']")
	 WebElement mediatab;
	
	@FindBy(xpath="//a[text()='Playlist']")
	 WebElement playlisttab;
	
	@FindBy(xpath="//a[text()='Upload']")
	 WebElement uploadtab;
	
	@FindBy(xpath="//a[text()='Tag']")
	 WebElement tagtab;
	
	@FindBy(xpath="//a[text()='Applications']")
	 WebElement applitab;
	
	@FindBy(xpath="//a[text()='Management']")
	 WebElement managementab;
	
	@FindBy(xpath=".//*[@id='headermenu']/li/a")
	 WebElement dashpagetitle;
	
	@FindBy(xpath="(//a[@href='#!/create-playlist'])[1]")
	 WebElement createplaybtn;
	
	@FindBy(xpath="//button[text()='Download']")
	 WebElement downbtn;
	
	
	// Initializing the Page Objects:
      public Blazeclanpage() 
      {
         PageFactory.initElements(driver, this);
      }
      
      public Dashboardpage clickOndashboarbtab()
      {
    	  dashboardtab.click();
    	 
          return new Dashboardpage();
      }  
      
      public  Mediapage clickOnmediatab()
      {
    	 mediatab.click();
    	 return new Mediapage();
      }  
      
      public Playlistpage clickOnplaylisttab()
      {
    	  playlisttab.click();
    	 // String element1 = createplaybtn.getText();
    	  return new Playlistpage();
      }  
      
      public Uploadpage clickOnuploadtab() throws InterruptedException
      {
    	  Thread.sleep(1000);
    	  uploadtab.click();
         return new Uploadpage();
    	 
      }  
      
      public  Tagpage clickOntagtab()
      {
    	  tagtab.click();
          return new Tagpage();
      }  
      
      public  Applicationpage clickOnapplitab()
      {
    	  applitab.click();
          return new Applicationpage();
      }  
      
      public Managementpage clickOnmanagementab()
      {
    	  managementab.click();
          return new Managementpage();
      }  
      
     
}
