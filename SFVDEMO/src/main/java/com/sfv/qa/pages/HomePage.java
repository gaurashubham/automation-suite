package com.sfv.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.sfv.qa.base.testbase;

public class HomePage extends testbase
{
	@FindBy(xpath="//span[text()='Smita Pote']")
	 WebElement Username;
	
	@FindBy(xpath = "//span[@ng-click='changePassword()']")
    WebElement changepasswordtab;
	
	@FindBy(xpath="//a[@class='pointer']")
	WebElement Allaccountstab;
	
    @FindBy(xpath="//span[@ng-click='logOut()']")
	 WebElement logouttab;
    
    @FindBy(xpath="//a[@class='btn menu-btn ng-binding active']")
	 WebElement Activeaccountbtn;
    
    @FindBy(xpath="//a[@class='btn menu-btn ng-binding']")
	 WebElement Inactiveaccountbtn;
    
    @FindBy(xpath="//a[text()='Manage Accounts']")
	 WebElement manageaccountbtn;
    
    @FindBy(xpath="//input[@placeholder='What are you looking for?']")
	 WebElement searchbox;
    
    @FindBy(xpath="//div[@title='Arena (Digital 5)']")
	 WebElement inactiveaccountname;
  
  //div[@title='roma-_@&']
  //div[@title='Arena (Digital 5)']  
  //span[text()='Smita Pote']
  //span[text()='Hitesh Kumar']
   
 // Initializing the Page Objects:
     public HomePage() 
     {
         PageFactory.initElements(driver, this);
     }
     
     
     public String verifyHomePageTitle()
     {
          return driver.getTitle();
     }

     public boolean verifyCorrectUserName()
     {
          return Username.isDisplayed();
 	 }

	public ActiveaccountPage clickOnActiveAccountButton()
	{
		Activeaccountbtn.click();
		return new ActiveaccountPage();
	}
    
	public Inactiveaccountpage clickOnInActiveAccBtn()
	{
		Inactiveaccountbtn.click();
		return new Inactiveaccountpage();
	}
	
	public Managementpage clickOnmanageacccbtn()
	{
		manageaccountbtn.click();
		return new Managementpage();
	}

	public Changepasswordpage clickOnchangepasstab()
	{
		changepasswordtab.click();
		return new Changepasswordpage();
	}
    
	public void clickOnlogoutbtn()
	{
		logouttab.click();
	}
	
	public void clickoninactiveaccounts()
	{
		Inactiveaccountbtn.click();
		boolean t1 = inactiveaccountname.isDisplayed();
		System.out.println(t1);
	}
	
	
}
    
	 
	


