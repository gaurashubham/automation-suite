package com.sfv.qa.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.sfv.qa.base.testbase;
import com.sfv.qa.util.TestUtil;

public class Createuserpage extends testbase
{
	@FindBy(xpath="//input[@id='email']")
	 WebElement emailaddress;
	
	@FindBy(xpath="//input[@id='password']")
	 WebElement passwordfield;
	
	@FindBy(xpath="//input[@id='firstName']")
	 WebElement firstname;
	
	@FindBy(xpath="//input[@id='cpassword']")
	 WebElement confirpassword;

	@FindBy(xpath="//input[@id='lastName']")
	 WebElement lastnames;
	
	@FindBy(xpath="//input[@id='contactNumber']")
	 WebElement contactnum;
	
	@FindBy(xpath="//select[@id='userRole']")
	 WebElement userrole;
	
	@FindBy(xpath="//label[@for='status']")
	 WebElement status;
	
	@FindBy(xpath="//a[@ng-click='showAccountAccessPopup()']")
	 WebElement accountaccess;
	
	@FindBy(xpath="//button[text()='Save']")
	 WebElement savebtn;
	
	@FindBy(xpath="//button[text()='Cancel']")
	 WebElement cancelbtn;
	
	@FindBy(xpath="//input[@name='search']")
	 WebElement searchbtn;
	
	@FindBy(xpath="(//a[@class='ng-binding'])[1]")
	 WebElement searchfield;
	
	@FindBy(xpath="//div[@class='search-form']")
	 WebElement searchenter;
	
	@FindBy(xpath="(//input[@ng-model='item.selected'])[1]")
	 WebElement accountac1;
	 
	@FindBy(xpath="//button[text()='Select All']")
	 WebElement selectallbtn;
	
	@FindBy(xpath="//button[text()='Confirm']")
	 WebElement confirmbtn;
	
	
	
	
	public Createuserpage() 
	   {
	       PageFactory.initElements(driver, this);
	   }
	
	public void creuser(String emailadd, String pass,String conpass,String fname,String lastname,String contactno) throws InterruptedException, AWTException
   {
		String emailaddid = emailadd + TestUtil.GetCurrentDateAndTimeAppFormat() + "@gmail.com";
		emailaddress.sendKeys(emailaddid);
		passwordfield.sendKeys(pass);
	    confirpassword.sendKeys(conpass);
		firstname.sendKeys(fname);
		lastnames.sendKeys(lastname);
		contactnum.sendKeys(contactno);
		
		
		Select sel = new Select(userrole);
		sel.selectByVisibleText("ADMIN");
		
		status.click();
		for (int second = 0;; second++) 
	     {
	            if(second >=3)
	            {
	               break;
	            } 
	       JavascriptExecutor jse = (JavascriptExecutor)driver;
	       jse.executeScript("window.scrollBy(0,100)");
	       Thread.sleep(3000);
	     }  
	   	
	   	savebtn.click();
	   	Thread.sleep(6000);
	   	
        searchbtn.click();
		searchbtn.sendKeys(emailaddid);
		/*Robot robot = new Robot();
        
		robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);*/
		Thread.sleep(5000);
		searchbtn.sendKeys(Keys.ENTER);
        Thread.sleep(5000);
		
        
		String ele1 = searchfield.getText();
		Thread.sleep(5000);
		System.out.println(ele1);
		System.out.println(emailaddid);
		Assert.assertEquals(emailaddid, ele1);
		System.out.println("User found");
     }
	
	public void creuserole(String emailadd, String pass,String conpass,String fname,String lastname,String contactno) throws InterruptedException, AWTException
	   {
		    String emailaddid = emailadd + TestUtil.GetCurrentDateAndTimeAppFormat() + "@gmail.com";
			emailaddress.sendKeys(emailaddid);
			passwordfield.sendKeys(pass);
		    confirpassword.sendKeys(conpass);
			firstname.sendKeys(fname);
			lastnames.sendKeys(lastname);
			contactnum.sendKeys(contactno);
			
			
			Select sel = new Select(userrole);
			sel.selectByVisibleText("USER");
			
			status.click();
			Thread.sleep(3000);
			accountaccess.click();
			Thread.sleep(3000);
			accountac1.click();
			Thread.sleep(3000);
			selectallbtn.click();
			Thread.sleep(3000);
			confirmbtn.click();
			for (int second = 0;; second++) 
		     {
		            if(second >=3)
		            {
		               break;
		            } 
		       JavascriptExecutor jse = (JavascriptExecutor)driver;
		       jse.executeScript("window.scrollBy(0,100)");
		       Thread.sleep(3000);
		     }  
		   	
		   	savebtn.click();
		   	Thread.sleep(6000);
		   	
	        searchbtn.click();
			searchbtn.sendKeys(emailaddid);
			searchbtn.sendKeys(Keys.ENTER);
	        Thread.sleep(2000);
			String ele2 = searchfield.getText();
			Thread.sleep(5000);
			System.out.println(ele2);
			System.out.println(emailaddid);
			Assert.assertEquals(emailaddid, ele2);
			System.out.println("User found");
			
		}
	
	

	
	
}
