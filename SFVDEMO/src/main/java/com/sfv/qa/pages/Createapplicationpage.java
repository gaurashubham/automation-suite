package com.sfv.qa.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.sfv.qa.base.testbase;

public class Createapplicationpage extends testbase
{
	@FindBy(xpath="//input[@id='applicationname']")
	 WebElement applicationname;
	
	@FindBy(xpath="//input[@id='applicationalias']")
	 WebElement applicationalias;
	
	@FindBy(xpath="//input[@id='website']")
	 WebElement website;
	
   @FindBy(xpath="//input[@id='playerkey']")
	 WebElement playerkey;
	
	@FindBy(xpath="//input[@id='adtag']")
	 WebElement adtag ;
	
	@FindBy(xpath="//textarea[@id='Insert video description here']")
	 WebElement description;
	
	@FindBy(xpath="//input[@id='gtmId']")
	 WebElement gtmid ;
	
	@FindBy(xpath="(//label[@style='float:left'])[2]")
	 WebElement activebtn;
	
	@FindBy(xpath="//button[@class='btn btn-astro-default col-100']")
	 WebElement addcountrybtn;
	
	@FindBy(xpath="//button[text()='Save']")
	 WebElement savebtn ;
	
	@FindBy(xpath="//input[@name='search']")
	 WebElement searchbtn;
	
	@FindBy(xpath="(//a[@class='ng-binding'])[1]")
	 WebElement searchfield;
	
	@FindBy(xpath="(//button[text()='Delete'])[1]")
	 WebElement deletebtn;
	
	@FindBy(xpath="//button[text()='OK']")
	 WebElement okbtn;
	
	@FindBy(xpath="//div[text()='Application Deleted successfully']")
	 WebElement deletedmessage;
	
	@FindBy(xpath="//td[text()='No Result Found']")
	 WebElement noresultfound;
	
	
// Initializing the Page Objects:
   public Createapplicationpage() 
   {
       PageFactory.initElements(driver, this);
   }
   
   public void createNewappli(String appliname, String applialias,String web,String plakey,String descrip,String adtagtext,String gtmidtext) throws InterruptedException, AWTException
   {
     
	   applicationname.sendKeys(appliname);
	   applicationalias.sendKeys(applialias);   
	   website.sendKeys(web);
	   playerkey.sendKeys(plakey);
	   //adtag.sendKeys(adtag1);
	   description.sendKeys(descrip);
	   adtag.sendKeys(adtagtext);
	   gtmid.sendKeys(gtmidtext);
	   String element1 = applialias;
	  
	   
   	activebtn.click();
   	 
   	for (int second = 0;; second++) 
     {
            if(second >=3)
            {
               break;
            } 
       JavascriptExecutor jse = (JavascriptExecutor)driver;
   	   jse.executeScript("window.scrollBy(0,100)");
       Thread.sleep(3000);
     }  
   	
   	savebtn.click();
   	Thread.sleep(8000);
   	searchbtn.click();
	searchbtn.sendKeys(element1);
	searchbtn.sendKeys(Keys.ENTER);
    Thread.sleep(2000);
    
    String ele1 = searchfield.getText();
	System.out.println(ele1);
	System.out.println(element1);
	Assert.assertEquals(element1, ele1);
	System.out.println("Application found");
	deleteapplication(element1);
	
}
   
   public void deleteapplication(String element1) throws InterruptedException
   {
	    searchbtn.clear();
	    Thread.sleep(2000);
		searchbtn.click();
		Thread.sleep(2000);
		searchbtn.sendKeys(element1);
		searchbtn.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		deletebtn.click();
		Thread.sleep(2000);
		Actions action = new Actions(driver);
		action.moveToElement(okbtn).build().perform();
		okbtn.click();
		Thread.sleep(2000);
		String noti = deletedmessage.getText();
		Thread.sleep(2000);
		String notif = "Application Deleted successfully";
		Thread.sleep(2000);
		Assert.assertEquals(noti, notif);
		Thread.sleep(2000);
		searchbtn.click();
		Thread.sleep(2000);
		searchbtn.clear();
		Thread.sleep(2000);
		searchbtn.sendKeys(element1);
		Thread.sleep(2000);
		searchbtn.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
	    String notification = noresultfound.getText();
	    String notification1 = "No Result Found";
		Assert.assertEquals(notification, notification1);
		
   } 
  
	
   
}
