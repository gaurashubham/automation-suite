package com.sfv.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.sfv.qa.base.testbase;

public class Changepasswordpage extends testbase
{
	@FindBy(xpath="//span[@ng-click='changePassword()']")
	 WebElement changepasstab;
	
	@FindBy(xpath="//input[@name='password']")
	 WebElement oldpasstext;
	
	@FindBy(xpath="//input[@name='newpassword']")
	 WebElement newpasstext;
	
	@FindBy(xpath="//input[@name='changePassword']")
	 WebElement confirmpasstext;
	
	@FindBy(xpath="//button[text()='Save']")
	 WebElement savebtn;
	
	@FindBy(xpath="//div[@class='alert ui-pnotify-container alert-danger ui-pnotify-shadow']")
	 WebElement messagenoti;
	
	@FindBy(xpath="//span[text()='Password and confirm password should match']")
	 WebElement wrongconnew;
	
	@FindBy(xpath="//button[text()='Cancel']")
	 WebElement cancelbtn;
	
	// Initializing the Page Objects:
    public Changepasswordpage() 
    {
        PageFactory.initElements(driver, this);
    }
    
    public void validchangepass(String oldpass,String newpass,String confirmpass) throws InterruptedException
    {
    	Thread.sleep(6000);
    	changepasstab.click();
    	oldpasstext.sendKeys(oldpass);
    	newpasstext.sendKeys(newpass);
    	confirmpasstext.sendKeys(confirmpass);
    	savebtn.click();
    }
    
    public void incorrectoldpass(String oldpass,String newpass,String confirmpass) throws InterruptedException
    {
    	Thread.sleep(6000);
    	changepasstab.click();
    	oldpasstext.sendKeys(oldpass);
    	newpasstext.sendKeys(newpass);
    	confirmpasstext.sendKeys(confirmpass);
    	savebtn.click();
    	Thread.sleep(2000);
    	String text1 = messagenoti.getText();
    	String text2 = "Invalid current password.";
    	Assert.assertEquals(text2, text1);
    	cancelbtn.click();
    	
    }
	 
    public void mismatchnewncon(String oldpass,String newpass,String confirmpass) throws InterruptedException
    {
    	Thread.sleep(6000);
    	changepasstab.click();
    	oldpasstext.sendKeys(oldpass);
    	newpasstext.sendKeys(newpass);
    	confirmpasstext.sendKeys(confirmpass);
    	savebtn.click();
    	Thread.sleep(2000);
    	String text1 = wrongconnew.getText();
    	String text2 = "Password and confirm password should match";
    	Assert.assertEquals(text2, text1);
    	cancelbtn.click();
    }
	
	
}
