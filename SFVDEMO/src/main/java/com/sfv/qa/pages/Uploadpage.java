package com.sfv.qa.pages;

import java.awt.AWTException;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.util.TestUtil;


public class Uploadpage extends testbase
{
	@FindBy(xpath="//i[@class='fa fa-upload']")
	 WebElement uploadbtn ;
	
	@FindBy(xpath="//input[@id='title']")
	 WebElement titlefield ;
	
	@FindBy(xpath="//textarea[@id='description']")
	 WebElement descriptionfield ;
	
	@FindBy(xpath="//input[@id='copyright']")
	 WebElement copyrightfield ;
	
	@FindBy(xpath="//button[text()='Save']")
	 WebElement savebtn;
	
	@FindBy(xpath="//button[text()='Cancel']")
	 WebElement cancelbtn;
	
	@FindBy(xpath="//th[@class='col-72 ng-binding']")
	 WebElement mediatid;
	
	@FindBy(xpath="(//span[@class='item-title pointer ng-binding'])[9]")
	 WebElement mediatobesyndicated;
	
	@FindBy(xpath="(//span[@class='ng-binding'])[3]")
	 WebElement mediatidnoti;
	
	@FindBy(xpath="//button[text()='Manage']")
	 WebElement managebtn;
	
	@FindBy(xpath="(//input[@name='checkbox'])[1]")
	 WebElement firstcheckbox;
	
	@FindBy(xpath="//button[contains(.,'Syndicate')]")
	 WebElement Syndicatebtn;
	
	@FindBy(xpath=" (//span[@class='notification-icon-count ng-binding'])[1]")
	 WebElement notificationbtn;
	
	@FindBy(xpath="(//a[text()='Youtube Video Published : '])[1]")
	 WebElement notificationdrop;
	
	@FindBy(xpath="//a[text()='Confirm']")
	 WebElement confirmbtn;
	
	@FindBy(xpath="//select[@ng-model='syndication.profileId']")
	 WebElement Syndicationprofile;
	
	@FindBy(xpath="//select[@ng-model='syndication.email']")
	 WebElement Syndicationprofilemail;
	
	@FindBy(xpath="//a[text()='Upload']")
	 WebElement uploadtab;
	
	@FindBy(xpath="(//span[@class='item-title pointer ng-binding'])[1]")
	 WebElement transcod;
	
	@FindBy(xpath="//a[text()='THUMBNAILS']")
	 WebElement thumbnailtab;
	
	@FindBy(xpath="(//button[text()='View thumbnail'])[1]")
	 WebElement viewthumb;
	
	@FindBy(xpath="//input[@id='directeDownloadURL']")
	 WebElement directdownloadurl;
	
	@FindBy(xpath="//input[@name='search']")
	 WebElement searchbtn;
	
	@FindBy(xpath="//div[text()='Please select a valid file. Allowed video file extensions are .webm, .mp4 & .mov.']")
	 WebElement errornotification;
	
	@FindBy(xpath="//button[text()='Download']")
	 WebElement downloadbtn;
	
	@FindBy(xpath="//div[text()='Title is required']")
	 WebElement titlenotification;
	
	@FindBy(xpath="//a[text()='DETAILS & TAGS']")
	 WebElement detailsandtagtab;
	
	@FindBy(xpath="//span[@class='btn btn-default form-control ui-select-toggle']")
	 WebElement tagdropdown;
	
	@FindBy(xpath="//input[@placeholder='eg. namespace:predicate=value']")
	 WebElement tagprofile;
	
	//button[@id='startTime']
	//button[@id='endTime']
	//input[@ng-model='time.seconds']
   
	// Initializing the Page Objects:
	public Uploadpage() 
   {
       PageFactory.initElements(driver, this);
   }
	
	 public void verificationofuploading(String path, String title,String description,String copyright,String startslot,String endslot) throws InterruptedException, AWTException
     {
		
		 Thread.sleep(5000);
         JavascriptExecutor js=(JavascriptExecutor) driver;
         WebElement fileInput = (WebElement) js.executeScript("return document.querySelector(\"input[name='file']\");");
         fileInput.sendKeys(path);
		 Thread.sleep(6000);
         WebElement element1 = driver.findElement(By.xpath("//div[text()='Video data saved successfully.']"));
  		/*WebDriverWait wait = new WebDriverWait(driver,2);
  		wait.until(ExpectedConditions.textToBePresentInElement(element1, "Video data saved successfully."));*/
  		 String element2 = element1.getText();
  		 String element3 = "Video data saved successfully.";
  		 Assert.assertEquals(element2, element3);
  		 System.out.println("Successfully uploaded");
  		 new WebDriverWait(driver, 600)
		 .until(ExpectedConditions.visibilityOfElementLocated(By.id("title")));
			
		   JavascriptExecutor jse = (JavascriptExecutor)driver;
		   jse.executeScript("window.scrollBy(0,-250)", "");
	       Thread.sleep(3000);
		
		 String generatedmediaid = mediatid.getText();
		 System.out.println("Media ID : "  +element1);
			
			for (int second = 0;; second++) 
		      {
		             if(second >=3)
		             {
		                break;
		             } 
		        JavascriptExecutor jse1 = (JavascriptExecutor)driver;
		    	jse1.executeScript("window.scrollBy(0,100)");
		        Thread.sleep(3000);
		      }  	
		  //String title1 = title + TestUtil.GetCurrentDateAndTimeAppFormat();	
	      titlefield.sendKeys(title);
	      descriptionfield.sendKeys(description);
	      copyrightfield.sendKeys(copyright);
	      savebtn.click();
	      Thread.sleep(30000);
	      driver.navigate().refresh();
	
	      //driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
	      driver.findElement(By.xpath("//a[text()='Media']")).click();
	      Thread.sleep(2000);
          searchbtn.click();
          searchbtn.sendKeys(generatedmediaid);
          searchbtn.sendKeys(Keys.ENTER);
          Thread.sleep(3000);
	
	      WebElement newMedia = (new WebDriverWait(driver, 30))
	      .until(ExpectedConditions.presenceOfElementLocated(
	      By.xpath("(//span[@class='item-title pointer ng-binding'])[1]")));

	      System.out.println("Media title :" + newMedia.getText());
	      Assert.assertEquals(title, newMedia.getText());
	      System.out.println("Assertion:Media uploaded successfully");
	      Mediapage mediapage = new Mediapage();
	      mediapage.mediapreview(startslot, endslot);
        
     }


	
   public Mediapage uploadmedia(String path, String title,String description,String copyright) throws InterruptedException, AWTException
				 
   {
	                         // uploadbtn.click();
	                        //  String filePathchrome1 = System.getProperty("user.dir") + path;
		     				  Thread.sleep(5000);
	                          JavascriptExecutor js=(JavascriptExecutor) driver;
		                      WebElement fileInput = (WebElement) js.executeScript("return document.querySelector(\"input[name='file']\");");
		                      fileInput.sendKeys(path);
		     					
		                      
		     					new WebDriverWait(driver, 600)
		     					.until(ExpectedConditions.visibilityOfElementLocated(By.id("title")));
		     					
		     					JavascriptExecutor jse = (JavascriptExecutor)driver;
		     				   jse.executeScript("window.scrollBy(0,-250)", "");
		     			        Thread.sleep(3000);
		     				
		     				 String element1 = mediatid.getText();
		     				 System.out.println("Media ID : "  +element1);
		     					
		     					for (int second = 0;; second++) 
		     				      {
		     				             if(second >=3)
		     				             {
		     				                break;
		     				             } 
		     				        JavascriptExecutor jse1 = (JavascriptExecutor)driver;
		     				    	jse1.executeScript("window.scrollBy(0,100)");
		     				        Thread.sleep(3000);
		     				      }  
		     					
		     				//String title1 = title + TestUtil.GetCurrentDateAndTimeAppFormat();	
					    	titlefield.sendKeys(title);
					    	descriptionfield.sendKeys(description);
					    	copyrightfield.sendKeys(copyright);
					    	savebtn.click();
					    	Thread.sleep(30000);
					    	driver.navigate().refresh();
					    	
					    	//driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
					    	driver.findElement(By.xpath("//a[text()='Media']")).click();
					    	Thread.sleep(2000);
					        searchbtn.click();
					        searchbtn.sendKeys(element1);
					        Robot robot = new Robot();
					        robot.keyPress(KeyEvent.VK_ENTER);
					        robot.keyRelease(KeyEvent.VK_ENTER);
					        Thread.sleep(3000);
					    	
					    	WebElement newMedia = (new WebDriverWait(driver, 30))
							.until(ExpectedConditions.presenceOfElementLocated(
							By.xpath("(//span[@class='item-title pointer ng-binding'])[1]")));

							System.out.println("Media title :" + newMedia.getText());
							Assert.assertEquals(title, newMedia.getText());
							System.out.println("Assertion:Media uploaded successfully");
					    	
		  return new Mediapage();
	} 
   
   public Mediapage syndicateendtoend(String path, String title,String description,String copyright,String syndiprofile,String syndiprofilemail) throws InterruptedException
	 
   {                       
	                    //  uploadbtn.click();
	                     // String filePathchrome1 = System.getProperty("user.dir") + path;
     		              Thread.sleep(5000);
	                      JavascriptExecutor js=(JavascriptExecutor) driver;
		                  WebElement fileInput = (WebElement) js.executeScript("return document.querySelector(\"input[name='file']\");");
		                  fileInput.sendKeys(path);
		     					
		     					Thread.sleep(15000);
		     					new WebDriverWait(driver, 100)
		     					.until(ExpectedConditions.visibilityOfElementLocated(By.id("title")));
		     					
		     					    JavascriptExecutor jse = (JavascriptExecutor)driver;
		     					   jse.executeScript("window.scrollBy(0,-250)", "");
		     				        Thread.sleep(8000);
		     					
		     					 String element1 = mediatid.getText();
		     					 System.out.println("Media ID : "  +element1+" -");
		     					Thread.sleep(3000);
		     					 String element2 = element1+" -";
		     					System.out.println("Media ID : "  +element2);
		     					
		     					for (int second = 0;; second++) 
		     				      {
		     				             if(second >=3)
		     				             {
		     				                break;
		     				             } 
		     				        JavascriptExecutor jse1 = (JavascriptExecutor)driver;
		     				    	jse1.executeScript("window.scrollBy(0,100)");
		     				        Thread.sleep(3000);
		     				      }  	
		     				String 	title1 = title + TestUtil.GetCurrentDateAndTimeAppFormat();
					    	titlefield.sendKeys(title1);
					    	descriptionfield.sendKeys(description);
					    	copyrightfield.sendKeys(copyright);
					    	savebtn.click();
					    	Thread.sleep(30000);
					    	
					    	//driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
					    	driver.findElement(By.xpath("//a[text()='Media']")).click();
							 
							   
					         driver.navigate().refresh();
					         Thread.sleep(6000);
					         driver.findElement(By.xpath("//a[text()='Media']")).click();
					         new WebDriverWait(driver, 100)
					         .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[text()='Manage']")));
							 managebtn.click();
							 Thread.sleep(5000);
							 searchbtn.click();
						     searchbtn.sendKeys(element1);
						     searchbtn.sendKeys(Keys.ENTER);
						     Thread.sleep(3000);
							 firstcheckbox.click();
							 Thread.sleep(5000);
							 
							 Syndicatebtn.click();
					         Select select = new Select(Syndicationprofile);
					         select.selectByVisibleText(syndiprofile);
							 
					         Select select1 = new Select(Syndicationprofilemail);
					         select1.selectByVisibleText(syndiprofilemail);
					         
					         confirmbtn.click();
					         driver.manage().timeouts().implicitlyWait(300,TimeUnit.SECONDS) ;
					         Thread.sleep(300000);
					        /* WebDriverWait wait = new WebDriverWait(driver,100);
					         wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='1']")));*/
					         
					        /*wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//span[@class='notification-icon-count ng-binding'])[1]")));*/
					         driver.navigate().refresh();
					         notificationbtn.click();
					         Thread.sleep(5000);
					         System.out.println(notificationdrop.getText());
					         Thread.sleep(5000);
					         String elem = notificationdrop.getText();
					         Thread.sleep(5000);
					         String mediaidele = mediatidnoti.getText();
					         System.out.println("Media ID 1 :"+"  " +mediaidele);
					         System.out.println("Media ID 2 :"+"  " +element2);
					         Assert.assertEquals(element2,mediaidele);
					        	 {
					        		 System.out.println("Video published successfully");
					        	 }
					        Mediapage mediapage = new Mediapage();
					        mediapage.checksyndicationprofileformedia(element1);
					        mediapage.syndicatedmediatab(element1); 
					         
	      return new Mediapage();
	} 
   
   public Mediapage transcoding(String path, String title,String description,String copyright,String tagsprofile) throws InterruptedException, AWTException
	{
	  
	  Thread.sleep(5000);
      JavascriptExecutor js=(JavascriptExecutor) driver;
      WebElement fileInput = (WebElement) js.executeScript("return document.querySelector(\"input[name='file']\");");
      fileInput.sendKeys(path);
		
		 new WebDriverWait(driver, 600)
		.until(ExpectedConditions.visibilityOfElementLocated(By.id("title")));
		 
		 JavascriptExecutor jse = (JavascriptExecutor)driver;
		   jse.executeScript("window.scrollBy(0,-250)", "");
	        Thread.sleep(3000);
		
		 String element1 = mediatid.getText();
		 System.out.println("Media ID : "  +element1);
		
		 for (int second = 0;; second++) 
	      {
	             if(second >=3)
	             {
	                break;
	             } 
	        JavascriptExecutor jse1 = (JavascriptExecutor)driver;
	    	jse1.executeScript("window.scrollBy(0,100)");
	        Thread.sleep(3000);
	      }  	
	 String title1 = title + TestUtil.GetCurrentDateAndTimeAppFormat();	
	 titlefield.sendKeys(title1);
	 descriptionfield.sendKeys(description);
	 copyrightfield.sendKeys(copyright);
	 savebtn.click();
	 Thread.sleep(120000);
	 driver.navigate().refresh();
	
	 //driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
	 driver.findElement(By.xpath("//a[text()='Media']")).click();
	 Thread.sleep(2000);
     searchbtn.click();
     searchbtn.sendKeys(element1);
     searchbtn.sendKeys(Keys.ENTER);
     Thread.sleep(3000);
	
	 WebElement newMedia = (new WebDriverWait(driver, 30))
	 .until(ExpectedConditions.presenceOfElementLocated(
	 By.xpath("(//span[@class='item-title pointer ng-binding'])[1]")));

	 System.out.println("Media title :" + newMedia.getText());
	 Assert.assertEquals(title1, newMedia.getText());
	 System.out.println("Assertion:Media uploaded successfully");
	 transcod.click();
	 Thread.sleep(3000);
	 thumbnailtab.click();
	 WebDriverWait wait = new WebDriverWait(driver,20);
	 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//button[text()='View thumbnail'])[1]")));
	 String text2 = viewthumb.getText();
	 String text1 = "View thumbnail";
	 System.out.println(text1);
	 Assert.assertEquals(text2, text1);
	 detailsandtagtab.click();
	 Thread.sleep(3000);
	 tagprofile.sendKeys(tagsprofile);
	 Thread.sleep(3000);
	 savebtn.click();
	 Mediapage mediapage = new Mediapage();
	 mediapage.checktagformedia(element1);
	 return new Mediapage();
	}
   
   public Mediapage uploadurl(String path,String title,String description,String copyright,String title1,String description1) throws AWTException, InterruptedException
  {
	  
	    directdownloadurl.click();
	    directdownloadurl.sendKeys(path);
	    downloadbtn.click();
	   
	    Thread.sleep(3000);
	    
         new WebDriverWait(driver, 600)
			.until(ExpectedConditions.visibilityOfElementLocated(By.id("title")));
         
         JavascriptExecutor jse = (JavascriptExecutor)driver;
		   jse.executeScript("window.scrollBy(0,-250)", "");
	        Thread.sleep(3000);
		
		 String element1 = mediatid.getText();
		 System.out.println("Media ID : "  +element1);
			
			for (int second = 0;; second++) 
		      {
		             if(second >=3)
		             {
		                break;
		             } 
		        JavascriptExecutor jse1 = (JavascriptExecutor)driver;
		    	jse1.executeScript("window.scrollBy(0,100)");
		        Thread.sleep(3000);
		      }  	
			
			String title2 = title + TestUtil.GetCurrentDateAndTimeAppFormat();
        	titlefield.sendKeys(title2);
	        descriptionfield.sendKeys(description);
	        copyrightfield.sendKeys(copyright);
	        savebtn.click();
	        Thread.sleep(105000);
	        driver.navigate().refresh();
	
	       //driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
	         driver.findElement(By.xpath("//a[text()='Media']")).click();
	         Thread.sleep(2000);
	         searchbtn.click();
	         searchbtn.sendKeys(element1);
	         searchbtn.sendKeys(Keys.ENTER);
	         Thread.sleep(3000);
	         
	         WebElement newMedia = (new WebDriverWait(driver, 30))
			.until(ExpectedConditions.presenceOfElementLocated(
			 By.xpath("(//span[@class='item-title pointer ng-binding'])[1]")));

	         System.out.println("Media title :" + newMedia.getText());
	         Assert.assertEquals(title2, newMedia.getText());
	         System.out.println("Assertion:Media uploaded successfully");
	         Mediapage mediapage = new Mediapage();
	         mediapage.multiplelanguage(element1, title1, description1);
	   
	 return new Mediapage();
	 
   
  }
   
   public void incorrectformat(String path) throws InterruptedException
   {
	   Thread.sleep(1000);
       JavascriptExecutor js=(JavascriptExecutor) driver;
       WebElement fileInput = (WebElement) js.executeScript("return document.querySelector(\"input[name='file']\");");
       fileInput.sendKeys(path);
	   Thread.sleep(2000);
	   String text1 = errornotification.getText();
	   String text2 = "Please select a valid file. Allowed video file extensions are .webm, .mp4 & .mov.";
	   System.out.println(text1);
	   Assert.assertEquals(text1, text2);
	   System.out.println("Passed");
   }
   
   public void nontranscoded(String path) throws InterruptedException
   {
	      String filePathchrome1 = System.getProperty("user.dir") + path;   
	     Thread.sleep(8000);
	     JavascriptExecutor js=(JavascriptExecutor) driver;
	     WebElement fileInput = (WebElement) js.executeScript("return document.querySelector(\"input[name='file']\");");
		 fileInput.sendKeys(path);
		 Thread.sleep(20000);
		 savebtn.click();
		 String text = titlenotification.getText();
		 System.out.println(text);
		 String text1 = "Title is required";
		 Assert.assertEquals(text, text1);
		 thumbnailtab.click();
	 	 WebDriverWait wait = new WebDriverWait(driver, 10);
		 wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("(//button[text()='View thumbnail'])[1]")));
		 System.out.println("Passed");
		 
   }
}
