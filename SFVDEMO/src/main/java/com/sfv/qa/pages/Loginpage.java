package com.sfv.qa.pages;

import org.openqa.selenium.WebElement;

//import org.eclipse.jetty.util.annotation.Name;
//import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.sfv.qa.base.testbase;

public class Loginpage extends testbase 
{
	@FindBy(id="email")
	WebElement username;

	@FindBy(id="password")
	WebElement password;
	
	@FindBy(xpath="//button[text()='SIGN IN']")
    WebElement SigninBtn;
	
	@FindBy(xpath="//a[text()='FORGOT PASSWORD?']")
	WebElement Forgotpassword;
	
	@FindBy(xpath="//img[@src='images/logo-big-black.png']")
	WebElement astrologo;
	
	@FindBy(xpath="//p[@style='margin: 5px 0 10px;']")
	WebElement invalidlogin;
	
	
	//Initializing the Page Objects:
	public Loginpage()
	{
       PageFactory.initElements(driver, this);
    }
	
	//Actions:
     public String validateLoginPageTitle()
     {
         return driver.getTitle();
     }

     public boolean validateAstroImage() throws InterruptedException
     {
    	 Thread.sleep(8000);
        return astrologo.isDisplayed();
     }
     
     public HomePage login(String un, String pwd)
     {
        username.sendKeys(un);
        password.sendKeys(pwd);
        SigninBtn.click();
        return new HomePage();
     }
     
     public void loginfail(String un, String pwd)
     {
        username.sendKeys(un);
        password.sendKeys(pwd);
        SigninBtn.click();
        String text1 = invalidlogin.getText();
        System.out.println(text1);
        String text2 = "Please enter valid username/password";
        System.out.println( text1  +  text2 );
        Assert.assertEquals(text2, text1);
     }
}
