package com.sfv.qa.pages;

import java.awt.AWTException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.util.TestUtil;

public class Dashboardpage extends testbase
{
	Uploadpage upload = new Uploadpage();		
	
	private class mediatitlecheck extends Uploadpage
	{
		String titleofmedia = "";
	}
	
	@FindBy(xpath="(//div[@class='analytics-data-col col-100'])[2]")
	 WebElement table;
	
	@FindBy(xpath ="//td//a[@class='ng-binding']")
     List<WebElement> videotitle;
	
	@FindBy(xpath ="//div[@class='btn-group dropdown']//button[@id='single-button']")
    WebElement sortbybutton ;
	
	@FindBy(xpath ="(//li[@role='menuitem']//a[text()='Recently Updated'])[2]")
   WebElement recentlyuploadbutton;
	
	@FindBy(xpath ="(//li[@role='menuitem']//a[text()='Alphabet (A-Z)'])[2]")
    WebElement alphabetically;
	
	@FindBy(xpath ="//a[text()='Media']")
    WebElement mediatab;
	
	@FindBy(xpath="//a[text()='Upload']")
	 WebElement uploadtab;
	
	@FindBy(xpath="(//span[@class='item-title pointer ng-binding'])[1]")
	 WebElement recentmedia;
	
	@FindBy(xpath="//button[@id='view-button']")
	 WebElement gridviewlistviewbutton;
	
	@FindBy(xpath="//div[text()='STATUS']")
	 WebElement statuscolumn;
	
	@FindBy(xpath="(//i[@class='fa fa-circle'])[1]")
	 WebElement  firststatus;
	
	@FindBy(xpath="//div[text()='THUMBNAIL']")
	 WebElement  thumbnailcolumn;
	
	@FindBy(xpath="(//span[text()='Active'])[1]")
	 WebElement  activebutton;
	
	
	
	
	// Initializing the Page Objects:
    public Dashboardpage() 
    {
        PageFactory.initElements(driver, this);
    }
    
    public void checkvideotitlelist()
	{
    	JavascriptExecutor js = (JavascriptExecutor) driver;
    	js.executeScript("window.scrollBy(0,1000)");
    	int j = videotitle.size();
    	System.out.println(j);
		table.isDisplayed();
		for (int i=0; i<videotitle.size(); i++)
		{
		   boolean t1 = videotitle.get(i).isDisplayed();
		   System.out.println(t1);
		   String title = videotitle.get(i).getText();
		   System.out.println("--------------------------------------------------------------");
		   System.out.println(title);
		   System.out.println("--------------------------------------------------------------");
		}
	}
    
    public void sortbyrecentlyupload(String path,String title,String description,String copyright) throws InterruptedException, AWTException
    {
    	uploadtab.click();
    	//String title1 = title + TestUtil.GetCurrentDateAndTimeAppFormat();
    	upload.uploadmedia(path, title, description, copyright);
    	mediatab.click();
    	driver.navigate().refresh();
    	WebDriverWait wait = new WebDriverWait(driver,10);
    	wait.until(ExpectedConditions.visibilityOf(sortbybutton));
    	sortbybutton.click();
    	wait.until(ExpectedConditions.visibilityOf(recentlyuploadbutton));
    	recentlyuploadbutton.click();
    	String mediatitle = recentmedia.getText();
    	driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
    	System.out.println(mediatitle);
    	Assert.assertEquals(mediatitle, title);
    	System.out.println("Sorted by recently upload");
    }
    
    public void sortbyalphabetically(String path,String title,String description,String copyright) throws InterruptedException, AWTException
    {
    	uploadtab.click();
    	upload.uploadmedia(path, title, description, copyright);
    	mediatab.click();
    	driver.navigate().refresh();
    	WebDriverWait wait = new WebDriverWait(driver,10);
    	wait.until(ExpectedConditions.visibilityOf(sortbybutton));
    	sortbybutton.click();
    	wait.until(ExpectedConditions.visibilityOf(alphabetically));
    	alphabetically.click();
    	String mediatitle = recentmedia.getText();
    	driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
    	System.out.println(mediatitle);
    	Assert.assertEquals(mediatitle, title);
    	System.out.println("Sorted by alphabetically upload");
    }
    
    public void gridviewandlistview()
    {
    	mediatab.click();
    	String text = gridviewlistviewbutton.getText();
    	System.out.println(text);
    	if(text.contains("Grid"))
    	{
    		boolean t1 = statuscolumn.isDisplayed();
    		System.out.println(t1);
    		boolean t2 = firststatus.isDisplayed();
    		System.out.println(t2);
    		boolean t3 = thumbnailcolumn.isDisplayed();
    		System.out.println(t3);
    		String text4 = thumbnailcolumn.getText();
    		System.out.println(text4);
    		System.out.println("Displayed in grid view");
    		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
    		gridviewlistviewbutton.click();
    		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
    		String text1 = gridviewlistviewbutton.getText();
    		if(text1.contains("List"))
    		{
    			boolean t4 = activebutton.isDisplayed();
        		System.out.println(t4);
        		System.out.println("Displayed in list view");
    		}
    		
    	}
    	else
    	{
    		gridviewlistviewbutton.click();
    		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
    		String text2 = gridviewlistviewbutton.getText();
    		System.out.println(text2);
    		boolean t1 = statuscolumn.isDisplayed();
    		System.out.println(t1);
    		boolean t2 = firststatus.isDisplayed();
    		System.out.println(t2);
    		boolean t3 = thumbnailcolumn.isDisplayed();
    		System.out.println(t3);
    		String text4 = thumbnailcolumn.getText();
    		System.out.println(text4);
    	    System.out.println("Displayed in grid view");
    		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
    		gridviewlistviewbutton.click();
    		String text1 = gridviewlistviewbutton.getText();
    		if(text1.contains("List"))
    		{
    			boolean t4 = activebutton.isDisplayed();
        		System.out.println(t4);
        		System.out.println("Displayed in list view");
    		}
    	}
    }
	
  
	
}
