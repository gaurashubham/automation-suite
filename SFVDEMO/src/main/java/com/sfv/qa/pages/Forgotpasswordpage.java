package com.sfv.qa.pages;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.sfv.qa.base.testbase;
import com.sfv.qa.util.TestUtil;

public class Forgotpasswordpage extends testbase
{
	@FindBy(xpath="//a[text()='FORGOT PASSWORD?']")
	 WebElement forgotpasslink;
	
	@FindBy(xpath="//input[@ng-model='forgotPassword.email']")
	 WebElement emailidtextbox;
	
	@FindBy(xpath="//button[text()='SUBMIT']")
	 WebElement submitbtn;
	
	@FindBy(xpath="//a[text()='CANCEL']")
	 WebElement cancelbtn;
	
	@FindBy(xpath="//div[@class='text-danger']")
	 WebElement text1;
	
	@FindBy(xpath="//p[@class='ng-binding']")
	 WebElement text2;
	
	@FindBy(xpath="//div[@class='text-danger ng-binding']")
	 WebElement text3;
	
	String text21 = "Please enter valid email address";
	//String text11 = "An e-mail has been sent to supriya.malave@blazeclan.com with further instruction.";
	String text31 = "User Not Found.";
	
	Properties prop = new Properties();
	InputStream input = null;
	{
	try 
	{
		
		input = new FileInputStream("src/Config/application.properties");
		
		prop.load(input);
	}catch (Exception ex) 
	{
		ex.printStackTrace();
	}
	
	}
	
	Properties dataprop = new Properties();
	InputStream inputdata = null;
	{
	try 
	{
		
		inputdata = new FileInputStream("src/Config/data.properties");
		
		dataprop.load(inputdata);
	}catch (Exception ex) 
	{
		ex.printStackTrace();
	}
	
	}
	
	 // Initializing the Page Objects:
		public Forgotpasswordpage() 
		   {
		       PageFactory.initElements(driver, this);
		   }
		
	   public void  forgotpassvaliduser() throws InterruptedException
	       {
//		   "dataprop.getProperty("Email")"
		   
		   
		     TestUtil.clickbutton(prop.getProperty("forgotpasswordlink"));
		     TestUtil.textboxsendkeys(prop.getProperty("emailIDtextbox"), dataprop.getProperty("Email"));
		     TestUtil.clickbutton(prop.getProperty("SubmitButton"));
		     String t1 = TestUtil.gettextofelement(prop.getProperty("Textmessage2"));
		     System.out.println(t1 + dataprop.getProperty("text11"));
			 Assert.assertEquals(t1, dataprop.getProperty("text11"));
			 System.out.println("Passed");
		     
			 /*
		     forgotpasslink.click();
			// emailidtextbox.sendKeys("test@gmail.com");
			 emailidtextbox.sendKeys("supriya.malave@blazeclan.com");
			 submitbtn.click();
			 String t1 = text2.getText();
			 System.out.println(t1 + text11);
			 Assert.assertEquals(t1, text11);
			 System.out.println("Passed");
			 /*if(t1.equals(text11))
			 {
				 System.out.println(t1  +   text11);
			 }
			 else
			 {
				 System.out.println("Failed");
			 }
		   */
	       } 
		 
	  public void  forgotpassinvalidemail() throws InterruptedException
	       {
			 forgotpasslink.click();
			 emailidtextbox.sendKeys("hudjolsij");
			 submitbtn.click();
			 String t2 = text1.getText();
			 System.out.println(t2 + text21);
			 Assert.assertEquals(t2, text21);
			 System.out.println("Passed");
			/* if(t2.equals(text21))
			 {
				 System.out.println(t2  +   text21);
			 }
			 else
			 {
				 System.out.println("Failed");
			 }*/
	       } 
		 
	  public void  forgotpassinvaliduser() throws InterruptedException
	       {
			 forgotpasslink.click();
			 emailidtextbox.sendKeys("hudss@gmail.com");
			 submitbtn.click();
			 String t3 = text3.getText();
			 System.out.println(t3 + text31);
			 Assert.assertEquals(t3, text31);
			 System.out.println("Passed");
			/* if(t3.equals(text31))
			 {
				 System.out.println(t3  +   text31);
			 }
			 else
			 {
				 System.out.println("Failed");
			 }*/
	       } 
}
