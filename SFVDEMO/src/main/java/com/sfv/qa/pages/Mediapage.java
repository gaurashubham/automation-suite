package com.sfv.qa.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.sfv.qa.base.testbase;

public class Mediapage extends testbase
{
	@FindBy(xpath="//button[@class='mobile-col-100 btn btn-astro-default btn-sm']")
	 WebElement uploadmediabtn;
	
	@FindBy(xpath="//button[text()='Manage']")
	 WebElement managebtn;
	
	@FindBy(xpath="(//input[@name='checkbox'])[1]")
	 WebElement checkbox;
	
	@FindBy(xpath="//button[contains(.,'Syndicate')]")
	 WebElement Syndicatebtn;
	
	@FindBy(xpath="//select[@ng-model='syndication.profileId']")
	 WebElement Syndicationprofile;
	
	@FindBy(xpath="//select[@ng-model='syndication.email']")
	 WebElement Syndicationprofilemail;
	
	@FindBy(xpath="//a[text()='Confirm']")
	 WebElement confirmbtn;
	
	@FindBy(xpath="//a[text()='Media']")
	 WebElement mediabtn;
	
	@FindBy(xpath=" (//span[@class='notification-icon-count ng-binding'])[1]")
	 WebElement notificationbtn;
	
	@FindBy(xpath="(//a[text()='Youtube Video Published : '])[1]")
	 WebElement notificationdrop;
	
	@FindBy(xpath="(//span[@class='item-title pointer ng-binding'])[1]")
	 WebElement mediatobesyndicated;
	
	@FindBy(xpath="//span[@class='item-title pointer ng-binding']")
	 WebElement mediatopreview;
	
	@FindBy(xpath="//th[@class='col-72 ng-binding']")
	 WebElement mediatid;
	
	@FindBy(xpath="(//span[@class='ng-binding'])[3]")
	 WebElement mediatidnoti;
	
	@FindBy(xpath="(//span[@class='item-title pointer ng-binding'])[11]")
	 WebElement mediatobereplaced;
	
	@FindBy(xpath="(//span[@class='item-title pointer ng-binding'])")
	 WebElement replacedmedia; 
	
	@FindBy(xpath="(//button[@class='btn btn-astro-default no-padding'])[2]")
	 WebElement replacebtn;
	
	@FindBy(xpath="//button[text()='Save']")
	 WebElement savebtn;
	
	@FindBy(xpath="//a[text()='Upload']")
	 WebElement uploadtab;
	
	@FindBy(xpath="//i[@class='fa fa-upload']")
	 WebElement uploadbtn ;
	
	@FindBy(xpath="//th[@class='col-72 ng-binding']")
	 WebElement mediaid;
	
	@FindBy(xpath="(//a[@class='btn menu-btn ng-binding'])[2]")
	 WebElement incompletemediatab;
	
	@FindBy(xpath="(//span[@class='item-title pointer ng-binding'])[1]")
	 WebElement incompletemedia;
	
	@FindBy(xpath="//th[@class='col-72 ng-binding']")
	 WebElement mediaid2;
	
	@FindBy(xpath="(//input[@ng-click='selectItem(item)'])[4]")
	 WebElement mediatobedel1;
	
	@FindBy(xpath="(//input[@ng-click='selectItem(item)'])[2]")
	 WebElement mediatobedel2;
	
	@FindBy(xpath="(//input[@ng-click='selectItem(item)'])[6]")
	 WebElement mediatobedel3;
	
	@FindBy(xpath="//i[@class='fa  fa-trash']")
	 WebElement deletebtn;
	
	@FindBy(xpath="(//button[@id='single-button'])[4]")
	 WebElement status;
	
	 @FindBy(xpath="//input[@id='statusInActive']")
	 WebElement statinactive;
    
    @FindBy(xpath=" //a[text()='Confirm']")
	 WebElement confirmbtn1;
    
    @FindBy(xpath="(//button[@id='single-button'])[3]")
	 WebElement syndicatebtn;
    
    @FindBy(xpath="(//input[@ng-click='selectItem(item)'])[1]")
	 WebElement mediatosyn1;
	
	@FindBy(xpath="(//input[@ng-click='selectItem(item)'])[3]")
	 WebElement mediatosyn2;
	
	@FindBy(xpath="(//input[@ng-click='selectItem(item)'])[5]")
	 WebElement mediatosyn3;
	
    @FindBy(xpath="//button[text()='OK']")
     WebElement OKbtn;
    
    @FindBy(xpath=" (//a[text()='Confirm'])[2]")
    WebElement confirmbtn2;
    
    @FindBy(xpath="(//a[@href='#!/list-syndicated-media/'])[1]")
    WebElement syndicatedmediabtn;
    
    @FindBy(xpath="(//span[@class='item-title pointer ng-binding'])[1]")
    WebElement syndicatedmedia1;
    
    @FindBy(xpath="//th[@class='col-72 ng-binding']")
    WebElement syndic1;
  
    @FindBy(xpath="(//span[@class='item-title pointer ng-binding'])[3]")
    WebElement mediatobedeleted;
    
    @FindBy(xpath="//th[@class='col-72 ng-binding']")
    WebElement mediaidofdelete;
    
    @FindBy(xpath="(//input[@ng-click='selectItem(item)'])[3]")
	 WebElement checkboxofmediadel;
    
    @FindBy(xpath=" (//a[@class='btn menu-btn ng-binding'])[4]")
	 WebElement deletetab;
    
    @FindBy(xpath="//input[@name='search']")
	 WebElement searchtab;
    
    @FindBy(xpath="//span[@class='item-title pointer ng-binding']")
	 WebElement deletemediaxpath;
    
    @FindBy(xpath="//th[@class='col-72 ng-binding']")
    WebElement mediapresentindel;
    
    @FindBy(xpath="(//span[@class='item-title pointer ng-binding'])[1]")
    WebElement mediatomultilang;
    
    @FindBy(xpath="//a[text()='Multi-Language']")
    WebElement multilangtab;
    
    @FindBy(xpath=" //select[@id='languageSelector']")
    WebElement langsel;
    
    @FindBy(xpath="//input[@id='languageTitle']")
    WebElement langtitle;
    
    @FindBy(xpath="//textarea[@id='languageDescriptor']")
    WebElement langdescrip;
    
    @FindBy(xpath="//a[text()='Save']")
    WebElement savebtn3;
  
    @FindBy(xpath=" //a[text()='DETAILS & TAGS']")
    WebElement details;
    
    @FindBy(xpath="(//span[@class='item-title pointer ng-binding'])[1]")
    WebElement mediapreview;
    
    @FindBy(xpath=" //a[text()='Media Preview']")
    WebElement mediapreviewtab;
 
    @FindBy(xpath="//button[@ngf-select='getPreSignedPreviewMedia($files)']")
    WebElement uploadmediaprebtn;
    
  //button[@ngf-select='getPreSignedPreviewMedia($files)']
 
    @FindBy(xpath="(//input[@ng-click='selectItem(item)'])[4]")
    WebElement mediatobeadded;
    
    @FindBy(xpath="(//button[@id='single-button'])[2]")
    WebElement addplaylistbtn;
    
    @FindBy(xpath="(//input[@ng-model='checkedItem'])[2]")
    WebElement searchtextbox;
    
    @FindBy(xpath="//input[@ng-checked='selectedPlaylist.indexOf(playlist.id) > -1']")
    WebElement checkbox1;
    
    @FindBy(xpath="//a[@href='#!/list-deleted-media/']")
    WebElement deleteandrestore;

    @FindBy(xpath="(//span[@class='item-title pointer ng-binding'])[1]")
    WebElement mediatitletoberestored;
    
    @FindBy(xpath="(//input[@name='checkbox'])[1]")
    WebElement mediatoberestored;
    
     @FindBy(xpath="//li[@class='menu-item ng-scope']//a[@href='#!/list-my-media/']	")
    WebElement mediatab;
     
     @FindBy(xpath=" //input[@name='search']")
     WebElement searchfieldtab;
  
     @FindBy(xpath="//span[@class='item-title pointer ng-binding']")
     WebElement mediatocompare;
  
     @FindBy(xpath="//button[@ng-if='!showRetrive']")
     WebElement restore;
  
     @FindBy(xpath=" (//span[@class='item-title pointer ng-binding'])[7]")
     WebElement mediadetail;
    
     @FindBy(xpath="//th[@class='col-72 ng-binding']")
     WebElement mediaiddetail;
   
     @FindBy(xpath="(//th[@class='ng-binding'])[1]")
     WebElement mediacreationdate;
   
     @FindBy(xpath="(//th[@class='ng-binding'])[2]")
     WebElement mediamodifieddate;
    		 
     @FindBy(xpath="//span[@class='ng-binding']")
     WebElement publishingdate;
     
     @FindBy(xpath="(//button[@class='btn btn-astro-default no-padding'])[1]")
     WebElement getcode;
     
     @FindBy(xpath="(//a[@class='ng-binding'])[3]")
     WebElement applicationtest;
     
     @FindBy(xpath="//code[@id='containerid']")
     WebElement code;
     
     @FindBy(xpath="(//span[@class='item-icon ng-scope dropdown'])[1]")
     WebElement hamburger;
     
     @FindBy(xpath="(//span[@class='item-icon ng-scope dropdown'])")
     WebElement wifi;
     
     @FindBy(xpath="//a[@data-ui-sref=\"edit-playlist({playlistId:playlist.id})\"]")
     WebElement playlistname;
     
     @FindBy(xpath="(//li[@ng-repeat=\"syndicate in syndicationList\"]//a[@class='ng-binding'])[1]")
     WebElement syndicationprofile;
     
    @FindBy(xpath="(//span[@class='item-title pointer ng-binding'])[1]")
     WebElement incompletemediatabmedia;
    
    @FindBy(xpath="(//input[@ng-click='selectItem(item)'])[1]")
    WebElement checkboxnumber;
    
    @FindBy(xpath="(//button[@id='single-button'])[2]")
    WebElement playlist;
    
    @FindBy(xpath="(//input[@type='checkbox'])[1]")
    WebElement playlistadd;
    
    @FindBy(xpath="(//a[text()='Confirm'])[1]")
    WebElement confirmbtnnumber;
    
    @FindBy(xpath="//i[@class='fa fa-tag']")
    WebElement tagcheck;
    
    @FindBy(xpath="//div[@class='ui-pnotify-text']")
    WebElement notificationmessage;
  
  
    @FindBy(xpath="(//a[@href='#!/list-incomplete-media/'])[1]")
    WebElement incompletemediatabcheck;
    
    @FindBy(xpath="//a[text()='THUMBNAILS']")
    WebElement thumbnailtab;
    
    @FindBy(xpath="//button[text()='Capture Thumbnail']")
    WebElement capturethumbnailbtn;
  
    @FindBy(xpath="//video[@class='jw-video jw-reset']")
    WebElement playbtn;
    
  //div[@aria-label='Start Playback']
  
    @FindBy(xpath="  //button[text()='Click to take snapshot']")
    WebElement clicktakesnapshotbtn;
    
    @FindBy(xpath="//input[@name='search']")
    WebElement searchbtn;
    
  
  //video[@class='jw-video jw-reset']

    @FindBy(xpath="//img[@src='//placehold.it/316x158?text=Transcoding in progress']")
    WebElement transcodinginprogress;
  
    @FindBy(xpath="(//button[@id='single-button'])[4]")
    WebElement statuschangetab;
    
    @FindBy(xpath=" (//button[text()='View thumbnail'])[1]")
    WebElement viewthumbnailbutton;
   
    @FindBy(xpath="(//span[@class='item-title pointer ng-binding'])[1]")
    WebElement mediatoberestore;
    
    @FindBy(xpath="//th[@class='col-72 ng-binding']")
    WebElement mediaidtoberestored;
  
    @FindBy(xpath="//button[@id='startTime']")
    WebElement startingslottime;
    
    @FindBy(xpath="//button[@id='endTime']")
    WebElement endingslottime;
    
    @FindBy(xpath="//input[@ng-model='time.seconds']")
    WebElement timetobeinputed;
    
    @FindBy(xpath="//div[@class='jw-icon jw-icon-display jw-button-color jw-reset']")
    WebElement mediapreviewplay;
  
    // Initializing the Page Objects:
	public Mediapage() 
	   {
	       PageFactory.initElements(driver, this);
	   }
	
	 public void syndicationofmedia(String mediaid,String syndicationprofiletype,String syndicationemail) throws InterruptedException
     {
		 mediabtn.click();
		 Thread.sleep(1000);
		 searchbtn.click();
		 Thread.sleep(3000);
		 searchbtn.sendKeys(mediaid);
		 Thread.sleep(3000);
		 searchbtn.sendKeys(Keys.ENTER);
		 Thread.sleep(3000);
		
		 
		 mediatobesyndicated.click();
		 Thread.sleep(3000);
		 String ele1 = mediatid.getText();
		 System.out.println("Media id: "+ele1+" -");
		 String ele=ele1+" -";
		   
		 driver.navigate().back();
		 Thread.sleep(3000);
		 
		 searchbtn.click();
		 Thread.sleep(3000);
		 searchbtn.sendKeys(mediaid);
		 Thread.sleep(3000);
		 searchbtn.sendKeys(Keys.ENTER);
		 Thread.sleep(3000);
		 
		 managebtn.click();
		 Thread.sleep(1000);
		 
		 checkbox.click();
		 Thread.sleep(1000);
		 
		 Syndicatebtn.click();
         Select select = new Select(Syndicationprofile);
         select.selectByVisibleText(syndicationprofiletype);
		 
         Select select1 = new Select(Syndicationprofilemail);
         select1.selectByVisibleText(syndicationemail);
        // select1.selectByVisibleText("supriya.malave@blazeclan.com");
         
         confirmbtn.click();
         
         Thread.sleep(120000);
         driver.navigate().refresh();
         Thread.sleep(10000);
         new WebDriverWait(driver, 600)
 		.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//span[@class='notification-icon-count ng-binding'])[1]")));
         notificationbtn.click();
         String elem = notificationdrop.getText();
         System.out.println(elem);
         
         String mediaidele = mediatidnoti.getText();
         System.out.println("Media id :"+mediaidele);
         Assert.assertEquals(ele, mediaidele);
        /* if(ele.equals(mediaidele))*/
        	 //{
        		 System.out.println("Video published successfully");
        	// }
         
         /* else
             {
        	     System.out.println("Failed");
             }
         Thread.sleep(8000);*/
    }  
	 
	public void replacement(String path) throws InterruptedException, AWTException
	{
	     mediabtn.click();
		 Thread.sleep(6000);
		 mediatobereplaced.click();
		 Thread.sleep(2000);
	     String text1 = mediaid.getText();
	     System.out.println(text1);
		 Thread.sleep(6000);
		
		
		// disable the click event on an `<input>` file
		  ((JavascriptExecutor)driver).executeScript(
		      "HTMLInputElement.prototype.click = function() {                     " +
		      "  if(this.type !== 'file') HTMLElement.prototype.click.call(this);  " +
		      "};                                                                  " );

		  // trigger the upload
		  driver.findElement(By.xpath("//button[@ngf-select='replacePreSignedMedia($files,media.id)']"))
		        .click();

		  // assign the file to the `<input>`
		  driver.findElement(By.cssSelector("input[type=file]"))
		        .sendKeys(path);
		driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS);
	    System.out.println("Media replaced");
		 
		      Thread.sleep(7000);
				for (int second = 0;; second++) 
			      {
			             if(second >=3)
			             {
			                break;
			             } 
			        JavascriptExecutor jse1 = (JavascriptExecutor)driver;
			    	jse1.executeScript("window.scrollBy(0,100)");
			        Thread.sleep(3000);
			      } 
				
				savebtn.click();
				Thread.sleep(30000);
				searchtab.click();
				Thread.sleep(5000);
				searchtab.sendKeys(text1);
				searchtab.sendKeys(Keys.ENTER);
				Thread.sleep(5000);
				replacedmedia.click();
				Thread.sleep(6000);
				String text5 = mediaid.getText();
				Thread.sleep(6000);
			    System.out.println(text5);
				thumbnailtab.click();
			    boolean text2 = viewthumbnailbutton.isDisplayed();
			    System.out.println(text2 +"Media is getting transcoded");
			    WebDriverWait wait = new WebDriverWait(driver,10);
			    Thread.sleep(5000);
			    wait.until(ExpectedConditions.visibilityOf(viewthumbnailbutton));
			    String text3 = viewthumbnailbutton.getText();
			    Thread.sleep(5000);
			    System.out.println(text3);
			    String text4 = "View thumbnail";
			    Assert.assertEquals(text3, text4);
				
        }
	
	public void incompletedmedia(String path) throws InterruptedException, AWTException 
	{
		
		uploadtab.click();
		Thread.sleep(1000);
				JavascriptExecutor js=(JavascriptExecutor) driver;
				WebElement fileInput = (WebElement) js.executeScript("return document.querySelector(\"input[name='file']\");");
				fileInput.sendKeys(path);
				Thread.sleep(10000);
				String element1 = mediaid.getText();
				
				for (int second = 0;; second++) 
			      {
			             if(second >=3)
			             {
			                break;
			             } 
			        JavascriptExecutor jse1 = (JavascriptExecutor)driver;
			    	jse1.executeScript("window.scrollBy(0,100)");
			        Thread.sleep(3000);
			      } 
				
				savebtn.click();
				Thread.sleep(2000);
				incompletemediatab.click();
				searchtab.sendKeys(element1);
				Robot robot = new Robot();
				robot.keyPress(KeyEvent.VK_ENTER);
			    robot.keyRelease(KeyEvent.VK_ENTER);
			    Thread.sleep(2000);
				incompletemedia.click();
				Thread.sleep(6000);
				String element2 = mediaid2.getText();
				System.out.println(element1);
				System.out.println(element2);
				Assert.assertEquals(element1, element2);
				System.out.println("Incomplete media is present");
				Thread.sleep(8000);
	}
	
	public void deletemulti() throws InterruptedException
	{
		mediabtn.click();
		managebtn.click();
		mediatobedel1.click();
		mediatobedel2.click();
		mediatobedel3.click();
		deletebtn.click();
		Thread.sleep(8000);
		OKbtn.click();
		Thread.sleep(8000);
		
		
		
	}
	public void statuschangemul() throws InterruptedException
	{
	    Thread.sleep(4000);
		mediabtn.click();
		managebtn.click();
		mediatobedel1.click();
		mediatobedel2.click();
		mediatobedel3.click();
		status.click();
		Thread.sleep(1000);
	    statinactive.click();
	    Thread.sleep(1000);
		confirmbtn2.click();
		Thread.sleep(8000);
	}
	
	public void syndicatemul(String syndicationprofiletype,String syndicationemail) throws InterruptedException
	{
		mediabtn.click();
		managebtn.click();
		mediatosyn1.click();
		mediatosyn2.click();
		mediatosyn3.click();
		syndicatebtn.click();
		Select select = new Select(Syndicationprofile);
        select.selectByVisibleText(syndicationprofiletype);
		 
        Select select1 = new Select(Syndicationprofilemail);
        select1.selectByVisibleText(syndicationemail);
       // select1.selectByVisibleText("supriya.malave@blazeclan.com");
         
         confirmbtn.click();
         Thread.sleep(8000);
	}
	
	public void syndicatedmediatab(String mediaid) throws InterruptedException 
	{
		 syndicatedmediabtn.click();
         searchtab.sendKeys(mediaid);
         searchtab.sendKeys(Keys.ENTER);
         Thread.sleep(1000);
         syndicatedmedia1.click();
         Thread.sleep(8000);
         String element3 = syndic1.getText();
         Thread.sleep(7000);
         System.out.println(element3);
         Thread.sleep(1000);
         System.out.println(mediaid);
         Thread.sleep(1000);
         Assert.assertEquals(mediaid, element3);
         Thread.sleep(1000);
         System.out.println("Passed syndicated media");
         
     }
	public void deletedmediatab() throws InterruptedException, AWTException
	{
		 mediabtn.click();
		 Thread.sleep(1000);
		 mediatobedeleted.click();
		 Thread.sleep(6000);
		 String element4 = mediaidofdelete.getText();
		 System.out.println(element4);
		 driver.navigate().back();
		 Thread.sleep(2000);
		 managebtn.click();
		 Thread.sleep(2000);
		 checkboxofmediadel.click();
		 Thread.sleep(2000);
		 deletebtn.click();
		 Thread.sleep(2000);
		 OKbtn.click();
		 driver.navigate().refresh();
		 Thread.sleep(2000);
		 deletetab.click();
		 Thread.sleep(4000);
		 searchtab.sendKeys(element4);
		 searchtab.sendKeys(Keys.ENTER);
		 /*Robot robot = new Robot();
		 robot.keyPress(KeyEvent.VK_ENTER);
	     robot.keyRelease(KeyEvent.VK_ENTER);*/
	     Thread.sleep(2000);
	     deletemediaxpath.click();
	     Thread.sleep(8000);
	     System.out.println(element4);
	     String element5 = mediapresentindel.getText();
	     Thread.sleep(5000);
	     System.out.println(element5);
	     Assert.assertEquals(element4, element5);
		 System.out.println("Deleted");
		 Thread.sleep(8000);
	}
	
	public void multiplelanguage(String mediaid,String title,String description) throws InterruptedException
	{
		 mediatomultilang.click();
		 Thread.sleep(3000);
		 multilangtab.click();
		 Thread.sleep(3000);
		 Select select = new Select(langsel);
		 select.selectByIndex(3);
		 Thread.sleep(3000);
		 langtitle.sendKeys(title);
		 JavascriptExecutor jse1 = (JavascriptExecutor)driver;
	     jse1.executeScript("window.scrollBy(0,100)");
	     Thread.sleep(3000);
		 langdescrip.sendKeys(description);
		 savebtn3.click();
		 details.click();
		 savebtn.click();
		 Thread.sleep(8000);
    }
	
	public void mediapreview(String startslot,String endslot) throws InterruptedException
	{
		mediatopreview.click();
		Thread.sleep(6000);
	    String mediapreviewid = mediatid.getText();
		System.out.println("Media Preview Media Id : "+mediapreviewid);
	
		new WebDriverWait(driver, 100)
        .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='search']")));
		Thread.sleep(6000);
		searchbtn.click();
		Thread.sleep(5000);
	    searchbtn.sendKeys(mediapreviewid);
	    Thread.sleep(5000);
	    searchbtn.sendKeys(Keys.ENTER);
	    Thread.sleep(5000);
		mediapreview.click();
		Thread.sleep(5000);
		mediapreviewtab.click();
		Thread.sleep(5000);
		startingslottime.click();
		Thread.sleep(5000);
		timetobeinputed.clear();
		Thread.sleep(5000);
		timetobeinputed.sendKeys(startslot);
		Thread.sleep(5000);
		endingslottime.click();
		Thread.sleep(5000);
		timetobeinputed.clear();
		Thread.sleep(5000);
		timetobeinputed.sendKeys(endslot);
		Thread.sleep(5000);
		details.click();
		Thread.sleep(2000);
		savebtn.click();
		new WebDriverWait(driver, 100)
        .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='search']")));
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
	    searchbtn.sendKeys(mediapreviewid);
	    driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
	    searchbtn.sendKeys(Keys.ENTER);
	    driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
	    mediabtn.click();
		Thread.sleep(2000);
		mediapreview.click();
		Thread.sleep(2000);
		mediapreviewtab.click();
		Thread.sleep(2000);
		boolean t1 = transcodinginprogress.isDisplayed();
		System.out.println(t1);
		Thread.sleep(5000);
		String t2 = transcodinginprogress.getAttribute("value");
		System.out.println(t2);
		Thread.sleep(60000);
		driver.navigate().refresh();
	    mediapreviewplay.isDisplayed();
		
	}
	public void addmediatoplay() throws InterruptedException
	{
		    Thread.sleep(4000);
			mediabtn.click();
			Thread.sleep(2000);
			managebtn.click();
			Thread.sleep(2000);
			mediatobeadded.click();
			Thread.sleep(2000);
			addplaylistbtn.click();
			Thread.sleep(2000);
			searchtextbox.click();
//			searchtextbox.sendKeys("testeco");
//			Thread.sleep(2000);
//			checkbox1.click();
			confirmbtn1.click();
	}
	
	public void restoredeletedmedia() throws InterruptedException
	{
		   Thread.sleep(4000);
		   mediabtn.click();
		   deleteandrestore.click();
		   Thread.sleep(2000);
		   mediatoberestore.click();
		   Thread.sleep(4000);
		   String mediaiddetails = mediaidtoberestored.getText();
		   Thread.sleep(6000);
		   System.out.println("Media ID to be deleted:"+mediaiddetails);
		   deleteandrestore.click();
		   Thread.sleep(2000);
		   managebtn.click();
		   Thread.sleep(6000);
		   searchbtn.click();
		   Thread.sleep(6000);
		   searchbtn.sendKeys(mediaiddetails);
		   searchbtn.sendKeys(Keys.ENTER);
		   String title = mediatitletoberestored.getText();
		   System.out.println(title);
		   mediatoberestored.click();
		   restore.click();
		   Thread.sleep(4000);
		   OKbtn.click();
		   Thread.sleep(4000);
		   mediatab.click();
		   Thread.sleep(4000);
		   searchfieldtab.sendKeys(mediaiddetails);
		   searchfieldtab.sendKeys(Keys.ENTER);
		   Thread.sleep(4000);
		   String title1 = mediatocompare.getText();
		   Thread.sleep(4000);
		   mediatoberestore.click();
		   Thread.sleep(4000);
		   String mediaidtest = mediaidtoberestored.getText();
		   Thread.sleep(6000);
		   System.out.println("Media ID restored:"+mediaidtest);
		   Thread.sleep(4000);
		   System.out.println(title1);
		   Assert.assertEquals(title, title1);
		   Assert.assertEquals(mediaidtest, mediaiddetails);
	}
	
	public void mediadetails()
	{
		mediadetail.click();
		boolean t1 = mediaiddetail.isDisplayed();
		System.out.println(t1);
		boolean t2 = mediacreationdate.isDisplayed();
		System.out.println(t2);
		boolean t3 = mediamodifieddate.isDisplayed();
		System.out.println(t3);
		boolean t4 = publishingdate.isDisplayed();
		System.out.println(t4);
		
	}
	
	public void checkcode()
	{
		mediadetail.click();
		getcode.click();
		applicationtest.click();
		String text = code.getText();
		System.out.println(text);
		code.isDisplayed();
	}
	
	public void checkplaylistnameformedia(String title) throws InterruptedException
	{
		//formaterr
		searchfieldtab.clear();
		Thread.sleep(9000);
		searchfieldtab.sendKeys(title);
		Thread.sleep(3000);
		searchfieldtab.sendKeys(Keys.ENTER);
        Thread.sleep(3000);
		hamburger.click();
		Thread.sleep(3000);
		playlistname.isDisplayed();
		Thread.sleep(3000);
		System.out.println(playlistname.getText());
	}
	
	public void checksyndicationprofileformedia(String title) throws InterruptedException
	{
		Thread.sleep(9000);
		searchfieldtab.sendKeys(title);
		Thread.sleep(3000);
		searchfieldtab.sendKeys(Keys.ENTER);
		Thread.sleep(8000);
		wifi.click();
		Thread.sleep(3000);
		syndicationprofile.isDisplayed();
		Thread.sleep(3000);
		System.out.println(syndicationprofile.getText());
	}
	
	public void incompletemediaaddplaylist() throws InterruptedException
	{
		Thread.sleep(9000);
		incompletemediatabcheck.click();
		Thread.sleep(3000);
		managebtn.click();
		Thread.sleep(3000);
		checkboxnumber.click();
		Thread.sleep(3000);
		playlist.click();
		Thread.sleep(3000);
		playlistadd.click();
		Thread.sleep(3000);
		confirmbtnnumber.click();
		Thread.sleep(2000);
		String text = notificationmessage.getText();
		System.out.println(text);
		String text1 = "Can't perform the action, One of the videos selected has no title";
		System.out.println(text1);
		Assert.assertEquals(text, text1);
	}
	
	public void checktagformedia(String mediaidtag) throws InterruptedException
	{
		//formaterr
		Thread.sleep(9000);
		searchfieldtab.sendKeys(mediaidtag);
		Thread.sleep(3000);
		searchfieldtab.sendKeys(Keys.ENTER);
        Thread.sleep(6000);
		
		// Create action class object
		Actions builder=new Actions(driver);
			 
		// find the tooltip xpath
	    WebElement username_tooltip=driver.findElement(By.xpath("//span[@uib-tooltip-html='htmlTooltip']"));
			 
		// Mouse hover to that text message
		builder.moveToElement(username_tooltip).perform();
			 
		// Extract text from tooltip
		String tooltip_msg=username_tooltip.getText();
			 
		// Print the tooltip message just for our refrences
		System.out.println("Tooltip/ Help message is "+tooltip_msg);
	}
	
	public void thumbnailcapture() throws InterruptedException
	{
		mediadetail.click();
		Thread.sleep(3000);
		thumbnailtab.click();
		Thread.sleep(3000);
		capturethumbnailbtn.click();
		Thread.sleep(3000);
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement element = null;
		//driver.execute_script("document.getElementsByTagName('video')[0].play()");
		
		//js.executeScript("document.getElementsByTagName('video')");
		((WebElement) js.executeScript("document.getElementByTagName('video')")).click();
		Actions action = new Actions(driver);
		action.moveToElement(playbtn).build().perform();
		
		Point point = playbtn.getLocation();
    	int x = point.getX();
    	System.out.println("Horizontal Position: " + x + " pixels");
    	int y = point.getY();
    	System.out.println("Vertical Position " + y + " pixels");
        action.moveToElement(playbtn, x, y).click().build().perform();
		Thread.sleep(3000);
		clicktakesnapshotbtn.click();
		Thread.sleep(3000);
		capturethumbnailbtn.isDisplayed();
	}
	
	public void settimetochangemediastatus(String mediaid)
	{
		searchtab.click();
		searchtab.sendKeys(mediaid);
		searchtab.sendKeys(Keys.ENTER);
		managebtn.click();
		checkbox.click();
		statuschangetab.click();
	}
}
