package com.sfv.qa.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.sfv.qa.base.testbase;
import com.sfv.qa.util.TestUtil;

public class Createplaylistpage extends testbase
{
	@FindBy(xpath="//a[text()='DETAILS & TAGS']")
	 WebElement detailsntagtab;
	
	@FindBy(xpath="//a[text()='THUMBNAILS']")
	 WebElement thumbnailstab;
	
	@FindBy(xpath="//a[text()='Media List']")
	 WebElement medialisttab;
	
    @FindBy(xpath="//label[@for='cmn-toggle-round-flat']")
	 WebElement activebtn;
	
	@FindBy(xpath="//div[@class='media-thumbnail-holder']")
	 WebElement playbtn ;
	
	@FindBy(xpath="//input[@id='playlistTitle']")
	 WebElement titletextfield;
	
	@FindBy(xpath="//span[text()='Choose Tag Profile']")
	 WebElement tagprofilefield ;
	
	@FindBy(xpath="//textarea[@id='desc']")
	 WebElement descriptfield;
	
	@FindBy(xpath="//input[@placeholder='eg. namespace:predicate=value']")
	 WebElement tagfield;
	
	@FindBy(xpath="//label[text()='Static']")
	 WebElement staticbtn ;
	
	@FindBy(xpath="//label[@class='btn btn-default dynamic-playlist-btn ng-untouched ng-valid ng-not-empty ng-dirty']")
	 WebElement dynamicmediabtn;
	
	@FindBy(xpath="(//label[@class='btn btn-default dynamic-playlist-btn ng-pristine ng-untouched ng-valid ng-not-empty'])[1]")
	 WebElement dynamicplaylistbtn;
	
	@FindBy(xpath="(//label[@class='btn btn-default dynamic-playlist-btn ng-pristine ng-untouched ng-valid ng-not-empty'])[2]")
	 WebElement dynamicplaylistmediabtn;
	
	@FindBy(xpath="//button[text()='Save']")
	 WebElement savebtn;
	
	@FindBy(xpath="//button[text()='Cancel']")
	 WebElement cancelbtn;
	
	@FindBy(xpath="(//a[@class='item-title ng-binding'])[1]")
	 WebElement clickontheplaylist ;
	
	@FindBy(xpath="//a[text()='Media List']")
	 WebElement medialist ;
	
	@FindBy(xpath="(//button[@ng-click='addPlaylistMedia()'])[1]")
	 WebElement addmediabtn ;
	
	@FindBy(xpath="(//input[@ng-model='mediaid'])[3]")
	 WebElement  media1;
	
	@FindBy(xpath="(//input[@ng-model='mediaid'])[1]")
	 WebElement media2;
	
	@FindBy(xpath="(//input[@ng-model='mediaid'])[2]")
	 WebElement media3;
	
	@FindBy(xpath="(//button[text()='Save'])[2]")
	 WebElement savebtn1;
	
	@FindBy(xpath="//button[text()='Confirm']")
	 WebElement confirmbtn;
	
	@FindBy(xpath="//a[text()='Playlist']")
	 WebElement playlistbtn;
	
	@FindBy(xpath="//input[@id='playerkey']")
	 WebElement searchbtn;
	
	@FindBy(xpath="(//div[@class='ng-binding'])[1]")
	 WebElement element1;
	
	@FindBy(xpath="(//td[@class='ng-binding'])[6]")
	 WebElement element2;
	
	@FindBy(xpath="//a[@data-ui-sref='create-playlist']")
	 WebElement createplaybtn;
	
	@FindBy(xpath="//button[text()='Manage']")
	 WebElement managebtn;
	
	@FindBy(xpath="	(//input[@name='checkbox'])[11]")
	 WebElement mediadel1;
	
	@FindBy(xpath="	(//input[@name='checkbox'])[13]")
	 WebElement mediadel2;
	
	@FindBy(xpath="	(//input[@name='checkbox'])[10]")
	 WebElement mediadel3;
	
	@FindBy(xpath="(//button[@sfv-acl='delete-playlist'])[1]")
	 WebElement deletebtn;
	
	@FindBy(xpath="(//input[@name='checkbox'])[6]")
	 WebElement mediastatus1;
	
	@FindBy(xpath="(//input[@name='checkbox'])[7]")
	 WebElement mediastatus2;
	
	@FindBy(xpath="(//input[@name='checkbox'])[8]")
	 WebElement mediastatus3;
	
	@FindBy(xpath="(//button[@id='single-button'])[2]")
	 WebElement status;
	
    @FindBy(xpath="//button[text()='OK']")
	 WebElement OKbtn;
    
    @FindBy(xpath="//input[@id='statusInActive']")
	 WebElement statinactive;
    
    @FindBy(xpath=" //a[text()='Confirm']")
	 WebElement confirmbtn1;
    
    @FindBy(xpath="//input[@name='search']")
	 WebElement searchbtn1;
	
	@FindBy(xpath="(//a[@class='item-title ng-binding'])[1]")
	 WebElement searchfield;
	
	@FindBy(xpath="(//a[@class='item-title ng-binding'])[1]")
	 WebElement playselect;
	
	@FindBy(xpath="(//button[@class='hidden-xs btn btn-sm btn-astro-default'])[1]")
	 WebElement removebtn1;
	
	@FindBy(xpath="//button[@class='btn btn-astro-default btn-small']")
	 WebElement savebtn2;
	
	@FindBy(xpath="(//button[@class='btn btn-astro-default btn-small'])[4]")
	 WebElement savebtnclick;
	
	@FindBy(xpath="//span[@class='playlist-total-items ng-scope']")
	 List<WebElement> playlistcheck;
	
	@FindBy(xpath="//div[@modal-animation='true']")
	 WebElement sideclick;
	
	@FindBy(xpath="//input[@name='search']")
	 WebElement searchtab;
	
	@FindBy(xpath="//a[@sfv-acl='edit-playlist']")
	 WebElement editplaylist;
	
	@FindBy(xpath="//input[@placeholder='Search']")
	 WebElement searchtextbox;
	
	@FindBy(xpath="(//th[@class='ng-binding'])[1]")
	 WebElement playlistid;
	
	@FindBy(xpath="//a[@class='btn btn-astro-default']")
	 WebElement createplaylistbtn ;
	
	@FindBy(xpath="(//a[@class='item-title ng-binding'])[1]")
	 WebElement playlistnumber;
	
	@FindBy(xpath="//a[text()='Thumbnail']")
	 WebElement thumbnailtab;
	
	@FindBy(xpath="(//button[text()='Upload Image'])[1]")
	 WebElement uploadimagebutton;
	
	@FindBy(xpath="//a[text()='Details & Tags']")
	 WebElement detailsandtagstab;
	
	@FindBy(xpath="(//button[text()='Save'])[1]")
	 WebElement savebtn5;
	
	@FindBy(xpath="//div[text()='Playlist updated successfully.']")
	 WebElement playlistupdatedsuccessfullynoti;
	
	@FindBy(xpath="//button[text()='View thumbnail']")
	 WebElement viewthumbnailbutton;
	
	@FindBy(xpath="//input[@name='search']")
	 WebElement searchbtn3;
	
	@FindBy(xpath="//a[text()='Media']")
	 WebElement mediatab;
	
	@FindBy(xpath="//span[@class='item-title pointer ng-binding']")
	 WebElement mediatobeaddedinplay;
	
	@FindBy(xpath="//th[@class='col-72 ng-binding']")
	 WebElement mediaid;
	
	@FindBy(xpath="//button[text()='Manage']")
	 WebElement managetab;
	
	@FindBy(xpath="(//input[@name='checkbox'])[1]")
	 WebElement mediatobeaddedtoplay;
	
	@FindBy(xpath="(//i[@class='fa fa-list'])[1]")
	 WebElement playlisttabtoaddmedia;
	
	@FindBy(xpath="//input[@ng-model='searchKey']")
	 WebElement searchplaylisttextbox;
	
	@FindBy(xpath="(//input[@ng-model='checkedItem'])[1]")
	 WebElement playlistcheckboxtoadd;
	
	@FindBy(xpath="//a[@ng-click='addToPlaylist($event)']")
	 WebElement confirmtheplaylistmediadd;
	
	@FindBy(xpath="//input[@name='search']")
	 WebElement searchbtnformedia;
	
	@FindBy(xpath="(//td[@class='ng-binding'])[6]")
	 WebElement removedmediatitle;
	
	//input[@placeholder='Search']
	
	 // Initializing the Page Objects:
    public Createplaylistpage() 
    {
        PageFactory.initElements(driver, this);
    }
    
    public void createNewPlaylist(String title, String description,String path) throws InterruptedException, AWTException
    {
        String title1 = title + TestUtil.GetCurrentDateAndTimeAppFormat();
    	titletextfield.sendKeys(title1);
    	descriptfield.sendKeys(description);
    	activebtn.click();
    	
    	 
    	for (int second = 0;; second++) 
      {
             if(second >=3)
             {
                break;
             } 
        JavascriptExecutor jse = (JavascriptExecutor)driver;
    	jse.executeScript("window.scrollBy(0,100)");
        Thread.sleep(3000);
      }  
    	
    	savebtn.click();
    	Thread.sleep(9000);
    	searchbtn1.click();
    	searchbtn1.sendKeys(title1);
    	searchbtn1.sendKeys(Keys.ENTER);
    	Thread.sleep(2000);
        String ele1 = searchfield.getText();
        Thread.sleep(2000);
    	System.out.println(ele1);
    	Thread.sleep(5000);
    	System.out.println(title1);
    	Thread.sleep(5000);
    	Assert.assertEquals(title1, ele1);
    	//Assert.assertEquals(actual, expected);
    	System.out.println("Playlist found");
    	this.additionofthumbnailtoplaylist(path);
    }
	
    public void  addmediatoplaylist() throws InterruptedException, AWTException
	{
		playlistbtn.click();
		Thread.sleep(4000);
	    clickontheplaylist.click();
		Thread.sleep(6000);
		medialist.click();
		addmediabtn.click();
		Thread.sleep(2000);
		
		if(media1.isSelected())
		{
			System.out.println("Media already present in playlist");
		}
		else
		{
			media1.click();
		}
		
	    String media1title = element2.getText();
     	media2.click();
		media3.click();
        Thread.sleep(3000);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,500)");
		Thread.sleep(6000);
		confirmbtn.click();
		Thread.sleep(6000);
		savebtn1.click();
		Thread.sleep(3000);
        clickontheplaylist.click();
		medialist.click();
		Thread.sleep(5000);
		searchtextbox.click();
		searchtextbox.sendKeys(media1title);
		String media2title = element1.getText();
		System.out.println(media1title);
		System.out.println(media2title);
		Assert.assertEquals(media1title, media2title);
		System.out.println("Passed");
     }
	
	public void endtoendplaylist(String title, String description) throws InterruptedException
    {
		String title1 = title + TestUtil.GetCurrentDateAndTimeAppFormat();
		playlistbtn.click();
		Thread.sleep(2000);
		createplaybtn.click();
		titletextfield.sendKeys(title1);
    	descriptfield.sendKeys(description);
    	activebtn.click();
    	 
    	for (int second = 0;; second++) 
      {
             if(second >=3)
             {
                break;
             } 
        JavascriptExecutor jse = (JavascriptExecutor)driver;
    	jse.executeScript("window.scrollBy(0,100)");
        Thread.sleep(3000);
      }  
    	savebtn.click();
    	Thread.sleep(5000);
    	clickontheplaylist.click();
    	String idofplaylist = playlistid.getText();
    	System.out.println("Playlist ID is :"+idofplaylist);
		medialist.click();
		addmediabtn.click();
		Thread.sleep(2000);

     	media1.click();
     	Thread.sleep(5000);
     	String media1title = element2.getText();
     	media2.click();
		media3.click();
        Thread.sleep(3000);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,500)");
		Thread.sleep(3000);
		confirmbtn.click();
		Thread.sleep(5000);
		savebtn1.click();
		WebDriverWait wait = new WebDriverWait (driver, 15);
		 //Title of the webpage is "Software Testing Material - A site for Software Testers"
		 wait.until(ExpectedConditions.visibilityOf(searchtab));
         
	    searchtab.click();
		Thread.sleep(3000);
		searchtab.sendKeys(idofplaylist);
		Thread.sleep(2000);
		searchtab.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		clickontheplaylist.click();
		String idofplaylist2 = playlistid.getText();
    	System.out.println("Playlist ID is :"+idofplaylist2);
    	Assert.assertEquals(idofplaylist2, idofplaylist);
		medialist.click();
		Thread.sleep(5000);
		String media2title = element1.getText();
		System.out.println(media1title);
		System.out.println(media2title);
		Assert.assertEquals(media1title, media2title);
		System.out.println("Passed");
		Thread.sleep(5000);
		detailsandtagstab.click();
		medialist.click();
		
		
	}
   public void deletemulti() throws InterruptedException 
   {
	   playlistbtn.click();
	   managebtn.click();
	   mediadel1.click();
	   mediadel2.click();
	   mediadel3.click();
	   deletebtn.click();
	   Thread.sleep(3000);
	   OKbtn.click();
   }
   
   public void changestatusmulti() throws InterruptedException
   {
	   
	   Thread.sleep(3000);
	   playlistbtn.click();
	   managebtn.click();
	   mediastatus1.click();
	   mediastatus2.click();
	   mediastatus3.click();
	   status.click();
	   statinactive.click();
	   confirmbtn1.click();
    }
  
   public void removalmedia(String title,String description) throws InterruptedException
   {
	   this.endtoendplaylist(title, description);
	   Thread.sleep(3000);
	   playlistbtn.click();
	   Thread.sleep(3000);
	   searchtab.click();
	   Thread.sleep(3000);
	   searchtab.sendKeys(title);
	   Thread.sleep(2000);
	   searchtab.sendKeys(Keys.ENTER);
	   Thread.sleep(2000);
	   playselect.click();
	   Thread.sleep(2000);
	   medialist.click();
	   Thread.sleep(3000);
	   removebtn1.click();
	   Thread.sleep(8000);
	   JavascriptExecutor jse = (JavascriptExecutor)driver;
	   jse.executeScript("window.scrollBy(0,250)");
	   Thread.sleep(000);
	   Actions action = new Actions(driver);
	   action.moveToElement(savebtnclick).click().build().perform();
	   System.out.println("Removed the playlist");
	   Thread.sleep(3000);
   }
   
   public void checkplaylistdetails() throws InterruptedException
   {
	   Thread.sleep(3000);
	   playlistbtn.click();
	   Thread.sleep(2000);
	   List<WebElement> playlistchecktest = driver.findElements(By.xpath("//span[@class='playlist-total-items ng-scope']"));
	   int i = playlistchecktest.size();
	   System.out.println("--------------------------------------");
	   System.out.println("Total number of playlist available :" + "     " +i);
	   System.out.println("--------------------------------------");
	    for (int j=0;j<=(i-1);j++) 
	    { 
	    	System.out.println("--------------------------------------");
	        System.out.println("Video Sequence number:" + "        " +j);
	        System.out.println("--------------------------------------");
	    	String text1 = playlistchecktest.get(j).getText();
	        System.out.println(text1);
	        if(text1.contains("NA") || text1.contains("0") )
	        {
	        	System.out.println("No media present in the playlist");
	        	System.out.println("--------------------------------------");
	        }
	        else
	        {
	        	Thread.sleep(8000);
	        	playlistchecktest.get(j).click();
	        	Thread.sleep(5000);
	        	List<WebElement> mediachecktest = driver.findElements(By.xpath("//div[@class='col-xs-6']"));
	        	int k = mediachecktest.size();
	        	System.out.println("--------------------------------------");
	        	System.out.println("Total number of videos present in the playlist:" + "          " +k);
	        	System.out.println("--------------------------------------");
	        	for(int l=0;l<=(k-1);l++)
	        	{
	        		String textofmedia = mediachecktest.get(l).getText();
	        		System.out.println(textofmedia);
	        	}
	        	
	        	WebElement element = driver.findElement(By.xpath("//div[@modal-animation='true']"));
	        	Point point = element.getLocation();
	        	int x = point.getX();
	        	System.out.println("Horizontal Position: " + x + " pixels");
	        	int y = point.getY();
	        	System.out.println("Vertical Position " + y + " pixels");
	        	Actions action = new Actions(driver);
                action.moveToElement(element, x, y).click().build().perform();
	        	
	        	
	       }
	    }
	  
	   
   }
   
   public void additionofthumbnailtoplaylist(String path) throws AWTException, InterruptedException
	{
	     
		  playlistnumber.click();
		  Thread.sleep(3000);
		  thumbnailtab.click();
		  Thread.sleep(2000);
		 
		  // disable the click event on an `<input>` file
		  ((JavascriptExecutor)driver).executeScript(
		      "HTMLInputElement.prototype.click = function() {                     " +
		      "  if(this.type !== 'file') HTMLElement.prototype.click.call(this);  " +
		      "};                                                                  " );

		  // trigger the upload
		  driver.findElement(By.xpath("(//button[text()='Upload Image'])[1]"))
		        .click();

		  // assign the file to the `<input>`
		  driver.findElement(By.cssSelector("input[type=file]"))
		        .sendKeys(path);
		 
		detailsandtagstab.click();
		Thread.sleep(5000);
		savebtn5.click();
		Thread.sleep(1000);
		String text = playlistupdatedsuccessfullynoti.getText();
		String text1 = "Playlist updated successfully.";
		Assert.assertEquals(text, text1);
		Thread.sleep(9000);
		searchbtn3.sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
		//searchbtn3.clear();
		Thread.sleep(3000);
	    playlistnumber.click();
		thumbnailtab.click();
		viewthumbnailbutton.isDisplayed();
		System.out.println("Thumbnail added to playlist successfully");
		
	}
   
     public void adduploadedmediatoplaylist(String path,String title,String description,String copyright,String playlistitle,String playlistdescription,String thumbnailpath) throws InterruptedException, AWTException
    {
       Blazeclanpage blazeclanapppage = new Blazeclanpage();	 
       blazeclanapppage.clickOnuploadtab();	 
	   Uploadpage uploadpage = new Uploadpage();
	   uploadpage.uploadmedia(path, title, description, copyright);	
	   Thread.sleep(3000);
	   mediatobeaddedinplay.click();
	   Thread.sleep(3000);
	   String mediaplayid = mediaid.getText();
	   System.out.println(mediaplayid);
	   Thread.sleep(3000);
	   playlistbtn.click();
	   Thread.sleep(3000);
	   Playlistpage playlistpage = new Playlistpage();
	   Thread.sleep(3000);
	   playlistpage.Listbtn();
	   this.createNewPlaylist(playlistitle, playlistdescription, thumbnailpath);
	   Thread.sleep(3000);
	   playlistbtn.click();
	   Thread.sleep(3000);
   	   playlistnumber.click();
   	   Thread.sleep(3000);
       String playidcheck = playlistid.getText();
       System.out.println(playidcheck);
       Thread.sleep(3000);
       mediatab.click();
       Thread.sleep(3000);
       searchbtnformedia.click();
       Thread.sleep(3000);
       searchbtnformedia.sendKeys(mediaplayid);
       Thread.sleep(3000);
       searchbtnformedia.sendKeys(Keys.ENTER);
       Thread.sleep(3000);
       managetab.click();
       Thread.sleep(3000);
       mediatobeaddedtoplay.click();
       Thread.sleep(3000);
       playlisttabtoaddmedia.click();
       Thread.sleep(3000);
       searchplaylisttextbox.click();
       Thread.sleep(3000);
       searchplaylisttextbox.sendKeys(playidcheck);
       Thread.sleep(3000);
       playlistcheckboxtoadd.click();
       Thread.sleep(3000);
       confirmtheplaylistmediadd.click();
       Thread.sleep(3000);
       Mediapage mediapage = new Mediapage();
       mediapage.checkplaylistnameformedia(mediaplayid);

    }
    
     
   
    
}
