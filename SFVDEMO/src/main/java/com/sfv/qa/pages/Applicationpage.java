package com.sfv.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.sfv.qa.base.testbase;

public class Applicationpage extends testbase
{
	@FindBy(xpath="//a[@sfv-acl='create-application']")
	 WebElement createapplicabtn ;
	
	// Initializing the Page Objects:
	public Applicationpage() 
   {
       PageFactory.initElements(driver, this);
   }
	
	//tested
	public  Createapplicationpage clickoncreateapplibtn()
	{
		createapplicabtn.click();
		return new  Createapplicationpage();
	} 

}
