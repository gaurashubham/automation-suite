package com.sfv.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.sfv.qa.base.testbase;

public class Hotspotapptestingpage extends testbase
{
	@FindBy(xpath="//a[@class='pointer']")
	 WebElement dashboardtab;
	
	@FindBy(xpath="//a[text()='Media']")
	 WebElement mediatab;
	
	@FindBy(xpath="//a[text()='Playlist']")
	 WebElement playlisttab;
	
	@FindBy(xpath="//a[text()='Upload']")
	 WebElement uploadtab;
	
	@FindBy(xpath="//a[text()='Tag']")
	 WebElement tagtab;
	
	@FindBy(xpath="//a[text()='Applications']")
	 WebElement applitab;
	
	@FindBy(xpath="//a[text()='Management']")
	 WebElement managementab;
	
	// Initializing the Page Objects:
     public Hotspotapptestingpage() 
     {
        PageFactory.initElements(driver, this);
     }
     
    public Dashboardpage clickOndashboarbtab()
     {
   	  dashboardtab.click();
         return new Dashboardpage();
     }  
     
     public  Mediapage clickOnmediatab()
     {
   	  mediatab.click();
         return new Mediapage();
     }  
     
     public Playlistpage clickOnplaylisttab()
     {
   	  playlisttab.click();
         return new Playlistpage();
     }  
     
     public Uploadpage clickOnuploadtab()
     {
   	  uploadtab.click();
         return new Uploadpage();
     }  
     
     public  Tagpage clickOntagtab()
     {
   	  tagtab.click();
         return new Tagpage();
     }  
     
     public  Applicationpage clickOnapplitab()
     {
   	  applitab.click();
         return new Applicationpage();
     }  
     
     public Managementpage clickOnmanagementab()
     {
   	  managementab.click();
         return new Managementpage();
     }  
     
    
}

