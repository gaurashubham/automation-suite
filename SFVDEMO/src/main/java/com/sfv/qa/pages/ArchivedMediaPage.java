package com.sfv.qa.pages;

import java.security.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.TimeZone;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.sfv.qa.base.testbase;

public class ArchivedMediaPage extends testbase
{
	@FindBy(xpath="(//a[@data-ui-sref='list-archive-media'])[1]")
	 WebElement archievedmediatab;
	
	@FindBy(xpath="(//span[@class='item-title pointer ng-binding'])[1]")
	 WebElement archivedmedialist;
	
	@FindBy(xpath="(//th[@class='ng-binding'])[1]")
	 WebElement creationdate; 
	
	@FindBy(xpath="//input[@id='contentExpiry']")
	 WebElement expirationdate; 
	
	@FindBy(xpath="//input[@ng-model='media.title']")
	 WebElement mediatitle; 
	
	@FindBy(xpath="//button[text()='Manage']")
	 WebElement managebutton;
	
	@FindBy(xpath="//input[@name='search']")
	 WebElement searchbutton;
	
	@FindBy(xpath="//input[@name='checkbox']")
	 WebElement checkbox;
	
	@FindBy(xpath="//button[@title='Retrieve archived media']")
	 WebElement retrivedarchievedmediabutton;
	
	@FindBy(xpath="//span[@class='item-title pointer ng-binding']")
	 WebElement mediatitletext;
	
	@FindBy(xpath="//th[@class='col-72 ng-binding']")
	 WebElement mediaid;
	
	@FindBy(xpath="//button[@title='Retrieve archived media']")
	 WebElement restorearchived;
	
	@FindBy(xpath="//button[text()='OK']")
	 WebElement OKbtn;
	
	@FindBy(xpath="//div[text()='Retrive operation successful']")
	 WebElement retrivedsuccessfully;
	
	@FindBy(xpath="(//a[@class=\"btn menu-btn ng-binding\"])[1]")
	 WebElement mediatab;
	
	@FindBy(xpath="//input[@placeholder='What are you looking for?']")
	 WebElement searchtab;
	
	@FindBy(xpath="(//span[@class='notification-icon-count ng-binding'])[1]")
	 WebElement notification;
	
	@FindBy(xpath="(//a[text()='Video Retrived : '])[1]")
	 WebElement notificationdrop;
	
	@FindBy(xpath="(//div[@class='row notification-list-wrapper-inner']//span[@class='ng-binding'])[2]")
	 WebElement retrivedmediaid;
	
	@FindBy(xpath="//span[@class='item-title pointer ng-binding']")
	 WebElement medianumber;
	
	@FindBy(xpath="//th[@class='col-72 ng-binding']")
	 WebElement mediaidtest;
	
   // Initializing the Page Objects:
	public ArchivedMediaPage() 
  {
      PageFactory.initElements(driver, this);
  }
	
	public void checkarchivedmedias() throws InterruptedException 
{
		archievedmediatab.click();
		archivedmedialist.click();
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOf(expirationdate));
		Thread.sleep(5000);
		String contentexpirydate = expirationdate.getAttribute("value");
		System.out.println("Date1 : "+contentexpirydate);
		
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String strDate= formatter.format(date);
		System.out.println(strDate);		
		
		if (strDate.compareTo(contentexpirydate) > 0) 
		{
            System.out.println("Archived Media present");
		}
 }
	
	public void restorearchivedmedia(String mediaid)
	{
		archievedmediatab.click();
		searchbutton.click();
		searchbutton.sendKeys(mediaid);
		managebutton.click();
		checkbox.click();
		restorearchived.click();
		Actions action = new Actions(driver);
		action.moveToElement(OKbtn).click().build().perform();
		String text1 = retrivedsuccessfully.getText();
		String text2 = "Retrive operation successful";
		Assert.assertEquals(text1, text2);
		System.out.println("Retrived the video");
		mediatab.click();
		searchtab.click();
		searchtab.sendKeys(mediaid);
		notification.click();
		String text3 = notificationdrop.getText();
		String text4 = "Video Retrived : ";
		if(text3.contains(text4))
		{
			System.out.println("Video Retrived");
		}
		String text5 = retrivedmediaid.getText();
		System.out.println("Media ID : "  +mediaid+" "+"-"+" ");
		String text6 = mediaid+" "+"-"+" ";
		System.out.println(text6);
		Assert.assertEquals(text5, text6);
		medianumber.click();
	    String text7 = mediaidtest.getText();
	    Assert.assertEquals(text7, mediaid);
	}
	
	public void replacearchievedmedia()
	{
		
	}
}

