package com.sfv.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.sfv.qa.base.testbase;

public class Usersettingpage extends testbase
{
	@FindBy(xpath="//a[@ng-click='showCreateUpdateUser()']")
	 WebElement createuserbtn;
	
	@FindBy(xpath="//input[@name='search']")
	 WebElement searchbtn;
	
	@FindBy(xpath="(//a[@class='ng-binding'])[1]")
	 WebElement searchfield;
	
	// Initializing the Page Objects:
			public Usersettingpage() 
			   {
			       PageFactory.initElements(driver, this);
			   }
			
			public Createuserpage clickoncreateuser()
			{
				createuserbtn.click();
				return new Createuserpage();
			} 	
	
			public void clickonsearch()
			{
				searchbtn.click();
				String ele = "Saiful.yusoff@isobar.com";
				searchbtn.sendKeys(ele);
				String ele1 = searchfield.getText();
				if(ele.equals(ele1))
				{
					System.out.println("User created");
				}
			} 	
	
}
