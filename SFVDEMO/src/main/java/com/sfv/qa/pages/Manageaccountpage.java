package com.sfv.qa.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.sfv.qa.base.testbase;
import com.sfv.qa.util.TestUtil;

public class Manageaccountpage extends testbase
{
	@FindBy(xpath="//a[text()='Manage Accounts']")
	 WebElement Manageaccountsbtn;
	
	@FindBy(xpath="//input[@id='AccountKey']")
	 WebElement Accountkeytextbox;
	
	@FindBy(xpath="//input[@id='AccountName']")
	 WebElement Accountnametextbox;
	
	@FindBy(xpath="(//i[@class='caret pull-right'])[2]")
	 WebElement Accountownerdropdown;
	
	@FindBy(xpath="//input[@placeholder='Choose User']")
	 WebElement Chooseuserdropdown;
	
	@FindBy(xpath="//label[@for='ownerDailyNotification']")
	 WebElement Dailynotificationfirsttoggleswitch;
	
	@FindBy(xpath="//div[@class='tags']")
	 WebElement Notificationemailtextbox;
	
	@FindBy(xpath="//label[@for='userDailyNotification']")
	 WebElement Dailynotificationsecondtoggleswitch;
	
	@FindBy(xpath="//textarea[@id='description']")
	 WebElement descriptiontextbox;
	
	@FindBy(xpath="(//button[@class='btn btn-astro-default'])[1]")
	 WebElement Savebutton;
	
	@FindBy(xpath="//div[text()='Web account created successfully.']")
	 WebElement Webaccountsavednotificationmessage;
	
	@FindBy(xpath="//input[@name='search']")
	 WebElement searchtextbox;
	
	@FindBy(xpath="//div[@class='webccount-item-wrapper text-wrap ng-binding']")
	 WebElement Accountcreatedname;
	
	@FindBy(xpath="(//span[@class='btn btn-default form-control ui-select-toggle'])[1]")
	 WebElement Accountopendropdownclick;
	
	@FindBy(xpath="(//input[@type='search'])[1]")
	 WebElement searchforcreatedaccounttext;
	
	@FindBy(xpath="(//span[@class='ng-binding'])[1]")
	 WebElement Accountid;
	
	@FindBy(xpath="(//span[@class='ng-binding'])[2]")
	 WebElement Accountkeytext;
	
	@FindBy(xpath="(//span[@class='ng-binding'])[3]")
	 WebElement createdon;
	
	@FindBy(xpath="//a[@class='pointer']")
	 WebElement allaccountsbtn;
	
	@FindBy(xpath="//a[text()='Management']")
	 WebElement Managementtab;
	
	@FindBy(xpath="//a[text()='Encoding Profile']")
	 WebElement Encodingtab;
	
	@FindBy(xpath="(//i[@class='fa fa-edit'])[1]")
	 WebElement editmp4;
	
	@FindBy(xpath="(//i[@class='fa fa-edit'])[2]")
	 WebElement editHLS;
	
	@FindBy(xpath="(//label[@ng-model='encodingProfile.bitrate'])[1]")
	 WebElement mp4checkbitrate;
	
	@FindBy(xpath="//button[text()='Save']")
	 WebElement savebtn;
	
	@FindBy(xpath="(//label[@ng-click='selectBitRate($index)'])[1]")
	 WebElement hlscheckbitrate;
			
	// Initializing the Page Objects:
	   public Manageaccountpage() 
	   {
	       PageFactory.initElements(driver, this);
	   }
	   
	   public void createAccounttestcase(String Accountkey,String Accountname,String AccountOwner,String Notificationemails,String Description) throws InterruptedException
	   {
		   Manageaccountsbtn.click();
		   Thread.sleep(10000);
		   String Accountkey1 = Accountkey + TestUtil.GetCurrentDateAndTimeAppFormat();
		   Accountkeytextbox.sendKeys(Accountkey1);
		   Thread.sleep(3000);
		   String Accountname1 = Accountkey + TestUtil.GetCurrentDateAndTimeAppFormat();
		   Accountnametextbox.sendKeys(Accountname1);
		   Thread.sleep(3000);
		   Accountownerdropdown.click();
		   Thread.sleep(8000);
		   Chooseuserdropdown.sendKeys(AccountOwner);
		   Thread.sleep(3000);
		   Chooseuserdropdown.sendKeys(Keys.ENTER);
		   Thread.sleep(3000);
		   Dailynotificationfirsttoggleswitch.click();
		   Thread.sleep(3000);
		   Actions actions = new Actions(driver);
		   actions.moveToElement(Notificationemailtextbox);
		   actions.click();
		   actions.sendKeys(Notificationemails);
		   actions.build().perform();
		   Thread.sleep(3000);
		   Dailynotificationsecondtoggleswitch.click();
		   /*actions.moveToElement(Dailynotificationsecondtoggleswitch);
		   actions.click();*/
		   Thread.sleep(3000);
		   descriptiontextbox.sendKeys(Description);
		   Thread.sleep(3000);
		   Savebutton.click();
		   Thread.sleep(2000);
		   String text1 = Webaccountsavednotificationmessage.getText();
		   System.out.println(text1);
		   String text2 = "Web account created successfully.";
		   Assert.assertEquals(text1, text2);
		   Thread.sleep(3000);
		   searchtextbox.click();
		   Thread.sleep(3000);
		   searchtextbox.sendKeys(Accountname1);
		   Thread.sleep(3000);
		   String text3 = Accountcreatedname.getText();
		   Thread.sleep(3000);
		   System.out.println(text3);
		   Assert.assertEquals(text3, Accountname1);
		   Thread.sleep(3000);
		   Manageaccountsbtn.click();
		   Thread.sleep(3000);
		   Accountopendropdownclick.click();
		   Thread.sleep(3000);
		   searchforcreatedaccounttext.sendKeys(Accountname1);
		   Thread.sleep(3000);
		   searchforcreatedaccounttext.sendKeys(Keys.ENTER);
		   Thread.sleep(3000);
		   String text4 = Accountid.getText();
		   System.out.println(text4);
		   String text5 = Accountkeytext.getText();
		   System.out.println(text5);
		   String text6 = createdon.getText();
		   System.out.println(text6);
		   allaccountsbtn.click();
		   Thread.sleep(3000);
		   searchtextbox.click();
		   Thread.sleep(3000);
		   searchtextbox.sendKeys(Accountname1);
		   Thread.sleep(3000);
		   Accountcreatedname.click();
		   Thread.sleep(3000);
		   Managementtab.click();
		   Thread.sleep(3000);
		   Encodingtab.click();
		   Thread.sleep(3000);
		   editmp4.click();
		   Thread.sleep(3000);
		   boolean text7 = mp4checkbitrate.isEnabled();
		   System.out.println(text7);
		   Thread.sleep(3000);
		   savebtn.click();
		   Thread.sleep(3000);
		   editHLS.click();
		   boolean text8 = hlscheckbitrate.isEnabled();
		   System.out.println(text8);
	   }
	
			
			
			
			
			
					
					
					
					
							
}
