package com.sfv.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.sfv.qa.base.testbase;

public class Tagpage extends testbase
{

	@FindBy(xpath="//a[@data-ui-sref='create-tag-profile']")
	 WebElement  createtagprofilebtn;
	
	// Initializing the Page Objects:
	public Tagpage() 
    {
        PageFactory.initElements(driver, this);
    }
	
	public Createtagprofilepage tagpageprofile()
	{
		createtagprofilebtn.click();
		return new Createtagprofilepage();
	} 

}
