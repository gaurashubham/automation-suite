package com.sfv.qa.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.sfv.qa.base.testbase;
import com.sfv.qa.util.TestUtil;

public class Createtagprofilepage extends testbase
{
	@FindBy(xpath="(//input[@id='title'])[1]")
	 WebElement titletab;
	
	@FindBy(xpath="//button[@ng-click='addTag()']")
	 WebElement addtagfield;
	
	@FindBy(xpath="//input[@name='namespace']")
	 WebElement namespacefie;
	
   @FindBy(xpath="//input[@name='predicate']")
	 WebElement predicatefie;
	
	@FindBy(xpath="//select[@name='count']")
	 WebElement countbtn;
	
	@FindBy(xpath="//select[@name='option']")
	 WebElement optionfield;
	
	@FindBy(xpath="//select[@name='type']")
	 WebElement typefield;
	
	@FindBy(xpath="(//button[@type='submit'])[1]")
	 WebElement savebtn;
	
	@FindBy(xpath="//button[@ng-click='rowform.$cancel()']")
	 WebElement cancelbtn;
	
	@FindBy(xpath="(//input[@id='title'])[2]")
	 WebElement valuebtn;
	
	@FindBy(xpath="//button[text()='Add Value']")
	 WebElement addvaluebtn;
	
	@FindBy(xpath="//button[text()='Save']")
	 WebElement savebutton;
	
	@FindBy(xpath="//button[text()='Cancel']")
	 WebElement cancelbutton;
	
	@FindBy(xpath="//input[@name='search']")
	 WebElement searchbtn;
	
	@FindBy(xpath="(//a[@class='ng-binding'])[1]")
	 WebElement searchfield;
	
	@FindBy(xpath="//span[text()='Please add at least one tag.']")
	 WebElement creationtagnoti;
	
	@FindBy(xpath="//button[@ng-click='saveTagProfile()']")
	 WebElement savebtn1;
	
	
	 public Createtagprofilepage() 
	    {
	        PageFactory.initElements(driver, this);
	    }
	    
	    public void createNewtagprofile(String title,String namespac,String predi,String values) throws InterruptedException, AWTException
	    {
            String title1 = title + TestUtil.GetCurrentDateAndTimeAppFormat();
	    	titletab.sendKeys(title1);
	    	addtagfield.click();
	    	namespacefie.sendKeys(namespac);
	    	predicatefie.sendKeys(predi);
	    	
	    	
	    	Select sel1 = new Select(countbtn);
	    	sel1.selectByIndex(1);
	    	
	    	Select sel2 = new Select(optionfield);
	    	sel2.selectByIndex(1);
	    	
	    	Select sel3 = new Select(typefield);
	    	sel3.selectByIndex(1);
	    	
	    	savebtn.click();
	    	valuebtn.sendKeys(values);
	    	addvaluebtn.click();
	    	Thread.sleep(6000);
	         for (int second = 0;; second++) 
	      {
	             if(second >=3)
	             {
	                break;
	             } 
	        JavascriptExecutor jse = (JavascriptExecutor)driver;
	    	jse.executeScript("window.scrollBy(0,100)");
	        Thread.sleep(3000);
	      }  
	    	
	    	savebutton.click();
	    	searchbtn.click();
	    	searchbtn.sendKeys(title1);
	    	searchbtn.sendKeys(Keys.ENTER);
	    	
	        Thread.sleep(2000);
	        
	        String ele1 = searchfield.getText();
			System.out.println(ele1);
			System.out.println(title1);
			Assert.assertEquals(title1, ele1);
			System.out.println("Tagprofile found");
	}
	    
	    public void creationtagmandatory(String title)
	    {
	    	titletab.sendKeys(title);
	    	savebtn1.click();
	    	String text = creationtagnoti.getText();
	    	System.out.println(text);
	    	String text1 = "Please add at least one tag.";
	    	Assert.assertEquals(text, text1);
	    	System.out.println("Tag profile not created");
	    }
	    
	    
}
