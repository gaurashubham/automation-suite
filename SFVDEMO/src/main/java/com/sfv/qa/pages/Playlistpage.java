package com.sfv.qa.pages;


import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.sfv.qa.base.testbase;

public class Playlistpage extends testbase
{
	
	
	@FindBy(xpath="//a[@class='btn btn-astro-default']")
	 WebElement createplaylistbtn ;
	
	@FindBy(xpath="(//a[@class='item-title ng-binding'])[1]")
	 WebElement playlistnumber;
	
	@FindBy(xpath="//a[text()='Thumbnail']")
	 WebElement thumbnailtab;
	
	@FindBy(xpath="(//button[text()='Upload Image'])[1]")
	 WebElement uploadimagebutton;
	
	@FindBy(xpath="//a[text()='Details & Tags']")
	 WebElement detailsandtagstab;
	
	@FindBy(xpath="(//button[text()='Save'])[1]")
	 WebElement savebtn;
	
	@FindBy(xpath="//div[text()='Playlist updated successfully.']")
	 WebElement playlistupdatedsuccessfullynoti;
	
	@FindBy(xpath="//button[text()='View thumbnail']")
	 WebElement viewthumbnailbutton;
	
	@FindBy(xpath="//input[@name='search']")
	 WebElement searchbtn;
	
	
	
	
	
    // Initializing the Page Objects:
	public Playlistpage() 
    {
        PageFactory.initElements(driver, this);
    }
	
	public Createplaylistpage Listbtn()
	{
		createplaylistbtn.click();
		return new Createplaylistpage();
	} 
	
	public void additionofthumbnailtoplaylist(String playlistid,String path) throws AWTException, InterruptedException
	{
		  Thread.sleep(8000);
		  searchbtn.click();
		  Thread.sleep(3000);
		  searchbtn.sendKeys(playlistid);
				  
		  Thread.sleep(3000);
		  searchbtn.sendKeys(Keys.ENTER);
		  Thread.sleep(3000);
		  playlistnumber.click();
		  Thread.sleep(3000);
		  thumbnailtab.click();
		  Thread.sleep(2000);
		 
		  //Click on the File Upload / Choose File button, so that the File Upload dialog is displayed.
		//  uploadimagebutton.click();
		  
		  //Copy your file's absolute path to the clipboard
		 /* String filePathchrome1 = System.getProperty("user.dir") + path;
          Thread.sleep(1000);
          */
		  
		  /*
		  StringSelection ss = new StringSelection(path);
		  Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
		  Thread.sleep(5000);
		  
		  //Paste the file's absolute path into the File name field of the File Upload dialog box
		//native key strokes for CTRL, V and ENTER keys
		  Robot robot = new Robot();
      
		  robot.keyPress(KeyEvent.VK_CONTROL);
		  robot.keyPress(KeyEvent.VK_V);
		  
		  Thread.sleep(5000);
		  robot.keyRelease(KeyEvent.VK_V);
		  robot.keyRelease(KeyEvent.VK_CONTROL);
		  Thread.sleep(5000);
		  robot.keyPress(KeyEvent.VK_ENTER);
		  robot.keyRelease(KeyEvent.VK_ENTER);
		  Thread.sleep(5000);	
		  */	  
		 
		// disable the click event on an `<input>` file
		  ((JavascriptExecutor)driver).executeScript(
		      "HTMLInputElement.prototype.click = function() {                     " +
		      "  if(this.type !== 'file') HTMLElement.prototype.click.call(this);  " +
		      "};                                                                  " );

		  // trigger the upload
		  driver.findElement(By.xpath("(//button[text()='Upload Image'])[1]"))
		        .click();

		  // assign the file to the `<input>`
		  driver.findElement(By.cssSelector("input[type=file]"))
		        .sendKeys(path);
		 
		  
		
		detailsandtagstab.click();
		Thread.sleep(5000);
		savebtn.click();
		Thread.sleep(1000);
		String text = playlistupdatedsuccessfullynoti.getText();
		String text1 = "Playlist updated successfully.";
		Assert.assertEquals(text, text1);
		searchbtn.sendKeys(playlistid);
		Thread.sleep(3000);
		searchbtn.sendKeys(Keys.ENTER);
	    Thread.sleep(3000);
		playlistnumber.click();
		thumbnailtab.click();
		viewthumbnailbutton.isDisplayed();
		System.out.println("Thumbnail added to playlist successfully");
		
	}
	
	
	
}

