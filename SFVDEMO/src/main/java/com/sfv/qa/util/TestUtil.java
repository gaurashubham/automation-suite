package com.sfv.qa.util;

import java.io.File;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.sfv.qa.base.testbase;


public class TestUtil extends testbase
{
   public static long PAGE_LOAD_TIMEOUT = 100;
   public static long IMPLICIT_WAIT = 100;
   public static String filePathchrome2 = System.getProperty("user.dir") + "\\src\\Excel\\tagsvalue.xlsx";
   public static String TESTDATA_SHEET_PATH = (filePathchrome2);

	static Workbook book;
    static Sheet sheet;

	public void switchToFrame()
	{
        driver.switchTo().frame("mainpanel");
    }

	public static Object[][] getTestData(String sheetName) {

		FileInputStream file = null;
        try {
               file = new FileInputStream(TESTDATA_SHEET_PATH);
            } catch (FileNotFoundException e) {

			       e.printStackTrace();
            }

		try 
		{
              book = WorkbookFactory.create(file);
        } 
		catch (InvalidFormatException e) 
		{
              e.printStackTrace();
        } 
		catch (IOException e) 
		{
              e.printStackTrace();
        }

		sheet = book.getSheet(sheetName);
        Object[][] data = new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
        // System.out.println(sheet.getLastRowNum() + "--------" +
        // sheet.getRow(0).getLastCellNum());
       /* for (int i = 0; i < sheet.getLastRowNum(); i++) 
        {*/
             for (int k = 0; k < sheet.getRow(0).getLastCellNum(); k++) 
              {
            	 int i=0;
                  data[i][k] = sheet.getRow(i + 1).getCell(k).toString();
                  // System.out.println(data[i][k]);
              }

		//}
         return data;
   }

	public static void takeScreenshotAtEndOfTest() throws IOException 
	{

		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        String currentDir = System.getProperty("user.dir");
        FileUtils.copyFile(scrFile, new File(currentDir + "\\screenshots\\" + System.currentTimeMillis() + ".png"));
    }

	

public static void clickbutton(String clickxpath) 
{
      driver.findElement(By.xpath(clickxpath)).click();
}

public static void textboxsendkeys(String sendkeyxpath,String texttobeentered)
{
	driver.findElement(By.xpath(sendkeyxpath)).sendKeys(texttobeentered);

}
public static String gettextofelement(String gettextxpath)
{
	return driver.findElement(By.xpath(gettextxpath)).getText();
}
 
public static void checkPageIsReady()
{
	 JavascriptExecutor js = (JavascriptExecutor)driver;
	  
	  
	  //Initially bellow given if condition will check ready state of page.
	  if (js.executeScript("return document.readyState").toString().equals("complete")){ 
	   System.out.println("Page Is loaded.");
	   return; 
	  } 
}
public static String GetCurrentDateAndTimeAppFormat() {
	// Create object of SimpleDateFormat class and decide the format
	 DateFormat dateFormat = new SimpleDateFormat("MMddyyyyHHmmss");
	 //get current date time with Date()
	 Date date = new Date();
	 // Now format the date
	 String CurrentDateTime = dateFormat.format(date);
	 return CurrentDateTime;
}
}
