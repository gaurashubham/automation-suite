package com.sfv.qa.testcase;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Ingestprofilepage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.pages.Managementpage;
import com.sfv.qa.pages.Usersettingpage;
import com.sfv.qa.util.TestUtil;

public class Ingestprofiletest extends testbase
{
	Loginpage loginpage;
    HomePage homePage;
    TestUtil testUtil;
    Blazeclanpage blazeclanapppage;
    ActiveaccountPage activeaccpage;
    Managementpage managepage;
    Ingestprofilepage ingestpropage;
    testbase testbase;
    
    public Ingestprofiletest()
    {
       super();
    }
    
    @BeforeMethod
    public void setUp() 
    {
    	initialization();
        testUtil = new TestUtil();
        loginpage = new  Loginpage();
        activeaccpage = new ActiveaccountPage();
        homePage = new HomePage();
        blazeclanapppage = new Blazeclanpage();
        managepage = new Managementpage();
        ingestpropage = new Ingestprofilepage();
        testbase = new testbase();
        
        homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
        activeaccpage = homePage.clickOnActiveAccountButton();
        activeaccpage.clickOnButton();
        blazeclanapppage.clickOnmanagementab();
        managepage.clickoningestionprofilepage();
    }
    
    @Test(priority=1)
    public void createingestionpro()
    {
    	//driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    	ingestpropage.clickoncreateingestprofilebtn();
    }
    
    @Test(priority=2)
    public void searchuser()
    {
    	//driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    	ingestpropage.clickonsearchbtn(); 
    }
    
    @AfterMethod
    public void closebrowser() throws InterruptedException
	{
       Thread.sleep(5000); 
       testbase.logout();
       Thread.sleep(5000);
       driver.quit();
    }
    
    
}
