package com.sfv.qa.testcase;

import java.awt.AWTException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.ArchivedMediaPage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.Createplaylistpage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.pages.Playlistpage;
import com.sfv.qa.util.TestUtil;

public class ArchivedPageTest extends testbase
{
	Loginpage loginpage;
    HomePage homePage;
    TestUtil testUtil;
    Blazeclanpage blazeclanapppage;
    ActiveaccountPage activeaccpage;
    ArchivedMediaPage archivedmediapage;
    testbase testbase;
    
  
    public ArchivedPageTest()
    {
       super();
    }
    
    @BeforeMethod
    public void setUp() 
    {
    	initialization();
        testUtil = new TestUtil();
        loginpage = new  Loginpage();
        activeaccpage = new ActiveaccountPage();
        homePage = new HomePage();
        blazeclanapppage = new Blazeclanpage();
        archivedmediapage = new ArchivedMediaPage();
        testbase = new testbase();
        
        homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
        activeaccpage = homePage.clickOnActiveAccountButton();
        TestUtil.checkPageIsReady();
        activeaccpage.clickOnButton();
        blazeclanapppage.clickOnmediatab();
        
    }
    
    
    @Test(priority=19)
    public void archivedmedia() throws InterruptedException, AWTException
    {
    	 System.out.println("-----------------------------------------------------------------------------------------");
    	 System.out.println("archivedmedia");
    	 System.out.println("-----------------------------------------------------------------------------------------");
    	 archivedmediapage.checkarchivedmedias();

	}
    
    
}
