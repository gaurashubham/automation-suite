package com.sfv.qa.testcase;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.Forgotpasswordpage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.pages.Mediapage;
import com.sfv.qa.pages.Uploadpage;
import com.sfv.qa.util.TestUtil;

public class Forgotpasswordtest extends testbase
{
	Forgotpasswordpage forgotpasspage;
	
	@BeforeMethod
    public void setUp() 
    {
    	initialization();
    	forgotpasspage = new Forgotpasswordpage();
    }
   
	 @Test(priority=11)
	    public void TestingForgotPasswordFunctionalityForExistingUser() throws InterruptedException
	    {
		 System.out.println("-----------------------------------------------------------------------------------------");
		 System.out.println("TestingForgotPasswordFunctionalityForExistingUser()");
		 System.out.println("-----------------------------------------------------------------------------------------");
		 forgotpasspage.forgotpassvaliduser();
	    }
	 
	 @Test(priority=9)
	    public void TestingForgotPasswordFunctionalityForIncorrectUserFormat() throws InterruptedException
	    {
		 System.out.println("-----------------------------------------------------------------------------------------");
		 System.out.println("TestingForgotPasswordFunctionalityForIncorrectUserFormat()");
		 System.out.println("-----------------------------------------------------------------------------------------");
		 forgotpasspage.forgotpassinvalidemail();
	    }
	 
	 @Test(priority=10)
	    public void TestingForgotPasswordFunctionalityForNonExistingUser() throws InterruptedException
	    {
		 System.out.println("-----------------------------------------------------------------------------------------");
		 System.out.println("TestingForgotPasswordFunctionalityForNonExistingUser()");
		 System.out.println("-----------------------------------------------------------------------------------------");
		 forgotpasspage.forgotpassinvaliduser();
	    }
	 
	 @AfterMethod
	   public void closebrowser() throws InterruptedException
	   {
	     Thread.sleep(5000);
	      driver.quit();
	   }

}

