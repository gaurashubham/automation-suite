
package com.sfv.qa.testcase;

import java.awt.AWTException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.Applicationpage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.Createapplicationpage;
import com.sfv.qa.pages.Createplaylistpage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.pages.Playlistpage;
import com.sfv.qa.util.TestUtil;

public class Createapplicationtest extends testbase
{
	Loginpage loginpage;
    HomePage homePage;
    TestUtil testUtil;
    Blazeclanpage blazeclanapppage;
    ActiveaccountPage activeaccpage;
    Playlistpage playlistpage;
    Createapplicationpage createapplicationpage;
    Applicationpage applicationpage;
    testbase testbase;
    
    String sheetName = "application";
    
    public Createapplicationtest()
    {
       super();
    }
    
    @BeforeMethod
    public void setUp() throws InterruptedException 
    {
    	initialization();
        testUtil = new TestUtil();
        loginpage = new  Loginpage();
        activeaccpage = new ActiveaccountPage();
        homePage = new HomePage();
        blazeclanapppage = new Blazeclanpage();
        applicationpage = new Applicationpage();
        createapplicationpage = new Createapplicationpage();
        testbase = new testbase();
        
        homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
        activeaccpage = homePage.clickOnActiveAccountButton();
        activeaccpage.clickOnButton();
        blazeclanapppage.clickOnapplitab();
       
        
    }
    
     @DataProvider
    public Object[][] getSFVTestData1()
    {
        Object data[][] = TestUtil.getTestData(sheetName);
        return data;
    }
    
    @Test(priority=29, dataProvider="getSFVTestData1")

	public void validateCreateapplication(String appliname, String applialias,String web,String plakey,String descrip,String adtagtext,String gtmidtext) throws InterruptedException, AWTException
    {
    	applicationpage.clickoncreateapplibtn();
        Thread.sleep(4000);
    	System.out.println("-----------------------------------------------------------------------------------------");
    	System.out.println("validateCreateapplication");
    	System.out.println("-----------------------------------------------------------------------------------------");
    	createapplicationpage.createNewappli(appliname,applialias,web,plakey,descrip, adtagtext, gtmidtext);

	}
    
    @AfterMethod
    public void closebrowser() throws InterruptedException
	{
       Thread.sleep(5000); 
       testbase.logout();
       Thread.sleep(5000);
       driver.quit();
    }
   
    
   
    

}
