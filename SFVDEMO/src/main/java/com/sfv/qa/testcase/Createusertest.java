
package com.sfv.qa.testcase;

import java.awt.AWTException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.Applicationpage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.Createapplicationpage;
import com.sfv.qa.pages.Createuserpage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.pages.Managementpage;
import com.sfv.qa.pages.Playlistpage;
import com.sfv.qa.pages.Usersettingpage;
import com.sfv.qa.util.TestUtil;

public class Createusertest extends testbase
{
	Loginpage loginpage;
    HomePage homePage;
    TestUtil testUtil;
    Blazeclanpage blazeclanapppage;
    ActiveaccountPage activeaccpage;
    Playlistpage playlistpage;
    Managementpage managepage;
    Usersettingpage usersetpage;
    Createuserpage createuser;
    testbase testbase;
    
    String sheetName = "User";
    String sheetName1 = "Sheet3";
   
    public Createusertest()
    {
       super();
    }
    
    @BeforeMethod
    public void setUp() 
    {
    	initialization();
        testUtil = new TestUtil();
        loginpage = new  Loginpage();
        activeaccpage = new ActiveaccountPage();
        homePage = new HomePage();
        blazeclanapppage = new Blazeclanpage();
        managepage= new Managementpage();
        usersetpage= new Usersettingpage();
        createuser = new Createuserpage();
        testbase = new testbase();
        
        homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
        activeaccpage = homePage.clickOnActiveAccountButton();
        activeaccpage.clickOnButton();
        blazeclanapppage.clickOnmanagementab();
        managepage.clickonusersetting();
        usersetpage.clickoncreateuser();
    }
    
    
     @DataProvider
    public Object[][] getSFVTestData()
    {
        Object data[][] = TestUtil.getTestData(sheetName);
        return data;
    }
    
    @Test(priority=30, dataProvider="getSFVTestData")

	public void createuseradminaccess(String emailadd, String pass,String conpass,String fname,String lastname,String contactno) throws InterruptedException, AWTException
    {
    	System.out.println("-----------------------------------------------------------------------------------------");
    	System.out.println("createuseradminaccess");
    	System.out.println("-----------------------------------------------------------------------------------------");
    	createuser.creuser(emailadd, pass, conpass, fname, lastname, contactno);
    	
    }
    
   @DataProvider
    public Object[][] getSFVTestData1()
    {
        Object data[][] = TestUtil.getTestData(sheetName1);
        return data;
    }
    
    
    @Test(priority=31, dataProvider="getSFVTestData1")
    public void createuserhavinguseraccess(String emailadd, String pass,String conpass,String fname,String lastname,String contactno) throws InterruptedException, AWTException
       {
    	System.out.println("-----------------------------------------------------------------------------------------");
    	System.out.println("createuserhavinguseraccess");
    	System.out.println("-----------------------------------------------------------------------------------------");
       	  createuser.creuserole(emailadd, pass, conpass, fname, lastname, contactno);
       }
       
    
    @AfterMethod
   public void closebrowser() throws InterruptedException
	{
       Thread.sleep(5000); 
       testbase.logout();
       Thread.sleep(5000);
       driver.quit();
    }

}
