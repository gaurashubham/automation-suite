package com.sfv.qa.testcase;

import java.awt.AWTException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.pages.Manageaccountpage;
import com.sfv.qa.util.TestUtil;

public class ManageAccountTest extends testbase
{
	Loginpage loginpage;
    HomePage homePage;
    TestUtil testUtil;
    testbase testbase;
    Manageaccountpage manageaccountpage;
    
    String sheetName = "Accountcreation";
    
    public ManageAccountTest()
    {
       super();
    }
    
    @BeforeMethod
    public void setUp() 
    {
    	initialization();
        testUtil = new TestUtil();
        loginpage = new  Loginpage();
        homePage = new HomePage();
        testbase = new testbase();
        manageaccountpage = new Manageaccountpage();
        homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
      
    }
    
    
     @DataProvider
    public Object[][] getSFVTestData()
    {
        Object data[][] = TestUtil.getTestData(sheetName);
        return data;
    }
    
    @Test(priority=59, dataProvider="getSFVTestData")

	public void CreateWebAccount(String Accountkey,String Accountname,String AccountOwner,String Notificationemails,String Description) throws InterruptedException, AWTException
    {
    	System.out.println("-----------------------------------------------------------------------------------------");
    	System.out.println("CreateWebAccount");
    	System.out.println("-----------------------------------------------------------------------------------------");
    	manageaccountpage.createAccounttestcase(Accountkey, Accountname, AccountOwner, Notificationemails, Description);
    	
    }
    
    @AfterMethod
    public void closebrowser() throws InterruptedException
	{
       Thread.sleep(5000); 
       testbase.logout();
       Thread.sleep(5000);
       driver.quit();
    }
    
}
