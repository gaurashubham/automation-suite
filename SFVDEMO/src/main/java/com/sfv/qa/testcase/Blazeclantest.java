
package com.sfv.qa.testcase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.Applicationpage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.pages.Tagpage;
import com.sfv.qa.util.TestUtil;

public class Blazeclantest extends testbase
{
	Loginpage loginpage;
    HomePage homePage;
    TestUtil testUtil;
    Blazeclanpage blazeclanapppage;
    ActiveaccountPage activeaccpage;
    Applicationpage applicationpage;
    testbase testbase;

    public Blazeclantest()
    {
       super();
    }
    
    @BeforeMethod
    public void setUp() throws InterruptedException 
    {
    	initialization();
        testUtil = new TestUtil();
        loginpage = new  Loginpage();
        activeaccpage = new ActiveaccountPage();
        homePage = new HomePage();
        blazeclanapppage = new Blazeclanpage();
        testbase = new testbase();
  
        homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
        activeaccpage = homePage.clickOnActiveAccountButton();
        activeaccpage.clickOnButton();
     }
    
     @Test(priority=12)
    public void verifyplaylisttablink()
    {
    	
   	  //driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    	 blazeclanapppage.clickOnplaylisttab();
    }
     
     @Test(priority=13)
    public void verifyappliclink()
    {
    	
   	  //driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    	 blazeclanapppage.clickOnapplitab();
    }
     
     @Test(priority=14)
     public void verifymanagementlink()
     {
     	//driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
     	 blazeclanapppage.clickOnmanagementab();
     }
     
     @Test(priority=15)
     public void verifytagtablink()
     {
     	//driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
     	 blazeclanapppage.clickOntagtab();
     }
      
     @Test(priority=16)
     public void verifyuploadtab() throws InterruptedException
     {
     	//driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
     	 blazeclanapppage.clickOnuploadtab();
     }
     
     @Test(priority=17)
     public void verifymediatab() throws InterruptedException
     {
     	//driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
     	 blazeclanapppage.clickOnmediatab();
     }
     
     @Test(priority=18)
     public void verifydashboardtab() throws InterruptedException
     {
     	//driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
     	 blazeclanapppage.clickOndashboarbtab();
     }
     @AfterMethod
     public void closebrowser() throws InterruptedException
 	{
        Thread.sleep(5000); 
        testbase.logout();
        Thread.sleep(5000);
        driver.quit();
     }
    
}
