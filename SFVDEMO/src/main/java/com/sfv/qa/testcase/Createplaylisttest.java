package com.sfv.qa.testcase;

import java.awt.AWTException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.Createplaylistpage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.pages.Playlistpage;
import com.sfv.qa.util.TestUtil;

public class Createplaylisttest extends testbase
{
	Loginpage loginpage;
    HomePage homePage;
    TestUtil testUtil;
    Blazeclanpage blazeclanapppage;
    ActiveaccountPage activeaccpage;
    Playlistpage playlistpage;
    Createplaylistpage createplaylistpage;
    testbase testbase;
    
    String sheetName = "createplaylist";
    String sheetName2 = "playlistnametowhichmediaremove";
    String sheetName1 = "endtoendplaylistaddition";
    String sheetName3 = "thumbnailuploadtoplaylist";
    String sheetName4 = "addnewmediatoplaylist";
    
    public Createplaylisttest()
    {
       super();
    }
    
    @BeforeMethod
    public void setUp() 
    {
    	initialization();
        testUtil = new TestUtil();
        loginpage = new  Loginpage();
        activeaccpage = new ActiveaccountPage();
        homePage = new HomePage();
        blazeclanapppage = new Blazeclanpage();
        playlistpage = new Playlistpage();
        createplaylistpage = new Createplaylistpage();
        testbase = new testbase();
        
        homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
        activeaccpage = homePage.clickOnActiveAccountButton();
        TestUtil.checkPageIsReady();
        activeaccpage.clickOnButton();
        blazeclanapppage.clickOnplaylisttab();
        playlistpage.Listbtn();
        
    }
    
    @DataProvider
    public Object[][] getSFVTestData()
    {
        Object data[][] = TestUtil.getTestData(sheetName);
        return data;
    }
    
    //TestCase to create a new playlist an add thumbnail to it
   @Test(priority=19, dataProvider="getSFVTestData")

	public void validateCreateplaylist(String title, String description,String path) throws InterruptedException, AWTException
    {
    	 System.out.println("-----------------------------------------------------------------------------------------");
    	 System.out.println("validateCreateplaylist");
    	 System.out.println("-----------------------------------------------------------------------------------------");
    	createplaylistpage.createNewPlaylist(title , description , path);

	}
    
    @Test(priority=20)
    public void AddMediaToThePlayist() throws InterruptedException, AWTException
    {
    	 System.out.println("-----------------------------------------------------------------------------------------");
    	 System.out.println("AddMediaToThePlayist() ");
    	 System.out.println("-----------------------------------------------------------------------------------------");
    	createplaylistpage.addmediatoplaylist();
    }
    
    @DataProvider
    public Object[][] getSFVTestData11()
    {
        Object data[][] = TestUtil.getTestData(sheetName1);
        return data;
    }
    
    @Test(priority=21 , dataProvider="getSFVTestData11")
    public void CompletePlaylistFlow(String title,String description) throws InterruptedException, AWTException
    {
    	 System.out.println("-----------------------------------------------------------------------------------------");
    	 System.out.println("CompletePlaylistFlow");
    	 System.out.println("-----------------------------------------------------------------------------------------");
    	createplaylistpage.endtoendplaylist(title, description);
    }
    
    
    @Test(priority=22)
    public void MediacheckPlaylist() throws InterruptedException
     {
       System.out.println("-----------------------------------------------------------------------------------------");
   	   System.out.println("MediacheckPlaylist");
   	   System.out.println("-----------------------------------------------------------------------------------------");
   	   createplaylistpage.checkplaylistdetails();
     }
    
    @Test(priority=23)
    public void ChangingStatusOfMultiplayLIST() throws InterruptedException
    {
	   System.out.println("-----------------------------------------------------------------------------------------");
	   System.out.println("ChangingStatusOfMultiplayLIST");
	   System.out.println("-----------------------------------------------------------------------------------------");
    	createplaylistpage.changestatusmulti();
    }
    
    @DataProvider
    public Object[][] getSFVTestData2()
    {
        Object data[][] = TestUtil.getTestData(sheetName2);
        return data;
    }
    
    
     @Test(priority=24, dataProvider="getSFVTestData2")
   public void RemovalOfMediaFromPlaylist(String title,String description) throws InterruptedException
    {
        System.out.println("-----------------------------------------------------------------------------------------");
  	   System.out.println("RemovalOfMediaFromPlaylist");
  	   System.out.println("-----------------------------------------------------------------------------------------");
  	   createplaylistpage.removalmedia(title,description);
    }
     
     @DataProvider
     public Object[][] getSFVTestData4()
     {
         Object data[][] = TestUtil.getTestData(sheetName4);
         return data;
     }
     
     @Test(priority=25, dataProvider="getSFVTestData4")
     public void addmediatoplaylistandchecksymbol(String path,String title,String description,String copyright,String playlistitle,String playlistdescription,String thumbnailpath) throws InterruptedException, AWTException
      {
        System.out.println("-----------------------------------------------------------------------------------------");
    	   System.out.println("addmediatoplaylistandchecksymbol");
    	   System.out.println("-----------------------------------------------------------------------------------------");
    	   createplaylistpage.adduploadedmediatoplaylist(path, title, description, copyright, playlistitle, playlistdescription, thumbnailpath);
       }
   
    @Test(priority=26)
    public void DeletionOfMultiplaylist() throws InterruptedException
    {
    	 System.out.println("-----------------------------------------------------------------------------------------");
    	 System.out.println("DeletionOfMultiplaylist");
    	 System.out.println("-----------------------------------------------------------------------------------------");
    	createplaylistpage.deletemulti();
    }
    
    @AfterMethod
    public void closebrowser() throws InterruptedException
	{
       Thread.sleep(5000); 
       testbase.logout();
       Thread.sleep(5000);
       driver.quit();
    }
    
   
}
