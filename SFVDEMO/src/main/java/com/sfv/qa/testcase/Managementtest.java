package com.sfv.qa.testcase;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.pages.Managementpage;
import com.sfv.qa.pages.Playlistpage;
import com.sfv.qa.util.TestUtil;

public class Managementtest extends testbase
{
	Loginpage loginpage;
    HomePage homePage;
    TestUtil testUtil;
    Blazeclanpage blazeclanapppage;
    ActiveaccountPage activeaccpage;
    Managementpage managepage;
    testbase testbase;
    
    String sheetName = "thumbnailprofilecreation";
    
    public Managementtest()
    {
       super();
    }
    
    @BeforeMethod
    public void setUp() 
    {
    	initialization();
        testUtil = new TestUtil();
        loginpage = new  Loginpage();
        activeaccpage = new ActiveaccountPage();
        homePage = new HomePage();
        blazeclanapppage = new Blazeclanpage();
        managepage = new Managementpage();
        testbase = new testbase();
        
        homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
        activeaccpage = homePage.clickOnActiveAccountButton();
        activeaccpage.clickOnButton();
        blazeclanapppage.clickOnmanagementab();
    }
    
   /* @Test(priority=62)
    public void Usersetting()
    {
    	//driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    	managepage.clickonusersetting();
    }
    
    @Test(priority=63)
    public void encoding()
    {
    	
   	  //driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    	managepage.clickonencodingprofile();
    }
    
    @Test(priority=64)
    public void thumbnail()
    {
    	
   	  //driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    	managepage.clickonthumbnailprofile();
    }
    
    @Test(priority=65)
    public void syndicationsetting()
    {
    	
   	  //driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    	managepage.clickonsyndicatesetting();
    }
    
    @Test(priority=66)
    public void ingestionpro()
    {
    	
   	  //driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    	managepage.clickoningestionprofilepage();
    }
    */
    @DataProvider
    public Object[][] getSFVTestData()
    {
        Object data[][] = TestUtil.getTestData(sheetName);
        return data;
    }
    
    @Test(priority=57 , dataProvider="getSFVTestData")
    public void thumbnailprofilecreation(String widthvalue,String heightvalue,String thumbnailoption)
    {
    	System.out.println("-----------------------------------------------------------------------------------------");
    	System.out.println("thumbnailprofilecreation");
    	System.out.println("-----------------------------------------------------------------------------------------");
    	managepage.creationthumbnailprofile(widthvalue, heightvalue, thumbnailoption);
    }
    
    @Test(priority=58)
    public void thumbnailprofiledeletion() throws InterruptedException
    {
    	System.out.println("-----------------------------------------------------------------------------------------");
    	System.out.println("thumbnailprofiledeletion");
    	System.out.println("-----------------------------------------------------------------------------------------");
    	managepage.deletethumbnailprofile();
    }
    
    
    
    @AfterMethod
    public void closebrowser() throws InterruptedException
	{
       Thread.sleep(5000); 
       testbase.logout();
       Thread.sleep(5000);
       driver.quit();
    }
    

}
