package com.sfv.qa.testcase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.util.TestUtil;

public class HomePagetest extends testbase
{
	  Loginpage loginpage;
      HomePage homePage;
      TestUtil testUtil;
      ActiveaccountPage  activeaccpage; 
      testbase testbase;
      
      public HomePagetest() 
      {
            super();
      }
      
      @BeforeMethod
      public void setUp() 
      {
             initialization();
             testUtil = new TestUtil();
             loginpage = new  Loginpage();
             testbase = new testbase();
             homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
      }
      
      @Test(priority=5)
      public void verifyHomePageTitleTest()
      {
    	  System.out.println("-----------------------------------------------------------------------------------------");
    	  System.out.println("verifyHomePageTitleTest()");
    	  System.out.println("-----------------------------------------------------------------------------------------");
             String homePageTitle = homePage.verifyHomePageTitle();
             Assert.assertEquals(homePageTitle, "Short Form Video - Astro","Home page title not matched");
      }
      
      @Test(priority=7)
      public void verifyUserNameTest()
      {
    	  System.out.println("-----------------------------------------------------------------------------------------");
    	  System.out.println(" verifyUserNameTest()");
    	  System.out.println("-----------------------------------------------------------------------------------------");
             //testUtil.switchToFrame();
             Assert.assertTrue(homePage.verifyCorrectUserName());
      }
      
      @Test(priority=6)
      public void verifyActiveaccbtnTest()
      {
    	  System.out.println("-----------------------------------------------------------------------------------------");
    	  System.out.println("verifyActiveaccbtnTest()");
    	  System.out.println("-----------------------------------------------------------------------------------------");
    	    //testUtil.switchToFrame();
    	    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            activeaccpage = homePage.clickOnActiveAccountButton();
      }
      
      @Test(priority=8)
      public void verifyinactiveaccounts()
      {
    	  System.out.println("-----------------------------------------------------------------------------------------");
    	  System.out.println("verifyinactiveaccounts()");
    	  System.out.println("-----------------------------------------------------------------------------------------");
          driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
          homePage.clickoninactiveaccounts();
      }
      
     
      
      @AfterMethod
      public void closebrowser() throws InterruptedException
  	{
         Thread.sleep(5000); 
         testbase.logout();
         Thread.sleep(5000);
         driver.quit();
      }

}
