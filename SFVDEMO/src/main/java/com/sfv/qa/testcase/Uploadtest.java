package com.sfv.qa.testcase;

import java.awt.AWTException;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.pages.Playlistpage;
import com.sfv.qa.pages.Uploadpage;
import com.sfv.qa.util.TestUtil;

public class Uploadtest extends testbase
{
	Loginpage loginpage;
    HomePage homePage;
    TestUtil testUtil;
    Blazeclanpage blazeclanapppage;
    ActiveaccountPage activeaccpage;
    Uploadpage uploadpage;
    testbase testbase;
    
    String sheetName = "uploadwebm";
    String sheetName7 = "uploadwithouttitle";
    String sheetName1 = "uplooadmp4";
    String sheetName2 = "uploadmov";
    String sheetName3 = "uploadincorrect";
    String sheetName4 = "syndicationvideo";
    String sheetName5 = "transcoding";
    String sheetName6 = "uploadurl";
    String sheetName8 = "uploadingverfication";
    
    public Uploadtest()
    {
       super();
    }
    
    @BeforeMethod
    public void setUp() throws InterruptedException 
    {
    	initialization();
        testUtil = new TestUtil();
        loginpage = new  Loginpage();
        activeaccpage = new ActiveaccountPage();
        homePage = new HomePage();
        blazeclanapppage = new Blazeclanpage();
        uploadpage = new Uploadpage();
        testbase = new testbase();
        
        homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
        activeaccpage = homePage.clickOnActiveAccountButton();
        activeaccpage.clickOnButton();
        blazeclanapppage.clickOnuploadtab();
        
     }
   
   
   
  
  
  @DataProvider
  public Object[][] getSFVTestData8()
  {
     Object data[][] = TestUtil.getTestData(sheetName);
     return data;
  }
  
    
    @Test(priority=48, dataProvider="getSFVTestData8")
    public void uploadingwebm(String path, String title,String description,String copyright) throws InterruptedException, AWTException
    {
    	 System.out.println("-----------------------------------------------------------------------------------------");
    	 System.out.println("uploadingwebm");
    	 System.out.println("-----------------------------------------------------------------------------------------");
    	 String title1 = title + TestUtil.GetCurrentDateAndTimeAppFormat();
  	     uploadpage.uploadmedia(path, title1, description, copyright);
    }
    
   @DataProvider
    public Object[][] getSFVTestData1()
    {
        Object data[][] = TestUtil.getTestData(sheetName1);
        return data;
    }
    
    @Test(priority=49, dataProvider="getSFVTestData1")
    public void uploadingmp4(String path, String title,String description,String copyright) throws InterruptedException, AWTException
    {
    	 System.out.println("-----------------------------------------------------------------------------------------");
    	 System.out.println("uploadingmp4");
    	 System.out.println("-----------------------------------------------------------------------------------------");
    	 String title1 = title + TestUtil.GetCurrentDateAndTimeAppFormat();
  	      uploadpage.uploadmedia(path, title1, description, copyright);
    }
   
    @DataProvider
    public Object[][] getSFVTestData2()
    {
        Object data[][] = TestUtil.getTestData(sheetName2);
        return data;
    }
    
    @Test(priority=50, dataProvider="getSFVTestData2")
    public void uploadingmov(String path, String title,String description,String copyright) throws InterruptedException, AWTException
    {
    	 System.out.println("-----------------------------------------------------------------------------------------");
    	 System.out.println("uploadingmov");
    	 System.out.println("-----------------------------------------------------------------------------------------");
    	 String title1 = title + TestUtil.GetCurrentDateAndTimeAppFormat();
  	     uploadpage.uploadmedia(path, title1, description, copyright);
    }
   
   @DataProvider
    public Object[][] getSFVTestData3()
    {
        Object data[][] = TestUtil.getTestData(sheetName3);
        return data;
    }
   
    @Test(priority=51, dataProvider="getSFVTestData3")
    public void uploadingincorrect(String path) throws InterruptedException, AWTException
    {
            System.out.println("-----------------------------------------------------------------------------------------"); 
            System.out.println("uploadingincorrect");            
            System.out.println("-----------------------------------------------------------------------------------------");
  	        uploadpage.incorrectformat(path);
    }
   
    @DataProvider
    public Object[][] getSFVTestData4()
    {
        Object data[][] = TestUtil.getTestData(sheetName4);
        return data;
    }
    
    //Testcase for syndication of a video , checking the wifi symbol , checking whether its in syndicated tab , syndication of a new video
   @Test(priority=52, dataProvider="getSFVTestData4")
    public void syndicateendtoend(String path, String title,String description,String copyright,String syndiprofile,String syndiprofilemail) throws InterruptedException
    {
	   System.out.println("-----------------------------------------------------------------------------------------");
	   System.out.println("syndicateendtoend");
	   System.out.println("-----------------------------------------------------------------------------------------");
	   uploadpage.syndicateendtoend(path, title, description, copyright, syndiprofile, syndiprofilemail);
    }
    
    @DataProvider
    public Object[][] getSFVTestData5()
    {
       Object data[][] = TestUtil.getTestData(sheetName5);
       return data;
    }
    
    
    //Transcoding testcase + addition of tag value and checking the tag symbol for the video
    @Test(priority=53, dataProvider="getSFVTestData5")
    public void transcoding(String path, String title,String description,String copyright,String tagsprofile) throws InterruptedException, AWTException
    {
    	 System.out.println("-----------------------------------------------------------------------------------------");
    	 System.out.println("transcoding");
    	 System.out.println("-----------------------------------------------------------------------------------------");
  	      uploadpage.transcoding(path, title, description, copyright,tagsprofile);
    }
   
   @DataProvider
  public Object[][] getSFVTestData6()
  {
     Object data[][] = TestUtil.getTestData(sheetName6);
     return data;
  }
    
   //Upload using the URL + addition of multiplelanguage to the video
    @Test(priority=54, dataProvider="getSFVTestData6")
  public void uploadusingurl(String path,String title,String description,String copyright,String title1,String description1) throws InterruptedException, AWTException
  {
    	 System.out.println("-----------------------------------------------------------------------------------------");
    	 System.out.println("uploadusingurl");
    	 System.out.println("-----------------------------------------------------------------------------------------");
	      uploadpage.uploadurl(path,title, description, copyright,title1,description1);
  }
    
  
    
    @DataProvider
    public Object[][] getSFVTestData7()
    {
       Object data[][] = TestUtil.getTestData(sheetName7);
       return data;
    }
    
   @Test(priority=55,dataProvider="getSFVTestData7")
  public void  verifinontranscoding(String path) throws InterruptedException
  {
	   System.out.println("-----------------------------------------------------------------------------------------");
  	   System.out.println("verifinontranscoding");
  	   System.out.println("-----------------------------------------------------------------------------------------");
	   uploadpage.nontranscoded(path);
  }
   
   @DataProvider
   public Object[][] getSFVTestData()
   {
      Object data[][] = TestUtil.getTestData(sheetName8);
      return data;
   }
   
   //Verification of media upload + adding media preview to the video
   @Test(priority=56,dataProvider="getSFVTestData")
   public void  verificationofupload(String path, String title,String description,String copyright,String startslot,String endslot) throws InterruptedException, AWTException
   {
 	 System.out.println("-----------------------------------------------------------------------------------------");
  	 System.out.println("verificationofupload");
  	 System.out.println("-----------------------------------------------------------------------------------------");
  	 String title1 = title + TestUtil.GetCurrentDateAndTimeAppFormat();
 	 uploadpage.verificationofuploading(path,title1,description,copyright,startslot,endslot);
   }
    
   @AfterMethod
   public void closebrowser() throws InterruptedException
 	{
      Thread.sleep(5000); 
      testbase.logout();
      Thread.sleep(5000);
      driver.quit();
   }

    
   
    
}
