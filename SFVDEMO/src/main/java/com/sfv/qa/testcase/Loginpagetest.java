package com.sfv.qa.testcase;

import org.testng.Assert;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Loginpage;

public class Loginpagetest extends testbase
{
	Loginpage loginPage;
    HomePage homePage;
    testbase testbase;
    

	public Loginpagetest()
	{
       super();
    }

	@BeforeMethod
    public void setUp() throws InterruptedException
	{
        initialization();
        loginPage = new Loginpage();
        testbase = new testbase();
       
    }
	
	//@Title('testExample')
    @Test(priority=1)
    public void loginPageTitleTest()
    {
    	System.out.println("-----------------------------------------------------------------------------------------");
    	System.out.println("loginPageTitleTest()");
    	System.out.println("-----------------------------------------------------------------------------------------");
       String title = loginPage.validateLoginPageTitle();
       Assert.assertEquals(title, "Short Form Video - Astro");
    }

	@Test(priority=2)
    public void LoginPageimagetest() throws InterruptedException
	{
		System.out.println("-----------------------------------------------------------------------------------------");
		System.out.println("LoginPageimagetest()");
		System.out.println("-----------------------------------------------------------------------------------------");
        boolean flag = loginPage.validateAstroImage();
        System.out.println(flag);
        Assert.assertTrue(flag);
    }
	
    @Test(priority=3)
    public void loginwithValidUserCredentials() throws InterruptedException
    {
    	System.out.println("-----------------------------------------------------------------------------------------");
    	System.out.println("loginwithValidUserCredentials()");
    	System.out.println("-----------------------------------------------------------------------------------------");
       loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
       Thread.sleep(5000);
       Thread.sleep(5000); 
       testbase.logout();
    }
    
    @Test(priority=4)
    public void loginWithInvalidUserCredentials() throws InterruptedException
    {
    	System.out.println("-----------------------------------------------------------------------------------------");
    	System.out.println("loginWithInvalidUserCredentials() ");
    	System.out.println("-----------------------------------------------------------------------------------------");
       String username = "test@gmail.com";
       String password = "dump";
       loginPage.loginfail("username", "password");
       Thread.sleep(5000);
    }
    
     @AfterMethod
    public void closebrowser() throws InterruptedException
	{
       
       Thread.sleep(5000);
       driver.quit();
    }

}
