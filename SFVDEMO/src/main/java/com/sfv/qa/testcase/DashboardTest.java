package com.sfv.qa.testcase;

import java.awt.AWTException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.Dashboardpage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.util.TestUtil;

public class DashboardTest extends testbase
{
	 Loginpage loginpage;
     HomePage homePage;
     TestUtil testUtil;
     ActiveaccountPage  activeaccpage; 
     testbase testbase;
     Dashboardpage dashboardpage;
     Blazeclanpage blazeclanapppage;
     
     String sheetName ="sortbyrecentlyupload";
     String sheetName1 = "sortbyalphabetically";
     
     public DashboardTest() 
     {
           super();
     }
     
     @BeforeMethod
     public void setUp() throws InterruptedException 
     {
            initialization();
            testUtil = new TestUtil();
            loginpage = new  Loginpage();
            testbase = new testbase();
            dashboardpage = new  Dashboardpage();
            blazeclanapppage = new Blazeclanpage();
            homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
            Thread.sleep(6000);
            activeaccpage = homePage.clickOnActiveAccountButton();
            Thread.sleep(6000);
            activeaccpage.clickOnButton();
            Thread.sleep(6000);
           /* blazeclanapppage.clickOndashboarbtab();*/
     }
     
     @Test(priority=60)
     public void verifyvideotitle()
     {
   	     System.out.println("-----------------------------------------------------------------------------------------");
   	     System.out.println("verifyvideotitle");
   	     System.out.println("-----------------------------------------------------------------------------------------");
         dashboardpage.checkvideotitlelist();
     }
     
     @DataProvider
     public Object[][] getSFVTestData()
     {
        Object data[][] = TestUtil.getTestData(sheetName);
        return data;
     }
    
    
     @Test(priority=61, dataProvider="getSFVTestData")
     public void sortbyrecentlyupload(String path,String title,String description,String copyright) throws InterruptedException, AWTException
     {
   	     System.out.println("-----------------------------------------------------------------------------------------");
   	     System.out.println("sortbyrecentlyupload");
   	     System.out.println("-----------------------------------------------------------------------------------------");
   	     String title1 = title + TestUtil.GetCurrentDateAndTimeAppFormat();
         dashboardpage.sortbyrecentlyupload(path, title1, description, copyright);
     }
     
     @DataProvider
     public Object[][] getSFVTestData1()
     {
        Object data[][] = TestUtil.getTestData(sheetName1);
        return data;
     }
     @Test(priority=62, dataProvider="getSFVTestData1")
     public void sortbyalphabetically(String path,String title,String description,String copyright) throws InterruptedException, AWTException
     {
   	     System.out.println("-----------------------------------------------------------------------------------------");
   	     System.out.println("sortbyalphabetically");
   	     System.out.println("-----------------------------------------------------------------------------------------");
   	     String title1 = title + TestUtil.GetCurrentDateAndTimeAppFormat(); 
         dashboardpage.sortbyalphabetically(path, title1, description, copyright);
     }
     
     
     @Test(priority=63)
     public void gridlistview() throws InterruptedException, AWTException
     {
   	     System.out.println("-----------------------------------------------------------------------------------------");
   	     System.out.println("gridlistview");
   	     System.out.println("-----------------------------------------------------------------------------------------");
         dashboardpage.gridviewandlistview();
     }
     
     
     @AfterMethod
     public void closebrowser() throws InterruptedException
  	{
         Thread.sleep(5000); 
         testbase.logout();
         Thread.sleep(5000);
         driver.quit();
      }
}
