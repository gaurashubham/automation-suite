package com.sfv.qa.testcase;

import java.awt.AWTException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.pages.Mediapage;
import com.sfv.qa.pages.Uploadpage;
import com.sfv.qa.util.TestUtil;

public class Mediatest extends testbase
{
	Loginpage loginpage;
    HomePage homePage;
    TestUtil testUtil;
    Blazeclanpage blazeclanapppage;
    ActiveaccountPage activeaccpage;
    Uploadpage uploadpage;
    Mediapage mediapage;
    testbase testbase;
    
    String sheetName = "Sheet2";
    String sheetName1 = "incomplete";
    String sheetName2 = "multilanguage";
    String sheetName4 = "mediaplaylistcheck";
    String sheetName6 = "syndicationtestcase";
    String sheetName10 = "syndicationmultiplemedia";
   
    
    
    public Mediatest()
    {
       super();
    }
    
    @BeforeMethod
    public void setUp() throws InterruptedException 
    {
    	initialization();
        testUtil = new TestUtil();
        loginpage = new  Loginpage();
        activeaccpage = new ActiveaccountPage();
        homePage = new HomePage();
        blazeclanapppage = new Blazeclanpage();
        uploadpage = new Uploadpage();
        mediapage = new Mediapage();
        testbase = new testbase();
        
        homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
        activeaccpage = homePage.clickOnActiveAccountButton();
        activeaccpage.clickOnButton();
        Thread.sleep(8000);
        blazeclanapppage.clickOnmediatab();
       
     }
    
    @DataProvider
    public Object[][] getSFVTestData()
    {
       Object data[][] = TestUtil.getTestData(sheetName);
       return data;
    }
    
    @Test(priority=35, dataProvider="getSFVTestData")
    public void verificationofreplace(String test) throws InterruptedException, AWTException
     {
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  System.out.println("verificationofreplace");
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  mediapage.replacement(test);
     }
    
    @DataProvider
    public Object[][] getSFVTestData1()
    {
       Object data[][] = TestUtil.getTestData(sheetName1);
       return data;
    }
    
    @Test(priority=36, dataProvider="getSFVTestData1")
    public void verificationofincomplete(String path) throws InterruptedException, AWTException
     {
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  System.out.println("verificationofincomplete");
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  mediapage.incompletedmedia(path);
     }
    
    @Test(priority=37)
    public void thumbnailcapturetest() throws InterruptedException
     {
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  System.out.println("thumbnailcapturetest");
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  mediapage.thumbnailcapture();
     }
    
    
    @Test(priority=38)
    public void statusmultimedia() throws InterruptedException
     {
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  System.out.println("statusmultimedia");
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  mediapage.statuschangemul();
     }
    
    @Test(priority=39)
    public void incompletemediaaddplay() throws InterruptedException
     {
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  System.out.println("incompletemediaaddplay");
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  mediapage.incompletemediaaddplaylist();
     }
    
    @Test(priority=40)
    public void deletedmedia() throws InterruptedException, AWTException
     {
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  System.out.println("deletedmedia");
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  mediapage.deletedmediatab();
     }
    
    @DataProvider
    public Object[][] getSFVTestData2()
    {
       Object data[][] = TestUtil.getTestData(sheetName2);
       return data;
    }
    
    @Test(priority=41, dataProvider="getSFVTestData2")
    public void veriofmultilanguage(String mediaid,String title,String description) throws InterruptedException, AWTException
     {
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  System.out.println("veriofmulti");
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  mediapage.multiplelanguage(mediaid,title, description);
     }
    
    @Test(priority=42)
    public void veriofaddmediatoplay() throws InterruptedException, AWTException
   {
    	System.out.println("-----------------------------------------------------------------------------------------");
    	System.out.println("veriofaddmediatoplay");
    	System.out.println("-----------------------------------------------------------------------------------------");
  	  mediapage.addmediatoplay();
   }
    
    @Test(priority=43)
    public void deleteandrestore() throws InterruptedException
     {
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  System.out.println("deleteandrestore");
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  mediapage.restoredeletedmedia();
     }
  
     @Test(priority=44)
  public void mediadetail() throws InterruptedException
  
   {
	  System.out.println("-----------------------------------------------------------------------------------------");
	  System.out.println("mediadetail");
	  System.out.println("-----------------------------------------------------------------------------------------");
	  mediapage.mediadetails();
   }
    
   
    @Test(priority=45)
    public void mediacode() throws InterruptedException
     {
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  System.out.println("mediacode");
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  mediapage.checkcode();
     }
    
    
    @DataProvider
    public Object[][] getSFVTestData10()
    {
       Object data[][] = TestUtil.getTestData(sheetName10);
       return data;
    }
   
   @Test(priority=46, dataProvider="getSFVTestData10")
    public void syndicatemultimedia(String syndicationprofiletype,String syndicationemail) throws InterruptedException
     {
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  System.out.println("syndicatemultimedia");
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  mediapage.syndicatemul(syndicationprofiletype, syndicationemail);
     }
   
     @Test(priority=47)
    public void deletemultimedia() throws InterruptedException
     {
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  System.out.println("deletemultimedia");
  	  System.out.println("-----------------------------------------------------------------------------------------");
  	  mediapage.deletemulti();
     }
    
    @AfterMethod
     public void closebrowser() throws InterruptedException
	{
     Thread.sleep(5000); 
     testbase.logout();
     Thread.sleep(5000);
     driver.quit();
  }
    
}    
    

