package com.sfv.qa.testcase;

import java.awt.AWTException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.Changepasswordpage;
import com.sfv.qa.pages.Createplaylistpage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.pages.Playlistpage;
import com.sfv.qa.util.TestUtil;

public class Changepasswordtest extends testbase
{
	Loginpage loginpage;
    HomePage homePage;
    TestUtil testUtil;
    testbase testbase;
    Changepasswordpage changepasspage;
    
    String sheetName = "validchangepass";
    String sheetName1 = "incorrectoldpassword";
    String sheetName2 = "incorrectnewncon";
    
    public Changepasswordtest()
    {
       super();
    }
    
    @BeforeMethod
    public void setUp() 
    {
    	initialization();
        testUtil = new TestUtil();
        loginpage = new  Loginpage();
        homePage = new HomePage();
        testbase = new testbase();
        changepasspage = new Changepasswordpage();
        
        homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
    }
    @DataProvider
    public Object[][] getSFVTestData()
    {
        Object data[][] = TestUtil.getTestData(sheetName);
        return data;
    }
    
    @DataProvider
    public Object[][] getSFVTestData1()
    {
        Object data[][] = TestUtil.getTestData(sheetName1);
        return data;
    }
    
    @DataProvider
    public Object[][] getSFVTestData2()
    {
        Object data[][] = TestUtil.getTestData(sheetName2);
        return data;
    }
    
    @Test(priority=3, dataProvider="getSFVTestData")

	public void ValidateChangePassword(String oldpass,String newpass,String confirmpass) throws InterruptedException
    {
    	 changepasspage.validchangepass(oldpass, newpass, confirmpass);
    }
    
    @Test(priority=2, dataProvider="getSFVTestData1")
    public void IncorrectoldpassWord(String oldpass,String newpass,String confirmpass) throws InterruptedException, AWTException
    {
    
    	 changepasspage.incorrectoldpass(oldpass, newpass, confirmpass);
    }
    
    @Test(priority=1, dataProvider="getSFVTestData2")
    public void MismatchNewpasswordAndConfirmPassword(String oldpass,String newpass,String confirmpass) throws InterruptedException
    {
    	 changepasspage.mismatchnewncon(oldpass, newpass, confirmpass);
    }
    
    @AfterMethod
    public void closebrowser() throws InterruptedException
	{
       Thread.sleep(5000); 
       testbase.logout();
       Thread.sleep(5000);
       driver.quit();
    }
    
   
}
