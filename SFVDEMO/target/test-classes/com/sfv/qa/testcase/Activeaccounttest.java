package com.sfv.qa.testcase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.sfv.qa.base.testbase;
import com.sfv.qa.pages.ActiveaccountPage;
import com.sfv.qa.pages.HomePage;
import com.sfv.qa.pages.Blazeclanpage;
import com.sfv.qa.pages.Loginpage;
import com.sfv.qa.util.TestUtil;

public class Activeaccounttest extends testbase
{
	Loginpage loginpage;
    HomePage homePage;
    TestUtil testUtil;
    ActiveaccountPage  activeaccpage;
    Blazeclanpage hotspotapppage;
    testbase testbase;
    
    public Activeaccounttest() 
    {
          super();
    }
	 
    @BeforeMethod
    public void setUp() 
    {
           initialization();
           testUtil = new TestUtil();
           loginpage = new  Loginpage();
           activeaccpage = new ActiveaccountPage();
           homePage = new HomePage();
           testbase = new testbase();
           homePage = loginpage.login(prop.getProperty("username"), prop.getProperty("password"));
           activeaccpage = homePage.clickOnActiveAccountButton();
           
    }
 
    @Test(priority=1)
    public void verifyhotspotaccbtnTest()
    {
  	    driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
  	    activeaccpage.clickOnButton();
    }
    
    
    
      @AfterMethod
    public void closebrowser()
	{
       testbase.logout();
       driver.quit();
    }
   

}
